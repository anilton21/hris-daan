'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');


exports.Add_CivilStatus = (data, next) => {
    var strSQL = mysql.format('INSERT INTO civil_status VALUES(Null,?);', [
        data.cs_name
    ]);
    db.insertWithId(strSQL, next);
};

exports.getAllCivilStatus = (next) => {
     db.query(mysql.format('SELECT b.*,MD5(b.cs_id) as UUID FROM civil_status as b '), next);
};


exports.getCivilStatusById = (civilstat_id, next) => {
    var strSQL = mysql.format('SELECT * FROM civil_status WHERE  MD5(cs_id)=? LIMIT 1;', [civilstat_id]);
    db.query(strSQL, next);
};


exports.deleteCivilStatus = (civilstat_id, next) => {
    var strSQL = mysql.format('DELETE FROM civil_status WHERE MD5(cs_id)=?;', [civilstat_id]);
    db.actionQuery(strSQL, next);
};


exports.updateCivilStatus = (civilstat_id, data, next) => {
    var strSQL = mysql.format('UPDATE civil_status SET cs_name=? WHERE cs_id =?;', [
        data.cs_name, civilstat_id
    ]);
    db.actionQuery(strSQL, next);
};


exports.updateCivilStatus = (cs_id, data, next) => {
    var strSQL = mysql.format('UPDATE civil_status SET cs_name=?  WHERE cs_id=?;', [
        data.cs_name , cs_id
    ]);
    db.actionQuery(strSQL, next);
};