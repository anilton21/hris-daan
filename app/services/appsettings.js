'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');


exports.GetAllApplicationSettings = (next) => {
    var strSQL = mysql.format('SELECT P.*,MD5(P.SetupID) AS UUID FROM payroll_setup P;');
    db.query(strSQL, next);
};

exports.Delete_Application_Settings = (SetupID, next) => {
    var strSQL = mysql.format('DELETE FROM payroll_setup WHERE MD5(SetupID)=?;', [SetupID]);
    db.actionQuery(strSQL, next);
};

exports.Activate_Settings = (SetupID, next) => {
    var strSQL = mysql.format('UPDATE payroll_setup SET Active=1 WHERE MD5(SetupID)=?;', [SetupID]);
    db.actionQuery(strSQL, next);
};

exports.DeActivate_Settings = (next) => {
    var strSQL = mysql.format('UPDATE payroll_setup SET Active=0 WHERE Active =1;');
    db.actionQuery(strSQL, next);
};

exports.Add_Application_Settings = (App, next) => {
    var AppSettings = {
        Active: 0,
        DateCreated: new Date(),
        PayrollCode: App.PayrollCode || '',
        PayrollTitle: App.PayrollTitle || '',
        Description: App.Description || '',
        StartingMonth: App.StartingMonth || '',
        '1stCutOff': App._1stCutOff || '',
        '2ndCutOff': App._2ndCutOff || '',
        BasicPay_AccountID: App.BasicPay_AccountID || null,
        Absent_AccountID: App.Absent_AccountID || null,
        Tardiness_AccountID: App.Tardiness_AccountID || null,
        OvertimeRate: App.OvertimeRate || null,
        OvertimeExcess: App.OvertimeExcess || null,
        Overtime_AccountID: App.Overtime_AccountID || null,
        HolidayRate: App.HolidayRate || null,
        HolidayExcess: App.HolidayExcess || null,
        Holiday_AccountID: App.Holiday_AccountID || null,
        SundayRate: App.SundayRate || null,
        SundayExcess: App.SundayExcess || null,
        Sunday_AccountID: App.Sunday_AccountID || null,
        SpecialHolidayRate: App.SpecialHolidayRate || null,
        SpecialHolidayExcess: App.SpecialHolidayExcess || null,
        SpecialHoliday_AccountID: App.SpecialHoliday_AccountID || null,
        NightDifferentialsRate: App.NightDifferentialsRate || null,
        NightDifferentialsExcess: App.NightDifferentialsExcess || null,
        NightDifferentials_AccountID: App.NightDifferentials_AccountID || null,
        NightPremiumRate: App.NightPremiumRate || null,
        NightPremiumExcess: App.NightPremiumExcess || null,
        NightPremium_AccountID: App.NightPremium_AccountID || null,
        BankName: App.BankName || '',
        CompanyAccountNo: App.CompanyAccountNo || '',
        TaxID: App.TaxID || null,
        SSSID: App.SSSID || null,
        PHID: App.PHID || null,
        TaxMethodID: App.TaxMethodID || null,
        MonthID: App.MonthID || null,
        OtherIncome: App.OtherIncome || null,
        SSS_EE_AccountID: App.SSS_EE_AccountID || null,
        SSS_ER_AccountID: App.SSS_ER_AccountID || null,
        SSS_EC_AccountID: App.SSS_EC_AccountID || null,
        GSIS_EE_AccountID: App.GSIS_EE_AccountID || null,
        GSIS_ER_AccountID: App.GSIS_ER_AccountID || null,
        GSIS_EC_AccountID: App.GSIS_EC_AccountID || null,
        PHIC_EE_AccountID: App.PHIC_EE_AccountID || null,
        PHIC_ER_AccountID: App.PHIC_ER_AccountID || null,
        PAGIBIG: App.PAGIBIG || null,
        PAGIBIG_VALUE: App.PAGIBIG_VALUE || null,
        PAGIBIG_EE_AccountID: App.PAGIBIG_EE_AccountID || null,
        PAGIBIG_ER_AccountID: App.PAGIBIG_ER_AccountID || null,
        Withholding_AccountID: App.Withholding_AccountID || null,
        OtherDeduction: App.OtherDeduction || null,
        GracePeriod: App.GracePeriod || null,
        HalfDaySetup: App.HalfDaySetup || '',
        TotalHrsBreak: App.TotalHrsBreak || null,
        TotalWorkingHrsPerDay: App.TotalWorkingHrsPerDay || null,
        TotalWorkingHrsPerMonth: App.TotalWorkingHrsPerMonth || null,
        TotalWorkingHrsPerAnnum: App.TotalWorkingHrsPerAnnum || null,
        TotalWorkingDaysPerAnnum: App.TotalWorkingDaysPerAnnum || null,
        '1STCUTOFF_FROM': App._1STCUTOFF_FROM || '',
        '1STCUTOFF_TO': App._1STCUTOFF_TO || '',
        '2NDCUTOFF_FROM': App._2NDCUTOFF_FROM || '',
        '2NDCUTOFF_TO': App._2NDCUTOFF_TO || '',
        '13thMonthPay_AcctID': App._13thMonthPay_AcctID || null,
        RoundingID: App.RoundingID || null,
        TimeKeepingTech: App.TimeKeepingTech || null,
        TimeKeepingSetup: App.TimeKeepingSetup || null,
        SecurityLogInID: App.SecurityLogInID || null,
        Tardiness: App.Tardiness || null,
        Default_LeaveTypeID: App.Default_LeaveTypeID || null,
        GSISComputation: App.GSISComputation || null,
        SSSComputation: App.SSSComputation || null,
        PHICComputation: App.PHICComputation || null,
        HDMFComputation: App.HDMFComputation || null,
        TAXComputation: App.TAXComputation || null,
        GSISDivisor: App.GSISDivisor || null,
        SSSDivisor: App.SSSDivisor || null,
        PHICDivisor: App.PHICDivisor || null,
        HDMFDivisor: App.HDMFDivisor || null,
        TAXDivisor: App.TAXDivisor || null,
        IsGSISEE: App.IsGSISEE || null,
        IsGSISER: App.IsGSISER || null,
        IsSSSEE: App.IsSSSEE || null,
        IsSSSER: App.IsSSSER || null,
        IsPHICEE: App.IsPHICEE || null,
        IsPHICER: App.IsPHICER || null,
        IsHDMFEE: App.IsHDMFEE || null,
        IsHDMFER: App.IsHDMFER || null,
        IsTAXEE: App.IsTAXEE || null,
        IsTAXER: App.IsTAXER || null,
        IsComputeTax: App.IsComputeTax || null
    }
    var strSQL = mysql.format('INSERT INTO payroll_setup SET ?', AppSettings);
    console.log('strSQL: ', strSQL);
    db.insertWithId(strSQL, next);
};

exports.Add_Application_Settings2 = (App, next) => {
    var strSQL = mysql.format('INSERT INTO payroll_setup(PayrollCode,PayrollTitle,Description) VALUES (?,?,?);', [
        App.PayrollCode || '', App.PayrollTitle || '', App.Description || ''
    ]);
    console.log('strSQL: ', strSQL);
    db.insertWithId(strSQL, next);
};

exports.Update_Application_Settings = (SetupID, App, next) => {
    var strSQL = mysql.format("UPDATE payroll_setup SET PayrollCode=?," +
        "PayrollTitle=?,Description=?,DateModified=CURDATE(),ModifiedBy=?,StartingMonth=?,`1stCutOff`=?," +
        "`2ndCutOff`=?,BasicPay_AccountID=?,Absent_AccountID=?,Tardiness_AccountID=?,OvertimeRate=?," +
        "OvertimeExcess=?,Overtime_AccountID=?,HolidayRate=?,HolidayExcess=?,Holiday_AccountID=?," +
        "SundayRate=?,SundayExcess=?,Sunday_AccountID=?,SpecialHolidayRate=?,SpecialHolidayExcess=?," +
        "SpecialHoliday_AccountID=?,NightDifferentialsRate=?,NightDifferentialsExcess=?,NightDifferentials_AccountID=?,NightPremiumRate=?," +
        "NightPremiumExcess=?,NightPremium_AccountID=?,BankName=?,CompanyAccountNo=?,TaxID=?," +
        "SSSID=?,PHID=?,TaxMethodID=?,MonthID=?,OtherIncome=?," +
        "SSS_EE_AccountID=?,SSS_ER_AccountID=?,SSS_EC_AccountID=?,GSIS_EE_AccountID=?,GSIS_ER_AccountID=?," +
        "GSIS_EC_AccountID=?,PHIC_EE_AccountID=?,PHIC_ER_AccountID=?,PAGIBIG=?,PAGIBIG_VALUE=?," +
        "PAGIBIG_EE_AccountID=?,PAGIBIG_ER_AccountID=?,Withholding_AccountID=?,OtherDeduction=?,GracePeriod=?," +
        "HalfDaySetup=?,TotalHrsBreak=?,TotalWorkingHrsPerDay=?,TotalWorkingHrsPerMonth=?,TotalWorkingHrsPerAnnum=?," +
        "TotalWorkingDaysPerAnnum=?,`1STCUTOFF_FROM`=?,`1STCUTOFF_TO`=?,`2NDCUTOFF_FROM`=?,`2NDCUTOFF_TO`=?," +
        "`13thMonthPay_AcctID`=?,RoundingID=?,TimeKeepingTech=?,TimeKeepingSetup=?,SecurityLogInID=?," +
        "Tardiness=?,Default_LeaveTypeID=?,GSISComputation=?,SSSComputation=?,PHICComputation=?," +
        "HDMFComputation=?,TAXComputation=?,GSISDivisor=?,SSSDivisor=?,PHICDivisor=?," +
        "HDMFDivisor=?,TAXDivisor=?,IsGSISEE=?,IsGSISER=?,IsSSSEE=?," +
        "IsSSSER=?,IsPHICEE=?,IsPHICER=?,IsHDMFEE=?,IsHDMFER=?," +
        "IsTAXEE=?,IsTAXER=?,IsComputeTax=? WHERE MD5(SetupID)=?", [
            App.PayrollCode,
            App.PayrollTitle, App.Description, App.UserID, App.StartingMonth, App._1stCutOff,
            App._2ndCutOff, App.BasicPay_AccountID, App.Absent_AccountID, App.Tardiness_AccountID, App.OvertimeRate,
            App.OvertimeExcess, App.Overtime_AccountID, App.HolidayRate, App.HolidayExcess, App.Holiday_AccountID,
            App.SundayRate, App.SundayExcess, App.Sunday_AccountID, App.SpecialHolidayRate, App.SpecialHolidayExcess,
            App.SpecialHoliday_AccountID, App.NightDifferentialsRate, App.NightDifferentialsExcess, App.NightDifferentials_AccountID, App.NightPremiumRate,
            App.NightPremiumExcess, App.NightPremium_AccountID, App.BankName, App.CompanyAccountNo, App.TaxID,
            App.SSSID, App.PHID, App.TaxMethodID, App.MonthID, App.OtherIncome,
            App.SSS_EE_AccountID, App.SSS_ER_AccountID, App.SSS_EC_AccountID, App.GSIS_EE_AccountID, App.GSIS_ER_AccountID,
            App.GSIS_EC_AccountID, App.PHIC_EE_AccountID, App.PHIC_ER_AccountID, App.PAGIBIG, App.PAGIBIG_VALUE,
            App.PAGIBIG_EE_AccountID, App.PAGIBIG_ER_AccountID, App.Withholding_AccountID, App.OtherDeduction, App.GracePeriod,
            App.HalfDaySetup, App.TotalHrsBreak, App.TotalWorkingHrsPerDay, App.TotalWorkingHrsPerMonth, App.TotalWorkingHrsPerAnnum,
            App.TotalWorkingDaysPerAnnum, App._1STCUTOFF_FROM, App._1STCUTOFF_TO, App._2NDCUTOFF_FROM, App._2NDCUTOFF_TO,
            App._13thMonthPay_AcctID, App.RoundingID, App.TimeKeepingTech, App.TimeKeepingSetup, App.SecurityLogInID,
            App.Tardiness, App.Default_LeaveTypeID, App.GSISComputation, App.SSSComputation, App.PHICComputation,
            App.HDMFComputation, App.TAXComputation, App.GSISDivisor, App.SSSDivisor, App.PHICDivisor,
            App.HDMFDivisor, App.TAXDivisor, App.IsGSISEE, App.IsGSISER, App.IsSSSEE,
            App.IsSSSER, App.IsPHICEE, App.IsPHICER, App.IsHDMFEE, App.IsHDMFER,
            App.IsTAXEE, App.IsTAXER, App.IsComputeTax, SetupID
        ]);
    console.log('strSQL: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.GetApplicationSettingsByID = (SetupID, next) => {
    var strSQL = mysql.format('SELECT P.*,MD5(P.SetupID) AS UUID FROM payroll_setup P WHERE MD5(P.SetupID)=? LIMIT 1;', [SetupID]);
    db.query(strSQL, next);
};