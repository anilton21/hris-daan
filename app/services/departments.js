'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');



exports.Add_Departments = (data, next) => {
    var strSQL = mysql.format('INSERT INTO departments(d_name , d_addedby,  d_dateadded,  d_code ,  short_name  , d_head,   branch_id ,  tel_no , fax_no ,  d_desc , inactive) VALUES (?,?, NOW(), ? , ?,?,?,?,?,?,?);', [
        data.d_name, data.CURRENT_USER.UserID , data.d_code, data.short_name, data.d_head, data.branch_id, data.tel_no, data.fax_no, data.d_desc, data.inactive || 0
    ]);
    console.log('strSQL: ',strSQL);
    db.insertWithId(strSQL, next);
};

exports.getAllDepartments = (next) => {
    db.query(mysql.format('SELECT b.*,MD5(b.d_id) as UUID FROM departments as b ;'), next);
};


exports.getDepartmentsById = (dep_id, next) => {
    var strSQL = mysql.format('SELECT * FROM departments WHERE MD5(d_id)=? LIMIT 1;', [dep_id]);
    db.query(strSQL, next);
};


exports.deleteDepartments = (dep_id, next) => {
    var strSQL = mysql.format('DELETE FROM departments WHERE MD5(d_id)=?;', [dep_id]);
    db.actionQuery(strSQL, next);
};

exports.updateDepartments = (dep_id, data, next) => {
    var strSQL = mysql.format('UPDATE departments SET d_name=? , d_modifiedby = ? ,  d_datemodified = CURDATE() , d_code = ? , short_name=? , d_head = ? , branch_id = ? , tel_no = ?,  fax_no = ? , d_desc = ? , inactive = ? WHERE MD5(d_id)=?;', [
        data.d_name,  data.CURRENT_USER.UserID , data.d_code, data.short_name, data.d_head, data.branch_id, data.tel_no, data.fax_no, data.d_desc, data.inactive ,  dep_id
    ]);
    db.actionQuery(strSQL, next);
};