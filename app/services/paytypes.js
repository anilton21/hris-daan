'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');


exports.getAllPayTypes = (next) => {
    db.query(mysql.format('SELECT * FROM paytypes;'), next);
};

exports.updatePayTypeDivisor = (tp_id, divisor, next) => {
    var strSQL = mysql.format('UPDATE paytypes SET tp_divisor=? WHERE tp_id=?;', [divisor, tp_id]);
    db.actionQuery(strSQL, next);
};