'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');



exports.Add_Religion= (data, next) => {
    var strSQL = mysql.format('INSERT INTO religions VALUES(Null,?,?,?);', [
        data.Religion , data. ShortName , data.Description
    ]);
    db.insertWithId(strSQL,next);
};

exports.updateReligion = (religion_id, data, next) => {
    var strSQL = mysql.format('UPDATE religions SET Religion = ? , ShortName = ?, Description =?  WHERE MD5(ReligionID)=?;', [
        data.Religion,data.ShortName , data.Description ,religion_id
    ]);
    db.actionQuery(strSQL, next);
};

exports.getAllReligions = (next)=> {
    db.query(mysql.format('SELECT b.*,MD5(b.ReligionID) as UUID FROM religions as b ;'), next);
};


exports.getReligionById = (religion_id, next) => {
    var strSQL = mysql.format('SELECT * FROM religions WHERE MD5(ReligionID) =? LIMIT 1;', [religion_id]);
    db.query(strSQL, next);
};


exports.deleteReligion = (religion_id, next) => {
    var strSQL = mysql.format('deleteReligion FROM religions WHERE MD5(ReligionID)=?;', [religion_id]);
    db.actionQuery(strSQL, next);
};