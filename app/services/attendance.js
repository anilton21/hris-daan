'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();
var async = require('async');
var _ = require('lodash-node');
var moment = require('moment');


function dateToYMD(date) {
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return '' + y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
}

exports.employeeRawTimelogs = (DeviceNo, dateFrom, dateTo, next) => {
    var newDateFrom = dateToYMD(new Date(dateFrom));
    var newDateTo = dateToYMD(new Date(dateTo));
    var str = mysql.format('SELECT DATE_FORMAT(TL.TimeLog,\'%Y-%m-%d\') formattedTimeLog,SUBSTR(TL.TimeLog, 11, 9) AS formattedLogTime,TL.LogTime,TL.EntryID,TL.TimeLog,MD5(EntryID) AS UUID FROM time_logs TL WHERE ((DATE_FORMAT(TL.TimeLog,\'%Y-%m-%d\')  >= ? AND DATE_FORMAT(TL.TimeLog,\'%Y-%m-%d\') <= ?) OR (DATE_FORMAT(TL.TimeLog,\'%Y-%m-%d\') BETWEEN ? AND ? )) AND TL.EmployeeID= ? ORDER BY TL.TimeLog;', [newDateFrom, newDateTo, newDateFrom, newDateTo, DeviceNo]);
    console.log('str: ', str);
    db.query(str, next);
};

exports.saveEmployeeRawTimeLogs = (DeviceNo, data, next) => {
    /*if (data.TimeLog) {
        data.TimeLog = moment(new Date(data.TimeLog)).format('YYYY-MM-DD HH:mm:ss')
    }*/

    var timelogs = {
        LogDate: data.LogDate,
        TimeLog: data.TimeLog,
        LogTime: data.LogTime,
        LogType: data.LogType,
        device_no: DeviceNo,
        EmployeeID: data.EmployeeID,
        MACID: data.MACID
    };
    var strSQL = mysql.format('INSERT INTO time_logs SET ?', [timelogs]);
    console.log('strSQL: ', strSQL);
    db.insertWithId(strSQL, next);
};

exports.deleteEmployeeRawTimelogs = (EntryID, next) => {
    var strSQL = mysql.format('DELETE FROM time_logs WHERE MD5(EntryID)=?;', [EntryID]);
    console.log('strSQL: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.updateEmployeeRawTimelogs = (EntryID, data, next) => {
    if (data.TimeLog) {
        data.TimeLog = moment(new Date(data.TimeLog)).format('YYYY-MM-DD HH:mm:ss')
    }

    var timelogs = {
        LogDate: data.LogDate,
        TimeLog: data.TimeLog,
        LogTime: data.LogTime,
        LogType: data.LogType,
        device_no: data.device_no,
        EmployeeID: data.EmployeeID,
        MACID: data.MACID
    };
    var strSQL = mysql.format('UPDATE time_logs SET ? WHERE MD5(EntryID)=?;', [timelogs, EntryID]);
    console.log('strSQL: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.GetEmployeeAttendance = (e_idno, PayrollID, next) => {
    var strSQL = mysql.format('SELECT (T.TimeInActualDate) AS TransDate,DAYNAME(T.TimeInActualDate) AS DAYNAME,DAYOFWEEK(T.TimeInActualDate) AS DAYOFWEEK,CAST(DATE_FORMAT(T.TimeInActualDate,"%m") AS SIGNED) AS MONTHID,' +
        'CAST(DATE_FORMAT(T.TimeInActualDate,"%Y") AS SIGNED) AS YEARID,T.TimeIn,DATE_FORMAT(T.TimeIn,"%h:%i:%s %p") AS F_TimeIn, T.TimeOut,DATE_FORMAT(T.TimeOut,"%h:%i:%s %p") AS F_TimeOut, T.TotalLate, T.TotalUnderTime,' +
        'T.TimeIn2,DATE_FORMAT(T.TimeIn2,"%h:%i:%s %p") AS F_TimeIn2, T.TimeOut2,DATE_FORMAT(T.TimeOut2,"%h:%i:%s %p") AS F_TimeOut2, T.TotalLate2, T.TotalUnderTime2,T.IsAbsent, T.IsOnTravel, T.IsNonWorking, T.IsHalfday, T.IsLate, T.isHoliday,T.IsLeave,' +
        'T.Totalhrs, T.Remarks,T.PostedDate,T.EntryID FROM time_keeping T WHERE MD5(T.PayrollID)=? AND MD5(T.EmployeeID)=?;', [PayrollID, e_idno]);
    db.query(strSQL, next);
};

exports.Add_TimeKeeping = (e_idno, PayrollID, T, next) => {

    if (T.TimeInActualDate) {
        T.TimeInActualDate = moment(new Date(T.TimeInActualDate)).format('YYYY-MM-DD')
    }

    if (T.TimeOutActualDate) {
        T.TimeOutActualDate = moment(new Date(T.TimeOutActualDate)).format('YYYY-MM-DD')
    }

    /*if (T.TimeIn) {
        T.TimeIn = moment(T.TimeIn).format('YYYY-MM-DD HH:mm:ss A')
    }

    if (T.TimeOut) {
        T.TimeOut = moment(T.TimeOut).format('YYYY-MM-DD HH:mm:ss A')
    }

    if (T.TimeIn2) {
        T.TimeIn2 = moment(T.TimeIn2).format('YYYY-MM-DD HH:mm:ss A')
    }

    if (T.TimeOut2) {
        T.TimeOut2 = moment(T.TimeOut2).format('YYYY-MM-DD HH:mm:ss A')
    }*/

    var time_keeping = {
        EmployeeID: T.EmployeeID,
        TransDate: new Date(),
        PayrollID: T.PayrollID,
        Year: T.Year,
        Day: T.Day,
        MonthID: T.MonthID,
        TimeIn: T.TimeIn,
        TimeOut: T.TimeOut,
        IsLate: T.IsLate,
        TotalHrs: T.TotalHrs,
        TotalLate: T.TotalLate,
        IsUnderTime: T.IsUnderTime,
        TotalUnderTime: T.TotalUnderTime,
        IsHalfDay: T.IsHalfDay,
        isHoliday: T.isHoliday,
        isAbsent: T.IsAbsent,
        isOnTravel: T.isOnTravel,
        isLeave: T.IsLeave,
        TimeIn2: T.TimeIn2,
        TimeOut2: T.TimeOut2,
        TotalLate2: T.TotalLate2,
        TotalUnderTime2: T.TotalUnderTime2,
        TimeIn3: T.TimeIn3,
        TimeOut3: T.TimeOut3,
        TotalLate3: T.TotalLate3,
        TotalUnderTime3: T.TotalUnderTime3,
        isAdjusted: T.isAdjusted,
        TimeInActualDate: T.TimeInActualDate,
        TimeOutActualDate: T.TimeOutActualDate,
        LastModifieddate: new Date(),
        LastModifiedBy: T.CURRENT_USER.UserID,
        IsNonWorking: T.IsNonWorking,
        Remarks: T.Remarks
    }
    var strSQL = mysql.format('INSERT INTO time_keeping SET ?', [time_keeping]);
    console.log('strSQL: ', strSQL);
    db.insertWithId(strSQL, next);
};

exports.IsTimeAttendancePostedByPayrollTerm = (e_idno, PayrollID, next) => {
    var strSQL = mysql.format('SELECT PostedDate,PostedBy FROM time_keeping WHERE MD5(EmployeeID)=? AND MD5(PayrollID)=? AND (PostedDate IS NOT NULL AND PostedBy IS NOT NULL);', [e_idno, PayrollID]);
    db.query(strSQL, next);
};

exports.Delete_TimeKeeping = (e_idno, PayrollID, next) => {
    var strSQL = mysql.format('DELETE FROM time_keeping WHERE MD5(EmployeeID)=? AND MD5(PayrollID) =?;', [e_idno, PayrollID]);
    db.actionQuery(strSQL, next);
};

exports.Post_TimeKeeping = (e_idno, PayrollID, data, next) => {
    var strSQL = mysql.format('UPDATE time_keeping SET PostedDate=CURDATE(),PostedBy=? WHERE MD5(EmployeeID)=? AND MD5(PayrollID) =?;', [data.CURRENT_USER.UserID, e_idno, PayrollID]);
    console.log('strSQL: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.getAllOvertime = (PayrollID, next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format('SELECT OT.EntryID,OT.RefNo,OT.PayrollID,OT.EmployeeID,fn_EmployeeName(OT.EmployeeID) as EmployeeName,fn_EmployeePositionTitle(OT.EmployeeID) as JobTitle,' +
                'fn_EmployeeStatus(OT.EmployeeID) as EmpStatus,fn_EmployeeDepartmentName(OT.EmployeeID) as department,(SELECT DeptID FROM employees WHERE e_idno = OT.EmployeeID LIMIT 1) AS department_id, ' +
                '(SELECT branch_id FROM employees WHERE e_idno = OT.EmployeeID LIMIT 1) AS branch_id,(SELECT branch_name FROM branch WHERE branch_id = (SELECT branch_id FROM employees WHERE e_idno = OT.EmployeeID LIMIT 1)) as branch_name, ' +
                'DATE_FORMAT(OT.TransDate, "%Y-%M-%d") as DateFiled,OT.TotalNoHrs,MD5(OT.EntryID) AS UUID ' +
                'FROM pay_overtime OT WHERE MD5(PayrollID)=?;', [PayrollID]);
            console.log('strSQL: ', strSQL)
            db.query(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }
                callback(null, response);
            });
        },
        (overtimes, callback) => {
            var strSQL = mysql.format('SELECT PD.* FROM pay_overtime P INNER JOIN pay_overtime_details PD ON P.RefNo = PD.RefNo WHERE MD5(P.PayrollID)=?;', [PayrollID])
            console.log('strSQL: ', strSQL);
            db.query(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }

                _.each(overtimes, (row) => {
                    row.details = _.filter(response, { 'RefNo': row.RefNo }) || [];
                });

                callback(null, overtimes);
            });
        }
    ], next);
};

exports.getAllOvertimeTypes = (next) => {
    var strSQL = mysql.format('SELECT A.*, MD5(OTYPEID) as UUID FROM pay_overtimetypes A;');
    db.query(strSQL, next);
};

exports.Add_Overtime = (OT, next) => {
    var strSQL = mysql.format('INSERT INTO pay_overtime (TransDate,PayrollID,EmployeeID,RefNo) VALUES (NOW(),?,?,?);', [
        OT.PayrollID, OT.EmployeeID, OT.RefNo
    ]);

    if (OT.overtimes && OT.overtimes.length > 0) {
        console.log('strSQL: ', strSQL);
        db.insertWithId(strSQL, (err, overtime) => {
            if (err) {
                next(err, null);
            }

            var overtimDetails = _.map(OT.overtimes, function(row) {
                if (row.InclusiveDates) {
                    row.InclusiveDates = moment(new Date(row.InclusiveDates)).format('YYYY-MM-DD')
                }

                /*if (row.TimeIn) {
                    row.TimeIn = moment(row.TimeIn).format('YYYY-MM-DD HH:mm:ss')
                    console.log('row.TimeIn: ',row.TimeIn)
                }

                if (row.TimeOut) {
                    row.TimeOut = moment(row.TimeOut).format('YYYY-MM-DD HH:mm:ss')
                    console.log('row.TimeOut: ',row.TimeOut)
                }*/

                var newdate = new Date();
                var date = newdate.setDate(newdate.getDate() + 2);
                var OTExpiry = moment(date).format('YYYY-MM-DD')
                return [
                    row.RefNo || null,
                    row.InclusiveDates || null,
                    row.TimeIn,
                    row.TimeOut,
                    row.NoHrs,
                    row.OTType.OTYPEID,
                    row.Project,
                    row.Remarks,
                    row.Requestor.e_id,
                    row.IsPreApproved,
                    row.PreApprovedUserID,
                    row.PreApprovedDate,
                    row.IsApproved,
                    row.ApprovedUserID,
                    row.ApprovedDate,
                    OTExpiry
                ];
            });

            var strSQL1 = mysql.format('INSERT INTO pay_overtime_details(RefNo,InclusiveDates,TimeIn,TimeOut,NoHrs,OTType,Project,Remarks,OTRequestor,IsPreApproved,PreApprovedBy,PreApprovedDate,IsApproved,ApprovedBy,ApprovedDate,OTExpiry) VALUES ?;', [overtimDetails]);
            console.log('strSQL1: ', strSQL1);
            db.insertBulkWithId(strSQL1, function(err, details) {
                if (err) {
                    next(err, null);
                }
                next(null, overtime);
            });
        });
    } else {
        console.log('else strSQL: ', strSQL);
        db.insertWithId(strSQL, next);
    }
};

exports.Update_Overtime = (OT, next) => {
    var strSQL = mysql.format('UPDATE pay_overtime SET TotalNoHrs=? WHERE RefNo=?;', [
        OT.TotalNoHrs, OT.RefNo
    ]);
    if (OT.overtimes && OT.overtimes.length > 0) {
        console.log('strSQL: ', strSQL);
        db.actionQuery(strSQL, (err, overtime) => {
            if (err) {
                next(err, null);
            }

            var overtimDetails = _.map(OT.overtimes, function(row) {
                if (row.InclusiveDates) {
                    row.InclusiveDates = moment(new Date(row.InclusiveDates)).format('YYYY-MM-DD')
                }

                /*if (row.TimeIn) {
                    row.TimeIn = moment(row.TimeIn).format('YYYY-MM-DD HH:mm:ss')
                    console.log('row.TimeIn: ', row.TimeIn)
                }

                if (row.TimeOut) {
                    row.TimeOut = moment(row.TimeOut).format('YYYY-MM-DD HH:mm:ss')
                    console.log('row.TimeOut: ', row.TimeOut)
                }*/

                var newdate = new Date();
                var date = newdate.setDate(newdate.getDate() + 2);
                var OTExpiry = moment(date).format('YYYY-MM-DD')
                if (row.IsApproved == 0) {
                    return [
                        row.RefNo || null,
                        row.InclusiveDates || null,
                        row.TimeIn,
                        row.TimeOut,
                        row.NoHrs,
                        row.OTType.OTYPEID,
                        row.Project,
                        row.Remarks,
                        row.Requestor.e_id,
                        row.IsPreApproved,
                        row.PreApprovedUserID,
                        row.PreApprovedDate,
                        row.IsApproved,
                        row.ApprovedUserID,
                        row.ApprovedDate,                        
                        (_.isEmpty(row.OTExpiry)) ? OTExpiry : row.OTExpiry
                    ];
                }
            });

            if (overtimDetails && overtimDetails.length > 0) {
                var strSQL1 = mysql.format('DELETE FROM pay_overtime_details WHERE (RefNo=? AND (IsApproved=0 AND ApprovedBy IS NULL AND ApprovedDate IS NULL));' +
                    'INSERT INTO pay_overtime_details(RefNo,InclusiveDates,TimeIn,TimeOut,NoHrs,OTType,Project,Remarks,OTRequestor,IsPreApproved,PreApprovedBy,PreApprovedDate,IsApproved,ApprovedBy,ApprovedDate,OTExpiry) VALUES ?;', [OT.RefNo, overtimDetails]);

                console.log('strSQL1: ', strSQL1);
                db.insertBulkWithId(strSQL1, function(err, details) {
                    if (err) {
                        next(err, null);
                    }
                    next(null, overtime);
                });
            } else {
                next(null, overtime);
            }
        });
    } else {
        console.log('else strSQL: ', strSQL);
        db.actionQuery(strSQL, next);
    }
};

exports.OvertimeAlreadyExisted = (RefNo, next) => {
    var strSQL = mysql.format('SELECT * FROM pay_overtime WHERE RefNo=?', [RefNo]);
    db.query(strSQL, next)
};

exports.Add_Overtime_Details = (ot_id, OTD, next) => {
    if (OTD.InclusiveDates) {
        OTD.InclusiveDates = moment(OTD.InclusiveDates).format('YYYY-MM-DD')
    }

    /*if (OTD.TimeIn) {
        OTD.TimeIn = moment(OTD.TimeIn).format('YYYY-MM-DD HH:mm:ss')
    }

    if (OTD.TimeOut) {
        OTD.TimeOut = moment(OTD.TimeOut).format('YYYY-MM-DD HH:mm:ss')
    }*/

    var strSQL = mysql.format('INSERT INTO pay_overtime_details(RefNo,InclusiveDates,TimeIn,TimeOut,NoHrs,OTType,Remarks) VALUES (?,?,?,?,?,?,?);', [
        OTD.RefNo, OTD.InclusiveDates, OTD.TimeIn, OTD.TimeOut, OTD.NoHrs, OTD.OTType, OTD.Remarks
    ]);
    db.query(strSQL, next);
};

exports.Update_OverTime_Details = (emp_id, payroll_id, ot_id, user, OTD, next) => {
    if (OTD.InclusiveDates) {
        OTD.InclusiveDates = moment(OTD.InclusiveDates).format('YYYY-MM-DD')
    }

    if (OTD.OTExpiry) {
        OTD.OTExpiry = moment(OTD.OTExpiry).format('YYYY-MM-DD')
    }

    /*if (OTD.TimeIn) {
        OTD.TimeIn = moment(OTD.TimeIn).format('YYYY-MM-DD HH:mm:ss')
    }

    if (OTD.TimeOut) {
        OTD.TimeOut = moment(OTD.TimeOut).format('YYYY-MM-DD HH:mm:ss')
    }*/

    var OTDetails = {
        TimeIn: OTD.TimeIn,
        TimeOut: OTD.TimeOut,
        NoHrs: OTD.NoHrs,
    };

    var strSQL = mysql.format('UPDATE pay_overtime_details SET ? WHERE IndexID=?;', [
        OTDetails, ot_id
    ]);
    console.log('strSQL: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.Delete_OverTime_Details = (emp_id, payroll_id, ot_id, next) => {
    var strSQL = mysql.format('DELETE FROM pay_overtime_details WHERE IndexID=?;', [ot_id]);
    db.actionQuery(strSQL, next);
};

exports.DeleteOvertime = (refno, next) => {
    var strSQL = mysql.format('DELETE FROM pay_overtime WHERE MD5(EntryID) =?;', [refno]);
    db.query(strSQL, next);
};

exports.DeleteOvertime2 = (refno, next) => {
    var strSQL = mysql.format('DELETE FROM pay_overtime WHERE RefNo =?;', [refno]);
    db.query(strSQL, next);
};

exports.ApprovedOvertime = (refno, user, next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format('UPDATE pay_overtime_details SET IsApproved=1,ApprovedBy =?,ApprovedDate=CURDATE() WHERE RefNo=?;', [
                user.UserID, refno
            ]);
            console.log('strSQL: ', strSQL);
            db.actionQuery(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }
                callback();
            });
        },
        (callback) => {
            var strSQL = mysql.format('SELECT DATE(InclusiveDates) AS InclusiveDates, TimeIn,DATE_FORMAT(TimeIn,"%h:%i %p") AS F_TimeIn,TimeOut,DATE_FORMAT(TimeOut,"%h:%i %p") AS F_TimeOut,NoHrs,OTType,OTD.Project,OTD.Remarks,OTD.OTRequestor,' +
                'OTD.IsPreApproved,DATE(OTD.PreApprovedDate) AS PreApprovedDate,(SELECT FullName FROM users WHERE UserID = OTD.PreApprovedBy LIMIT 1) AS PreApprovedBy,' +
                'OTD.IsApproved,DATE(OTD.ApprovedDate) AS ApprovedDate,(SELECT FullName FROM users WHERE UserID = OTD.ApprovedBy LIMIT 1) AS ApprovedBy,OTD.OTExpiry,OTD.IndexID,OTD.RefNo ' +
                'FROM pay_overtime_details OTD WHERE OTD.RefNo = ?;', [refno]);
            console.log('strSQL: ', strSQL);
            db.query(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }
                callback(null, response);
            });
        }
    ], next);
};

exports.PreApprovedOvertime = (RefNo, user, next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format('UPDATE pay_overtime_details SET IsPreApproved=1,PreApprovedBy =?,PreApprovedDate=CURDATE() WHERE RefNo=?;', [
                user.UserID, RefNo
            ]);
            console.log('strSQL: ', strSQL);
            db.actionQuery(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }
                callback();
            });
        },
        (callback) => {
            var strSQL = mysql.format('SELECT DATE(InclusiveDates) AS InclusiveDates, TimeIn,DATE_FORMAT(TimeIn,"%h:%i %p") AS F_TimeIn,TimeOut,DATE_FORMAT(TimeOut,"%h:%i %p") AS F_TimeOut,NoHrs,OTType,OTD.Project,OTD.Remarks,OTD.OTRequestor,' +
                'OTD.IsPreApproved,DATE(OTD.PreApprovedDate) AS PreApprovedDate,(SELECT FullName FROM users WHERE UserID = OTD.PreApprovedBy LIMIT 1) AS PreApprovedBy,' +
                'OTD.IsApproved,DATE(OTD.ApprovedDate) AS ApprovedDate,(SELECT FullName FROM users WHERE UserID = OTD.ApprovedBy LIMIT 1) AS ApprovedBy,OTD.OTExpiry,OTD.IndexID,OTD.RefNo ' +
                'FROM pay_overtime_details OTD WHERE OTD.RefNo = ?;', [RefNo]);
            console.log('strSQL: ', strSQL);
            db.query(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }
                callback(null, response);
            });
        }
    ], next);
};

exports.ApprovedEmployeeOvertime = (IndexID, user, next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format('UPDATE pay_overtime_details SET IsApproved=1,ApprovedBy =?,ApprovedDate=CURDATE() WHERE IndexID=?;', [
                user.UserID, IndexID
            ]);
            console.log('strSQL: ', strSQL);
            db.actionQuery(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }
                callback();
            });
        },
        (callback) => {
            var strSQL = mysql.format('SELECT DATE(InclusiveDates) AS InclusiveDates, TimeIn,DATE_FORMAT(TimeIn,"%h:%i %p") AS F_TimeIn,TimeOut,DATE_FORMAT(TimeOut,"%h:%i %p") AS F_TimeOut,NoHrs,OTType,OTD.Project,OTD.Remarks,OTD.OTRequestor,' +
                'OTD.IsPreApproved,DATE(OTD.PreApprovedDate) AS PreApprovedDate,(SELECT FullName FROM users WHERE UserID = OTD.PreApprovedBy LIMIT 1) AS PreApprovedBy,' +
                'OTD.IsApproved,DATE(OTD.ApprovedDate) AS ApprovedDate,(SELECT FullName FROM users WHERE UserID = OTD.ApprovedBy LIMIT 1) AS ApprovedBy,OTD.OTExpiry,OTD.IndexID,OTD.RefNo ' +
                'FROM pay_overtime_details OTD WHERE OTD.IndexID = ? LIMIT 1;', [IndexID]);
            console.log('strSQL: ', strSQL);
            db.query(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    response = response[0];
                }
                callback(null, response);
            });
        }
    ], next);
};

exports.PreApprovedEmployeeOvertime = (IndexID, user, next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format('UPDATE pay_overtime_details SET IsPreApproved=1,PreApprovedBy =?,PreApprovedDate=CURDATE() WHERE IndexID=?;', [
                user.UserID, IndexID
            ]);
            console.log('strSQL: ', strSQL);
            db.actionQuery(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }
                callback();
            });
        },
        (callback) => {
            var strSQL = mysql.format('SELECT DATE(InclusiveDates) AS InclusiveDates, TimeIn,TimeOut,NoHrs,OTType,OTD.Project,OTD.Remarks,OTD.OTRequestor,' +
                'OTD.IsPreApproved,DATE(OTD.PreApprovedDate) AS PreApprovedDate,(SELECT FullName FROM users WHERE UserID = OTD.PreApprovedBy LIMIT 1) AS PreApprovedBy,' +
                'OTD.IsApproved,DATE(OTD.ApprovedDate) AS ApprovedDate,(SELECT FullName FROM users WHERE UserID = OTD.ApprovedBy LIMIT 1) AS ApprovedBy,OTD.OTExpiry,OTD.IndexID,OTD.RefNo ' +
                'FROM pay_overtime_details OTD WHERE OTD.IndexID = ? LIMIT 1;', [IndexID]);
            console.log('strSQL: ', strSQL);
            db.query(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    response = response[0];
                }
                callback(null, response);
            });
        }
    ], next);
};

exports.GetOvertimeInfo = (refno, next) => {
    var strSQL = mysql.format('SELECT *,MD5(PayrollID) AS Payroll_UUID,MD5(EmployeeID) AS Emp_UUID FROM pay_overtime WHERE RefNo=? LIMIT 1;', [refno]);
    db.query(strSQL, next);
};

exports.IsOverTimeApproved = (refno, next) => {
    var strSQL = mysql.format('SELECT * FROM pay_overtime_details WHERE (RefNo=? AND (IsApproved=1 AND ApprovedBy IS NOT NULL AND ApprovedDate IS NOT NULL)) LIMIT 1;', [refno]);
    db.query(strSQL, next);
};

exports.IsOverTimePreApproved = (refno, next) => {
    var strSQL = mysql.format('SELECT * FROM pay_overtime_details WHERE (RefNo=? AND (IsPreApproved=1 AND PreApprovedBy IS NOT NULL AND PreApprovedDate IS NOT NULL)) LIMIT 1;', [refno]);
    db.query(strSQL, next);
};

exports.IsOverTimeApproved2 = (IndexID, next) => {
    var strSQL = mysql.format('SELECT * FROM pay_overtime_details WHERE (IndexID=? AND (IsApproved=1 AND ApprovedBy IS NOT NULL AND ApprovedDate IS NOT NULL)) LIMIT 1;', [IndexID]);
    db.query(strSQL, next);
};

exports.IsOverTimePreApproved2 = (IndexID, next) => {
    var strSQL = mysql.format('SELECT * FROM pay_overtime_details WHERE (IndexID=? AND (IsPreApproved=1 AND PreApprovedBy IS NOT NULL AND PreApprovedDate IS NOT NULL)) LIMIT 1;', [IndexID]);
    db.query(strSQL, next);
};

exports.IsOverTimeApproved3 = (EntryID, next) => {
    var strSQL = mysql.format('SELECT * FROM pay_overtime OT INNER JOIN pay_overtime_details OTD ON OT.RefNo = OTD.RefNo WHERE (MD5(EntryID)=? AND (IsApproved=1 AND ApprovedBy IS NOT NULL AND ApprovedDate IS NOT NULL)) LIMIT 1;', [EntryID]);
    console.log('strSQL: ', strSQL);
    db.query(strSQL, next);
};

exports.getEmployeeOvertime = (emp_id, payroll_id, next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format('SELECT OT.EntryID,OT.RefNo,OT.PayrollID,OT.EmployeeID,fn_EmployeeName(OT.EmployeeID) as EmployeeName,fn_EmployeePositionTitle(OT.EmployeeID) as JobTitle,fn_EmployeeStatus(OT.EmployeeID) as EmpStatus,DATE_FORMAT(OT.TransDate, "%Y-%M-%d") as DateFiled,OT.TransDate,MD5(OT.EntryID) AS OT_UUID ' +
                'FROM pay_overtime OT WHERE MD5(PayrollID)=? AND MD5(OT.EmployeeID)=?;', [payroll_id, emp_id]);
            console.log('strSQL: ', strSQL);
            db.query(strSQL, (err, overtimes) => {
                if (err) {
                    next(err, null);
                }
                callback(null, overtimes);
            });
        },
        (overtimes, callback) => {
            var strSQL = mysql.format('SELECT A.*, MD5(OTYPEID) as UUID FROM pay_overtimetypes A;');
            db.query(strSQL, (err, types) => {
                if (err) {
                    next(err, null);
                }
                callback(null, overtimes, types);
            });
        },
        (overtimes, types, callback) => {
            var str = 'SELECT E.e_id,E.e_idno, fn_EmployeeName(E.e_idno) AS employee_name,E.e_fname,E.e_lname,E.e_mname,E.e_suffix,E.e_gender, fn_EmployeePositionTitle(E.e_idno) as job_position,fn_EmployeeDepartmentName(E.e_idno) as department,' +
                'fn_EmploymentStatus(E.s_id) as employment_status,E.Inactive,E.DateResigned,E.DateRetired,MD5(E.e_id) AS UUID,MD5(E.e_idno) as Emp_UUID FROM employees E ORDER BY E.e_id ';
            var strSQL = mysql.format(str);
            db.query(strSQL, (err, employees) => {
                if (err) {
                    next(err, null);
                }
                callback(null, overtimes, types, employees);
            });
        },
        (overtimes, types, employees, callback) => {
            var strSQL = mysql.format('SELECT DATE(InclusiveDates) AS InclusiveDates, TimeIn,TimeOut,NoHrs,OTType,OTD.Project,OTD.Remarks,OTD.OTRequestor,OTD.IndexID,OT.RefNo,MD5(OT.EntryID) AS OT_UUID,' +
                'OTD.IsPreApproved,DATE(OTD.PreApprovedDate) AS PreApprovedDate,(SELECT FullName FROM users WHERE UserID = OTD.PreApprovedBy LIMIT 1) AS PreApprovedBy,OTD.PreApprovedBy AS PreApprovedUserID,' +
                'OTD.IsApproved,DATE(OTD.ApprovedDate) AS ApprovedDate,(SELECT FullName FROM users WHERE UserID = OTD.ApprovedBy LIMIT 1) AS ApprovedBy,OTD.ApprovedBy AS ApprovedUserID,OTD.OTExpiry ' +
                'FROM pay_overtime OT INNER  JOIN pay_overtime_details OTD ON OT.RefNo= OTD.RefNo WHERE MD5(OT.PayrollID)=? AND MD5(OT.EmployeeID)=?;', [payroll_id, emp_id]);
            console.log('strSQL: ', strSQL);
            db.query(strSQL, (err, details) => {
                if (err) {
                    next(err, null);
                }
                _.each(details, (row) => {
                    row.OTTypeID = row.OTType;
                    row.OTType = _.find(types, {
                        'OTYPEID': row.OTTypeID
                    }) || {};
                    row.Requestor = _.find(employees, { 'e_id': row.OTRequestor }) || {};
                });

                _.each(overtimes, (row) => {
                    row.details = _.filter(details, {
                        'RefNo': row.RefNo
                    }) || [];
                });
                callback(null, overtimes);
            });
        }
    ], next);
};