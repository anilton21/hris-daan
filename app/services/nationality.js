'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');

exports.Add_Nationality = (data, next) => {
    var strSQL = mysql.format('INSERT INTO nationality VALUES(null,?,?,?);', [
        data.Nationality, data.ShortName, data.Description
    ]);
    db.insertWithId(strSQL,next);
};

exports.getNationalityById = (nat_id, next) => {
    var strSQL = mysql.format('SELECT * FROM nationality WHERE  MD5(NationalityID)=? LIMIT 1;', [nat_id]);
    db.query(strSQL, next);
};

exports.getAllNationality = (next) => {
    db.query(mysql.format('SELECT b.*,MD5(b.NationalityID) as UUID FROM nationality as b '), next);
};

exports.getNationalityById = (nat_id, next) => {
    var strSQL = mysql.format('SELECT * FROM nationality WHERE MD5(NationalityID)=? LIMIT 1;', [nat_id]);
    db.query(strSQL, next);
};

exports.findNationalityByName = (name, next) => {
    var strSQL = mysql.format('SELECT * FROM nationality WHERE Nationality=? LIMIT 1;', [name]);
    db.query(strSQL, next);
};

exports.deleteNationality = (nat_id, next) => {
    var strSQL = mysql.format('DELETE FROM nationality WHERE MD5(NationalityID)=?;', [nat_id]);
    db.actionQuery(strSQL, next);
};

exports.updateNationality = (nat_id, data, next) => {
    var strSQL = mysql.format('UPDATE nationality SET Nationality=?, ShortName=?, Description=? WHERE MD5(NationalityID)=?;', [
        data.Nationality, data.ShortName, data.Description, nat_id
    ]);
    db.actionQuery(strSQL, next);
};