'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');


exports.getAllTaxExemptions = (next) => {
    db.query(mysql.format('SELECT *,MD5(IndexID) as UUID FROM exemption;'), next);
};

exports.Add_Tax_Exemption = (EX, next) => {
    var strSQL = mysql.format('INSERT INTO exemption(ExemptionCode,Exemption,TaxExemption,Inactive) VALUES (?,?,?,?);', [
        EX.ExemptionCode, EX.Exemption, EX.TaxExemption, EX.Inactive || 0
    ]);
    db.insertWithId(strSQL, next);
};

exports.Update_Tax_Exemption = (IndexID, EX, next) => {
    var strSQL = mysql.format('UPDATE exemption SET ExemptionCode=?,Exemption=?,TaxExemption=?,Inactive=? WHERE MD5(IndexID)=?;', [
        EX.ExemptionCode, EX.Exemption, EX.TaxExemption, EX.Inactive || 0, IndexID
    ]);
    db.insertWithId(strSQL, next);
};

exports.Delete_Tax_Exemption = (IndexID,next)=>{
    var strSQL = mysql.format('DELETE FROM exemption WHERE MD5(IndexID)=?;',[IndexID]);
    db.actionQuery(strSQL,next);
};

exports.GetExemptionByID = (IndexID,next)=>{
    var strSQL = mysql.format('SELECT *,MD5(IndexID) as UUID FROM exemption WHERE MD5(IndexID) =? LIMIT 1;',[IndexID]);
    db.query(strSQL,next);
};

