'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');


exports.Add_Job_Position = (data, next) => {
    console.log('strSQL:', strSQL)
     var strSQL = mysql.format('INSERT INTO job_titles(jt_name ,jt_addedby, jt_dateadded,  jt_code ,   jt_shortname ,  jt_desc,    jt_classid ,    jt_groupid , inactive) VALUES (?,?,NOW(),?,?,?,?,?,?);', [
        data.jt_name, data.CURRENT_USER.UserID  , data.jt_code, data.jt_shortname, data.jt_desc, data.jt_classid, data.jt_groupid, data.inactive
    ]);
    db.insertWithId(strSQL, next);
};

exports.updateJobPosition = (jp_id, data, next) => {
    var strSQL = mysql.format('UPDATE job_titles SET jt_name=? , jt_modifiedby =? , jt_datemodified= CURDATE() , jt_code = ? , jt_shortname = ? , jt_desc =? , jt_classid = ? , jt_groupid=? , inactive = ?  WHERE MD5(jt_id) =?;', [
        data.jt_name,  data.CURRENT_USER.UserID ,  data.jt_code, data.jt_shortname, data.jt_desc, data.jt_classid, data.jt_groupid, data.inactive, jp_id

    ]);
    db.actionQuery(strSQL, next);
};

exports.Add_Group = (data, next) => {
   var strSQL = mysql.format('INSERT INTO group_position(GroupCode , GroupName, Description, Inactive , CreatedBy , dateCreated ) VALUES (?,?,?,?,? , NOW());', [
        data.GroupCode, data.GroupName, data.Description, data.Inactive, data.CURRENT_USER.UserID 
    ]);
    db.insertWithId(strSQL, next);
};

exports.updateGroup = (GroupID, data, next) => {
    var strSQL = mysql.format('UPDATE group_position SET GroupCode=? , GroupName = ? , Description = ? , Inactive =? ,  ModifiedBy = ? , DateModified = CURDATE()  WHERE GroupID =?;', [
        data.GroupCode, data.GroupName, data.Description || '', data.Inactive,  data.CURRENT_USER.UserID  , GroupID
    ]);
    db.actionQuery(strSQL, next);
};

exports.updateClass = (IndexID, data, next) => {
    var strSQL = mysql.format('UPDATE evaluation_classification SET ClassCode=? , Classification = ? , Description = ? , Inactive =? ,  ModifiedBy = ? , DateModified = CURDATE()  WHERE IndexID =?;', [
        data.ClassCode, data.Classification, data.Description || '', data.Inactive,  data.CURRENT_USER.UserID ,  IndexID
    ]);
    db.actionQuery(strSQL, next);
};
exports.Add_Class = (data, next) => {
   var strSQL = mysql.format('INSERT INTO evaluation_classification(ClassCode , Classification, Description, Inactive , CreatedBy , dateCreated ) VALUES (?,?,?,?,?, NOW());', [
        data.ClassCode, data.Classification, data.Description || '', data.Inactive, data.CURRENT_USER.UserID 
    ]);
    db.insertWithId(strSQL, next);
};

exports.getAllJobPosition = (next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format('SELECT A.*,MD5(A.jt_id) AS UUID FROM job_titles A;')
            db.query(strSQL, (err, job_titles) => {
                if (err) {
                    next(err, null);
                }
                callback(null, job_titles);
            });
        },
        (job_titles, callback) => {
            var strSQL = mysql.format('SELECT * FROM group_position G;');
            db.query(strSQL, (err, group_position) => {
                if (err) {
                    next(err, null);
                }
                _.each(job_titles, (row) => {

                    row.groupdata = _.find(group_position, {
                        'GroupID': row.jt_groupid
                    }) || {};
                });
                callback(null, job_titles);
            });
        },
        (job_titles, callback) => {
            var strSQL = mysql.format('SELECT * FROM evaluation_classification C;');
            db.query(strSQL, (err, evaluation_classification) => {
                if (err) {
                    next(err, null);
                }
                _.each(job_titles, (row) => {
                    row.classdata = _.find(evaluation_classification, {
                        'IndexID': row.jt_classid
                    }) || {};
                });
                callback(null, job_titles);
            });
        },
    ], next);
};

exports.getAllJobClassification = (next) => {
    db.query(mysql.format('SELECT *, MD5(IndexID)AS UUID FROM evaluation_classification;'), next);
};

exports.getAllGroup = (next) => {
    db.query(mysql.format('Select *, MD5(GroupID) AS UUID from group_position;'), next);
};


exports.deleteGroup = (group_id, next) => {
    var strSQL = mysql.format('DELETE FROM group_position WHERE MD5(GroupID)=?;', [group_id]);
    db.actionQuery(strSQL, next);
};


exports.deleteClass = (jclass_id, next) => {
    var strSQL = mysql.format('DELETE FROM evaluation_classification WHERE MD5(IndexID)=?;', [jclass_id]);
    db.actionQuery(strSQL, next);
};

exports.getClassById = (jclass_id, next) => {
    var strSQL = mysql.format('SELECT *, MD5(IndexID) AS UUID  FROM evaluation_classification WHERE MD5(IndexID)=? LIMIT 1;', [jclass_id]);
    db.query(strSQL, next);
};

exports.getAllClass = (next) => {
    db.query(mysql.format('Select *, MD5(IndexID) AS UUID from evaluation_classification;'), next);
};

exports.getJobPositionById = (jp_id, next) => {
    var strSQL = mysql.format('SELECT *, MD5(jt_id) as UUID FROM job_titles WHERE  md5(jt_id)=? LIMIT 1;', [jp_id]);
    db.query(strSQL, next);
};

exports.getGroupById = (group_id, next) => {
    var strSQL = mysql.format('SELECT * FROM group_position WHERE MD5(GroupID)=? LIMIT 1;', [group_id]);
    db.query(strSQL, next);
};

exports.deleteJobPosition = (jp_id, next) => {
    var strSQL = mysql.format('DELETE FROM job_titles WHERE MD5(jt_id)=?;', [jp_id]);
    db.actionQuery(strSQL, next);
};