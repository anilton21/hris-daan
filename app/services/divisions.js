'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');


exports.getAllDivisions = (next) => {
  db.query(mysql.format('SELECT b.*,MD5(b.div_id) as UUID FROM divisions as b '), next);

}

exports.Add_Divisions = (data, next) => {
    var strSQL = mysql.format('INSERT INTO divisions VALUES(null,?,?,?,?);', [
        data.div_code, data.div_name, data.div_desc, data.inactive
    ]);
    db.insertWithId(strSQL, next);
};

exports.getDivisionsById = (div_id, next) => {
    var strSQL = mysql.format('SELECT * FROM divisions WHERE MD5(div_id)=? LIMIT 1;', [div_id]);
    db.query(strSQL, next);
};

exports.deleteDivision = (div_id, next) => {
    var strSQL = mysql.format('DELETE FROM divisions WHERE MD5(div_id)=?;', [div_id]);
    db.actionQuery(strSQL, next);
};


exports.updateDivision = (div_id, data, next) => {
    var strSQL = mysql.format('UPDATE divisions SET div_code=? , div_name = ? , div_desc = ? , inactive=? WHERE MD5(div_id)=?;', [
        data.div_code, data.div_name, data.div_desc, data.inactive, div_id
    ]);
    db.actionQuery(strSQL, next);
};