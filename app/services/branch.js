'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');

exports.updateBranch = (branch_id, data, next) => {
    var strSQL = mysql.format('UPDATE branch SET branch_code=? , branch_name = ? , short_name = ? , address =? , inactive=? WHERE MD5(branch_id)=?;', [
        data.branch_code, data.branch_name, data.short_name, data.address, data.inactive, branch_id
    ]);
    db.actionQuery(strSQL, next);
};

exports.Add_Branch = (data, next) => {
    var strSQL = mysql.format('INSERT INTO branch VALUES(Null,?,?,?,?,?);', [
        data.branch_code, data.branch_name, data.short_name, data.address, data.inactive
    ]);
    db.insertWithId(strSQL, next);
};

exports.getAllBranch = (next) => {
    db.query(mysql.format('SELECT b.*,MD5(b.branch_id) as UUID FROM branch as b ;'), next);
};


exports.deleteBranch = (branch_id, next) => {
    db.query(mysql.format('DELETE FROM branch WHERE MD5(branch_id)=?;', [branch_id]), next);

};


exports.getBranchById = (branch_id, next) => {
    var strSQL = mysql.format('SELECT * FROM branch WHERE MD5(branch_id) =? LIMIT 1;', [branch_id]);
    console.log('strSQL;', strSQL)
    db.query(strSQL, next);
};