'use strict';

var cb = require('./../../utils/callback');
var employmentStatusCtrl = require('../../controllers/employmentstatus').EmploymentStatus;
var employmentstatus = new employmentStatusCtrl();

exports.getAllEmploymentStatus = (req, res) => {
    employmentstatus.getAllEmploymentStatus(cb.setupResponseCallback(res));
};


exports.getEmploymentStatusById = (req, res) => {
    employmentstatus.getEmploymentStatusById(req.params.empstat_id, cb.setupResponseCallback(res));
};


exports.Add_Employstat = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        employmentstatus.Add_Employstat(req.body, cb.setupResponseCallback(res));
    } else {

        res.sendStatus(401);
    }
};

exports.deleteEmploymentStatus = (req, res) => {
    employmentstatus.deleteEmploymentStatus(req.params.empstat_id, cb.setupResponseCallback(res));
};

exports.updateEmploymentStatus = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        employmentstatus.updateEmploymentStatus(req.params.empstat_id, req.body, cb.setupResponseCallback(res));
    } else {

        res.sendStatus(401);
    }
};