'use strict';

var cb = require('./../../utils/callback');
var religionsCtrl = require('../../controllers/religions').Religions;
var religions = new religionsCtrl();



exports.Add_Religion = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        religions.Add_Religion(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.updateReligion = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        religions.updateReligion(req.params.religion_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getAllReligions = (req, res) => {
    religions.getAllReligions(cb.setupResponseCallback(res));
};


exports.getReligionById = (req, res) => {
    religions.getReligionById(req.params.religion_id, cb.setupResponseCallback(res));
};

exports.deleteReligion = (req, res) => {
    religions.deleteReligion(req.params.religion_id, cb.setupResponseCallback(res));
};