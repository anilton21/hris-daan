'use strict';

var cb = require('./../../utils/callback');
var nationalityCtrl = require('../../controllers/nationality').Nationality;
var nationality = new nationalityCtrl();


exports.Add_Nationality = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        nationality.Add_Nationality(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getAllNationality = (req, res) => {
    nationality.getAllNationality(cb.setupResponseCallback(res));
};

exports.getNationalityById = (req, res) => {
    nationality.getNationalityById(req.params.nat_id, cb.setupResponseCallback(res));
};

exports.deleteNationality = (req, res) => {
    nationality.deleteNationality(req.params.nat_id, cb.setupResponseCallback(res));
};

exports.updateNationality = (req, res) => {
    if (req.user) {
        nationality.updateNationality(req.params.nat_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sentStatus(401);
    }
};