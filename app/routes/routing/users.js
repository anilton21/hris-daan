'use strict';

var cb = require('./../../utils/callback');
var usersctrl = require('../../controllers/users').Users;
var users = new usersctrl();

exports.updateLogDate = function(req, res) {
    var data = {};
    var DateNow = new Date(Date.now());
    data.log_inDate = DateNow.toISOString();
    if (req.user) {

    } else {
        res.send('0');
    }
};

exports.logout = function(req, res) {
    req.logout();
    res.status(200).json({
        response: {
            msg: 'User successfully logout',
            success: true,
            result: null,
        },
        statusCode: 200
    }).end();
};

exports.login = function(req, res) {
    if (req.isAuthenticated()) {
        console.log('login at if');
        res.status(200).json({
            response: req.user,
            statusCode: 200
        });
    } else {
        console.log('login at else');
        res.sendStatus(401);
    }
};

exports.IsUserExisted = function(req, res) {
    users.IsUserExisted(req.params.id, cb.setupResponseCallback(res));
};

exports.Add_User = (req, res) => {
    if (req.user) {
        req.body.UserID = req.user.result.user.UserID;
        users.Add_User(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.Update_User = (req, res) => {
    if (req.query) {
        if (req.query == 'password') {
            users.Update_User_Password(req.params.user_id, req.body, cb.setupResponseCallback(res));
        } else if (req.query == 'usergroup') {
            users.Update_Users_Group(req.params.user_id, req.body, cb.setupResponseCallback(res));
        } else {
            if (req.user) {
                req.body.UserID = req.user.result.user.UserID;
                users.Update_User(req.params.user_id, req.body, cb.setupResponseCallback(res));
            } else {
                res.sendStatus(401);
            }
        }
    } else {
        if (req.user) {
            req.body.UserID = req.user.result.user.UserID;
            users.Update_User(req.params.user_id, req.body, cb.setupResponseCallback(res));
        } else {
            res.sendStatus(401);
        }
    }
};

exports.GetAllUsers = (req, res) => {
    users.GetAllUsers(cb.setupResponseCallback(res));
};

exports.GetUserByUserID = (req, res) => {
    users.GetUserByUserID(req.params.user_id, cb.setupResponseCallback(res));
};

exports.Delete_User = (req, res) => {
    users.Delete_User(req.params.user_id, cb.setupResponseCallback(res));
};

exports.setUserPrivilege = (req, res) => {
    users.setUserPrivilege(req.params.user_id, req.body, cb.setupResponseCallback(res));
};

exports.getUserPrivilege = (req, res) => {
    users.getUserPrivilege(req.params.user_id, cb.setupResponseCallback(res));
};

exports.setUserGroup = (req,res)=>{
    users.setUserGroup(req.params.user_id,req.body,cb.setupResponseCallback(res));
};


exports.Add_User_Group = (req, res) => {
    users.Add_User_Group(req.body, cb.setupResponseCallback(res));
};

exports.Update_User_Group = (req, res) => {
    users.Update_User_Group(req.params.group_id, req.body, cb.setupResponseCallback(res));
};

exports.Delete_User_Group = (req, res) => {
    users.Delete_User_Group(req.params.group_id, cb.setupResponseCallback(res));
};

exports.GetAllUserGroups = (req, res) => {
    users.GetAllUserGroups(cb.setupResponseCallback(res));
};

exports.GetUserGroupByID = (req, res) => {
    users.GetUserGroupByID(req.params.group_id, cb.setupResponseCallback(res));
};

exports.setGroupPrivilege = (req, res) => {
    users.setGroupPrivilege(req.params.group_id, req.body, cb.setupResponseCallback(res));
};

exports.getGroupPrivilege = (req,res)=>{
    users.getGroupPrivilege(req.params.group_id,cb.setupResponseCallback(res));
};



exports.PopulateAccessPriviledge = (req, res) => {
    users.PopulateAccessPriviledge(cb.setupResponseCallback(res));
};

exports.forgotPassword = (req, res) => {
    users.forgotPassword(req.body, cb.setupResponseCallback(res));
};