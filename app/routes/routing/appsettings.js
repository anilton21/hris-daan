'use strict';

var cb = require('./../../utils/callback');
var appSettingsCtrl = require('../../controllers/appsettings').AppSettings;
var appsettings = new appSettingsCtrl();


exports.GetAllApplicationSettings= (req,res)=>{
	appsettings.GetAllApplicationSettings(cb.setupResponseCallback(res));
};

exports.GetApplicationSettingsByID = (req,res)=>{
    appsettings.GetApplicationSettingsByID(req.params.setup_id,cb.setupResponseCallback(res));
};

exports.Delete_Application_Settings = (req,res)=>{
    appsettings.Delete_Application_Settings(req.params.setup_id,cb.setupResponseCallback(res));
};

exports.Activate_Settings = (req,res)=>{
    appsettings.Activate_Settings(req.params.setup_id,cb.setupResponseCallback(res));
};

exports.Add_Application_Settings = (req,res)=>{
    appsettings.Add_Application_Settings(req.body,cb.setupResponseCallback(res));
};

exports.Update_Application_Settings = (req,res)=>{
    appsettings.Update_Application_Settings(req.params.setup_id,req.body,cb.setupResponseCallback(res));
};