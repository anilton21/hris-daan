'use strict';

var cb = require('./../../utils/callback');
var divisionCtrl = require('../../controllers/divisions').Divisions;
var divisions = new divisionCtrl();

exports.getAllDivisions = (req, res) => {
    divisions.getAllDivisions(cb.setupResponseCallback(res));
};


exports.Add_Divisions = (req, res) => {
        divisions.Add_Divisions(req.body, cb.setupResponseCallback(res));
   
};

exports.getDivisionsById = (req, res) => {
    divisions.getDivisionsById(req.params.div_id, cb.setupResponseCallback(res));
};


exports.deleteDivision = (req, res) => {
    divisions.deleteDivision(req.params.div_id, cb.setupResponseCallback(res));
};


exports.updateDivision = (req, res) => {
        divisions.updateDivision(req.params.div_id, req.body, cb.setupResponseCallback(res));
    
};