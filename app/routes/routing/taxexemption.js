'use strict';

var cb = require('./../../utils/callback');
var taxexemptionCtrl = require('../../controllers/taxexemption').TaxExempt;
var taxexemption = new taxexemptionCtrl();

exports.getAllTaxExemptions = (req, res) => {
    taxexemption.getAllTaxExemptions(cb.setupResponseCallback(res));
};

exports.GetExemptionByID = (req, res) => {
    taxexemption.GetExemptionByID(req.params.index_id, cb.setupResponseCallback(res));
};

exports.Add_Tax_Exemption = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        taxexemption.Add_Tax_Exemption(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.Update_Tax_Exemption = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        taxexemption.Update_Tax_Exemption(req.params.index_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.Delete_Tax_Exemption = (req, res) => {
    taxexemption.Delete_Tax_Exemption(req.params.index_id, cb.setupResponseCallback(res));
};