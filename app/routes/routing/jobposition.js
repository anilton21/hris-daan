'use strict';

var cb = require('./../../utils/callback');
var jobpositionCtrl = require('../../controllers/jobposition').JobPosition;
var jobposition = new jobpositionCtrl();

exports.getAllJobPosition = (req, res) => {
    jobposition.getAllJobPosition(cb.setupResponseCallback(res));
};
exports.deleteGroup = (req, res) => {
    jobposition.deleteGroup(req.params.group_id, cb.setupResponseCallback(res));
};

exports.Add_Group = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        jobposition.Add_Group(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};
exports.Add_Class = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        jobposition.Add_Class(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getGroupById = (req, res) => {
    jobposition.getGroupById(req.params.group_id, cb.setupResponseCallback(res));
};

exports.getClassById = (req, res) => {
    jobposition.getClassById(req.params.jclass_id, cb.setupResponseCallback(res));

};

exports.updateClass = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        jobposition.updateClass(req.params.jclass_id, req.body, cb.setupResponseCallback(res));
    } else {

        res.sendStatus(401);
    }
};


exports.deleteClass = (req, res) => {
    jobposition.deleteClass(req.params.jclass_id, cb.setupResponseCallback(res));
};


exports.getAllGroup = (req, res) => {
    jobposition.getAllGroup(cb.setupResponseCallback(res));
};

exports.getAllClass = (req, res) => {
    jobposition.getAllClass(cb.setupResponseCallback(res));
};
exports.getJobPositionById = (req, res) => {
    jobposition.getJobPositionById(req.params.jp_id, cb.setupResponseCallback(res));
};

exports.updateJobPosition = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        jobposition.updateJobPosition(req.params.jp_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.updateGroup = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        jobposition.updateGroup(req.params.group_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.deleteJobPosition = (req, res) => {
    jobposition.deleteJobPosition(req.params.jp_id, cb.setupResponseCallback(res));
};

exports.Add_Job_Position = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        jobposition.Add_Job_Position(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getJobPositionGroup = (req, res) => {
    jobposition.getJobPositionGroup(req.params.jp_id, cb.setupResponseCallback(res));
};

exports.getJobPositionClassification = (req, res) => {
    jobposition.getJobPositionClassification(req.params.jp_id, cb.setupResponseCallback(res));
};