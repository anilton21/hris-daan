'use strict';

var cb = require('./../../utils/callback');
var branchCtrl = require('../../controllers/branch').Branch;
var branch = new branchCtrl();



exports.updateBranch = (req, res) => {
    branch.updateBranch(req.params.branch_id, req.body, cb.setupResponseCallback(res));
};

exports.getAllBranch = (req, res) => {
    branch.getAllBranch(cb.setupResponseCallback(res));
};

exports.Add_Branch = (req, res) => {
    branch.Add_Branch(req.body, cb.setupResponseCallback(res));
};

exports.deleteBranch = (req, res) => {
    branch.deleteBranch(req.params.branch_id, cb.setupResponseCallback(res));
};
exports.getBranchById = (req, res) => {
    branch.getBranchById(req.params.branch_id, cb.setupResponseCallback(res));
}