/*jshint camelcase: false */

'use strict';

var validatorUsers = require('../validation/user');
var validatorEmployee = require('../validation/employee');
var validatorSharer = require('../validation/email');

// ======================== SETUP MANAGER VALIDATIONS ============================ //

var validatorCompany = require('../validation/company');
var validatorBranch = require('../validation/branch');
var validatorDivision = require('../validation/division');
var validatorDepartment = require('../validation/department');
var validatorJobTitle = require('../validation/jobtitle');
var validatorNationality = require('../validation/nationality');
var validatorEmploymentStatus = require('../validation/employmentstatus');
var validatorReligion = require('../validation/religion');
var validatorEmploymentStatus = require('../validation/employmentstatus');
var validatorCivilStatus = require('../validation/civilstatus');

// ======================== ACCOUNTING VALIDATIONS ============================ //

var validatorSSS =  require('../validation/sss');
var validatorHDMF =  require('../validation/hdmf');
var validatorPHIC =  require('../validation/phic');
var validatorAccounts = require('../validation/accounts');
var validatorTaxExemption = require('../validation/taxexemption');
var validatorTAX = require('../validation/tax');

/*var validatorWTAX =  require('../validation/wtax');
var validatorPHIC =  require('../validation/phic');
*/




var users = require('./routing/users');
var payroll = require('./routing/payroll');
var paytypes = require('./routing/paytypes');
var attendance = require('./routing/attendance');
var employees = require('./routing/employees');
var civilstatus = require('./routing/civilstatus');
var employmentstatus = require('./routing/employmentstatus');
var departments = require('./routing/departments');
var branch = require('./routing/branch');
var divisions = require('./routing/divisions');
var taxexemption = require('./routing/taxexemption');
var religions = require('./routing/religions');
var jobposition = require('./routing/jobposition');
var nationality = require('./routing/nationality');
var company = require('./routing/company');
var workschedules = require('./routing/workschedules');
var appsettings = require('./routing/appsettings');
var sss = require('./routing/sss');
var pagibig = require('./routing/pagibig');
var phic = require('./routing/philhealth');
var wtax = require('./routing/tax');
var accounts = require('./routing/accounts');
var sharer = require('./routing/email');



module.exports = function(app, passport, config, middleware) {

    app.route('/')
        .get(function onRequest(req, res) {
            res.render('index');
        });

    app.route('/kiosk')
        .get(function onRequest(req, res) {
            res.render('kiosk');
        });

    app.route('/hris')
        .get(function onRequest(req, res) {
            res.render('hris');
        });

    // ======================== AUTHENTICATION ============================ //
    app.route(config.api_version + '/login')
        .post(validatorUsers.validateBasicAuth, passport.authenticate('user'), users.login);

    app.route(config.api_version + '/logout')
        .get(users.logout);

    app.route(config.api_version + '/hris')
        .post(validatorUsers.validateBasicAuth, passport.authenticate('hris'), users.login);

    // ======================== SHARE ============================ //
    app.route(config.api_version + '/shareMail')
        .post(validatorSharer.validateShare, sharer.shareExperience);
    app.route(config.api_version + '/sendMail')
        .post(validatorSharer.validateSend, sharer.sendMail);

    // ======================== Forgot Password ============================ //
    app.route(config.api_version + '/forgotpassword')
        .post(validatorUsers.validateForgot, users.forgotPassword);


    // ======================== USERS ============================ //

    app.route(config.api_version + '/users/:id/isuserexisted')
        .get(middleware.authorizeBasicAuth, users.IsUserExisted);

    app.route(config.api_version + '/useraccounts')
        .post(middleware.authorizeBasicAuth,validatorUsers.validateLogin, users.Add_User)
        .get(middleware.authorizeBasicAuth, users.GetAllUsers);
    app.route(config.api_version + '/useraccounts/:user_id')
        .delete(middleware.authorizeBasicAuth, users.Delete_User)
        .put(middleware.authorizeBasicAuth,validatorUsers.validateLogin, users.Update_User)
        .get(middleware.authorizeBasicAuth, users.GetUserByUserID);
    app.route(config.api_version + '/useraccounts/:user_id/access')
        .put(middleware.authorizeBasicAuth, users.setUserPrivilege)
        .get(middleware.authorizeBasicAuth, users.getUserPrivilege);
    app.route(config.api_version + '/useraccounts/:user_id/group')
        .put(middleware.authorizeBasicAuth, users.setUserGroup);

    app.route(config.api_version + '/usergroups')
        .post(middleware.authorizeBasicAuth, users.Add_User_Group)
        .get(middleware.authorizeBasicAuth, users.GetAllUserGroups);
    app.route(config.api_version + '/usergroups/:group_id')
        .get(middleware.authorizeBasicAuth, users.GetUserGroupByID)
        .put(middleware.authorizeBasicAuth, users.Update_User_Group)
        .delete(middleware.authorizeBasicAuth, users.Delete_User_Group);
    app.route(config.api_version + '/usergroups/:group_id/access')
        .put(middleware.authorizeBasicAuth, users.setGroupPrivilege)
        .get(middleware.authorizeBasicAuth, users.getGroupPrivilege);

    // ======================== MODULES ============================ //
    // ======================== MODULES ============================ //
    // ======================== MODULES ============================ //

    app.route(config.api_version + '/privileges')
        .get(middleware.authorizeBasicAuth, users.PopulateAccessPriviledge);


    // ======================== SETUP MANAGER ============================ //
    // ======================== SETUP MANAGER ============================ //
    // ======================== SETUP MANAGER ============================ //
    app.route(config.api_version + '/civilstatus')
        .get(middleware.authorizeBasicAuth, civilstatus.getAllCivilStatus)
        .post(middleware.authorizeBasicAuth, validatorCivilStatus.validateCivilStatus, civilstatus.Add_CivilStatus);
    app.route(config.api_version + '/civilstatus/:civilstat_id')
        .get(middleware.authorizeBasicAuth, civilstatus.getCivilStatusById)
        .delete(middleware.authorizeBasicAuth, civilstatus.deleteCivilStatus)
        .put(middleware.authorizeBasicAuth, validatorCivilStatus.validateCivilStatus, civilstatus.updateCivilStatus);

    app.route(config.api_version + '/employmentstatus')
        .get(middleware.authorizeBasicAuth, employmentstatus.getAllEmploymentStatus)
        .post(middleware.authorizeBasicAuth, validatorEmploymentStatus.validateEmploymentStatus, employmentstatus.Add_Employstat);
    app.route(config.api_version + '/employmentstatus/:empstat_id')
        .get(middleware.authorizeBasicAuth, employmentstatus.getEmploymentStatusById)
        .delete(middleware.authorizeBasicAuth, employmentstatus.deleteEmploymentStatus)
        .put(middleware.authorizeBasicAuth, validatorEmploymentStatus.validateEmploymentStatus, employmentstatus.updateEmploymentStatus);

    app.route(config.api_version + '/departments')
        .get(middleware.authorizeBasicAuth, departments.getAllDepartments)
        .post(middleware.authorizeBasicAuth, validatorDepartment.validateDepartment, departments.Add_Departments);
    app.route(config.api_version + '/departments/:dep_id')
        .get(middleware.authorizeBasicAuth, departments.getDepartmentsById)
        .delete(middleware.authorizeBasicAuth, departments.deleteDepartments)
        .put(middleware.authorizeBasicAuth, validatorDepartment.validateDepartment, departments.updateDepartments);

    app.route(config.api_version + '/branch')
        .get(middleware.authorizeBasicAuth, branch.getAllBranch)
        .post(middleware.authorizeBasicAuth, validatorBranch.validateBranch, branch.Add_Branch);
    app.route(config.api_version + '/branch/:branch_id')
        .delete(middleware.authorizeBasicAuth, branch.deleteBranch)
        .get(middleware.authorizeBasicAuth, branch.getBranchById)
        .put(middleware.authorizeBasicAuth, validatorBranch.validateBranch, branch.updateBranch);

    app.route(config.api_version + '/divisions')
        .get(middleware.authorizeBasicAuth, divisions.getAllDivisions)
        .post(middleware.authorizeBasicAuth, validatorDivision.validateDivision, divisions.Add_Divisions);
    app.route(config.api_version + '/divisions/:div_id')
        .delete(middleware.authorizeBasicAuth, divisions.deleteDivision)
        .get(middleware.authorizeBasicAuth, divisions.getDivisionsById)
        .put(middleware.authorizeBasicAuth, validatorDivision.validateDivision, divisions.updateDivision);

    app.route(config.api_version + '/taxexemption')
        .get(middleware.authorizeBasicAuth, taxexemption.getAllTaxExemptions)
        .post(middleware.authorizeBasicAuth, validatorTaxExemption.validateTaxExemption, taxexemption.Add_Tax_Exemption);
    app.route(config.api_version + '/taxexemption/:index_id')
        .get(middleware.authorizeBasicAuth, taxexemption.GetExemptionByID)
        .delete(middleware.authorizeBasicAuth, taxexemption.Delete_Tax_Exemption)
        .put(middleware.authorizeBasicAuth, validatorTaxExemption.validateTaxExemption, taxexemption.Update_Tax_Exemption)

    app.route(config.api_version + '/religions')
        .get(middleware.authorizeBasicAuth, religions.getAllReligions)
        .post(middleware.authorizeBasicAuth, validatorReligion.validateReligion, religions.Add_Religion);
    app.route(config.api_version + '/religions/:religion_id')
        .get(middleware.authorizeBasicAuth, religions.getReligionById)
        .delete(middleware.authorizeBasicAuth, religions.deleteReligion)
        .put(middleware.authorizeBasicAuth, validatorReligion.validateReligion, religions.updateReligion);

    app.route(config.api_version + '/jobposition')
        .get(middleware.authorizeBasicAuth, jobposition.getAllJobPosition)
        .post(middleware.authorizeBasicAuth, validatorJobTitle.validateJobTitle, jobposition.Add_Job_Position);
    app.route(config.api_version + '/jobposition/:jp_id')
        .get(middleware.authorizeBasicAuth, jobposition.getJobPositionById)
        .delete(middleware.authorizeBasicAuth, jobposition.deleteJobPosition)
        .put(middleware.authorizeBasicAuth, jobposition.updateJobPosition);

    app.route(config.api_version + '/jobpositiongroup')
        .get(middleware.authorizeBasicAuth, jobposition.getAllGroup)
        .post(middleware.authorizeBasicAuth, validatorJobTitle.validateJobGroup, jobposition.Add_Group);
    app.route(config.api_version + '/jobpositiongroup/:group_id')
        .get(middleware.authorizeBasicAuth, jobposition.getGroupById)
        .delete(middleware.authorizeBasicAuth, jobposition.deleteGroup)
        .put(middleware.authorizeBasicAuth, validatorJobTitle.validateJobGroup, jobposition.updateGroup);

    app.route(config.api_version + '/jobpositionclass')
        .get(middleware.authorizeBasicAuth, jobposition.getAllClass)
        .post(middleware.authorizeBasicAuth, validatorJobTitle.validateJobClass, jobposition.Add_Class);
    app.route(config.api_version + '/jobpositionclass/:jclass_id')
        .get(middleware.authorizeBasicAuth, jobposition.getClassById)
        .delete(middleware.authorizeBasicAuth, jobposition.deleteClass)
        .put(middleware.authorizeBasicAuth, validatorJobTitle.validateJobClass, jobposition.updateClass);


    app.route(config.api_version + '/nationality')
        .get(middleware.authorizeBasicAuth, nationality.getAllNationality)
        .post(middleware.authorizeBasicAuth, validatorNationality.validateNationality, nationality.Add_Nationality);
    app.route(config.api_version + '/nationality/:nat_id')
        .get(middleware.authorizeBasicAuth, nationality.getNationalityById)
        .delete(middleware.authorizeBasicAuth, nationality.deleteNationality)
        .put(middleware.authorizeBasicAuth, validatorNationality.validateNationality, nationality.updateNationality);

    app.route(config.api_version + '/company')
        .get(middleware.authorizeBasicAuth, company.getCompanyInfo)
        .post(middleware.authorizeBasicAuth, validatorCompany.validateCompany, company.Add_Company_Info);

    app.route(config.api_version + '/company/:ci_id')
        .get(middleware.authorizeBasicAuth, company.getCompanyById)
        .delete(middleware.authorizeBasicAuth, company.deleteCompany)
        .put(middleware.authorizeBasicAuth, company.updateCompanyInfo);

    app.route(config.api_version + '/company/:ci_id/image')
        .put(middleware.authorizeBasicAuth, company.AddCompanyProfile)
        .delete(middleware.authorizeBasicAuth, company.DeleteCompanyProfile);

    app.route(config.api_version + '/days')
        .get(middleware.authorizeBasicAuth, workschedules.getAllDays);

    // ======================== EMPLOYEES ============================ //
    // ======================== EMPLOYEES ============================ //
    // ======================== EMPLOYEES ============================ //
    app.route(config.api_version + '/employees')
        .get(middleware.authorizeBasicAuth, employees.employeeMasterlist)
        .post(middleware.authorizeBasicAuth, validatorEmployee.validateEmployee, employees.Add_Employee);
    app.route(config.api_version + '/employees/:emp_id')
        .get(middleware.authorizeBasicAuth, employees.GetEmployeeProfile)
        .delete(middleware.authorizeBasicAuth, employees.Delete_Employee)
        .put(middleware.authorizeBasicAuth, employees.Update_Employee);
    app.route(config.api_version + '/employees/:emp_id/correction')
        .put(middleware.authorizeBasicAuth, validatorEmployee.validateEmployee, employees.Correction_Employee);
    app.route(config.api_version + '/employees/:emp_id/ID')
        .put(middleware.authorizeBasicAuth, employees.Update_Employee_ID);
    app.route(config.api_version + '/employees/:emp_id/family')
        .get(middleware.authorizeBasicAuth, employees.GetEmployeeFamilyBackground)
        .put(middleware.authorizeBasicAuth, employees.Add_Employee_Family_Background);
    app.route(config.api_version + '/employees/:emp_id/children')
        .get(middleware.authorizeBasicAuth, employees.GetEmployeeChildren)
        .put(middleware.authorizeBasicAuth, employees.Add_Employee_Children);
    app.route(config.api_version + '/employees/:emp_id/children/:_id')
        .delete(middleware.authorizeBasicAuth, employees.deleteEmployeeChildren);
    app.route(config.api_version + '/employees/:emp_id/education')
        .get(middleware.authorizeBasicAuth, employees.GetEmployeeEducation)
        .put(middleware.authorizeBasicAuth, employees.Update_Employee_Education);
    app.route(config.api_version + '/employees/:emp_id/schedule/:sched_id')
        .put(middleware.authorizeBasicAuth, employees.TagEmployeeWorkSchedule)
        .delete(middleware.authorizeBasicAuth, employees.UnTagEmployeeWorkSchedule);
    app.route(config.api_version + '/employees/:emp_id/image')
        .put(middleware.authorizeBasicAuth, employees.Add_Employee_Photo)
        .delete(middleware.authorizeBasicAuth, employees.Remove_Employee_Photo);
    app.route(config.api_version + '/employees/:emp_id/fingerprint')
        .get(middleware.authorizeBasicAuth,employees.getEmployeeFingerPrint);


    app.route(config.api_version + '/workschedules')
        .post(middleware.authorizeBasicAuth, workschedules.Add_Work_Schedule)
        .get(middleware.authorizeBasicAuth, workschedules.getAllWorkSchedules);
    app.route(config.api_version + '/workschedules/:sched_id')
        .get(middleware.authorizeBasicAuth, workschedules.getWorkSchedulesTemplates)
        .delete(middleware.authorizeBasicAuth, workschedules.Delete_Work_Schedule)
        .put(middleware.authorizeBasicAuth, workschedules.Update_Work_Schedule);
    app.route(config.api_version + '/workschedules/:sched_id/details')
        .get(middleware.authorizeBasicAuth, workschedules.getWorkScheduleDetails);
    app.route(config.api_version + '/workschedules/:sched_id/templates')
        .post(middleware.authorizeBasicAuth, workschedules.saveWorkScheduleTemplates)
        .put(middleware.authorizeBasicAuth, workschedules.updateWorkScheduleTemplates);




    // ======================== ATTENDANCE ============================ //
    // ======================== ATTENDANCE ============================ //
    // ======================== ATTENDANCE ============================ //
    app.route(config.api_version + '/attendance/:deviceno/timelogs')
        .post(middleware.authorizeBasicAuth, attendance.saveEmployeeRawTimeLogs)
        .get(middleware.authorizeBasicAuth, attendance.employeeRawTimelogs);
    app.route(config.api_version + '/attendance/:deviceno/timelogs/:entry_id')
        .delete(middleware.authorizeBasicAuth, attendance.deleteEmployeeRawTimelogs)
        .put(middleware.authorizeBasicAuth, attendance.updateEmployeeRawTimelogs);

    app.route(config.api_version + '/attendance/:emp_id/payroll/:payroll_id')
        .get(middleware.authorizeBasicAuth, attendance.GetEmployeeAttendance)
        .post(middleware.authorizeBasicAuth, attendance.Add_TimeKeeping)
        .delete(middleware.authorizeBasicAuth, attendance.Delete_TimeKeeping);

    app.route(config.api_version + '/attendance/:emp_id/payroll/:payroll_id/post')
        .get(middleware.authorizeBasicAuth, attendance.IsTimeAttendancePostedByPayrollTerm)
        .post(middleware.authorizeBasicAuth, attendance.Post_TimeKeeping);

    app.route(config.api_version + '/overtime_types')
        .get(middleware.authorizeBasicAuth, attendance.getAllOvertimeTypes);

    app.route(config.api_version + '/overtime/:refno')
        .delete(middleware.authorizeBasicAuth, attendance.DeleteOvertime);

    app.route(config.api_version + '/overtime/:refno/preapproved')
        .put(middleware.authorizeBasicAuth, attendance.PreApprovedOvertime);

    app.route(config.api_version + '/overtime/:refno/approved')
        .put(middleware.authorizeBasicAuth, attendance.ApprovedOvertime);

    app.route(config.api_version + '/overtime/:payroll_id/payroll')
        .get(middleware.authorizeBasicAuth, attendance.getAllOvertime);

    app.route(config.api_version + '/overtime/:emp_id/payroll/:payroll_id')
        .get(middleware.authorizeBasicAuth, attendance.getEmployeeOvertime)
        .post(middleware.authorizeBasicAuth, attendance.Add_Overtime)

    app.route(config.api_version + '/overtime/:emp_id/payroll/:payroll_id/items/:ot_id')
        .post(middleware.authorizeBasicAuth, attendance.Add_Overtime_Details)
        .delete(middleware.authorizeBasicAuth, attendance.Delete_OverTime_Details);

    app.route(config.api_version + '/overtime/:emp_id/payroll/:payroll_id/items/:ot_id/log')
        .put(middleware.authorizeBasicAuth, attendance.Update_OverTime_Details)

    app.route(config.api_version + '/overtime/:emp_id/payroll/:payroll_id/items/:ot_id/approved')
        .put(middleware.authorizeBasicAuth, attendance.ApprovedEmployeeOvertime);

    app.route(config.api_version + '/overtime/:emp_id/payroll/:payroll_id/items/:ot_id/preapproved')
        .put(middleware.authorizeBasicAuth, attendance.PreApprovedEmployeeOvertime);


    // ======================== PAYROLL ============================ //
    // ======================== PAYROLL ============================ //
    // ======================== PAYROLL ============================ //

    app.route(config.api_version + '/months')
        .get(middleware.authorizeBasicAuth, payroll.getAllMonths);

    app.route(config.api_version + '/paytypes')
        .get(middleware.authorizeBasicAuth, paytypes.getAllPayTypes)
    app.route(config.api_version + '/paytypes/:tp_id')
        .put(middleware.authorizeBasicAuth, paytypes.updatePayTypeDivisor);

    app.route(config.api_version + '/paymenttypes')
        .get(middleware.authorizeBasicAuth, payroll.GetAllPaymentTypes);

    app.route(config.api_version + '/payrolltypes')
        .get(middleware.authorizeBasicAuth, payroll.GetAllPayrollTypes);
    app.route(config.api_version + '/payrolltypes/:ptypid')
        .get(middleware.authorizeBasicAuth, payroll.GetAllPayrollTypesID);

    app.route(config.api_version + '/payroll/config')
        .get(middleware.authorizeBasicAuth, payroll.payrollConfig)
        .post(middleware.authorizeBasicAuth, payroll.createPayrollConfig);
    app.route(config.api_version + '/payroll/config/:payroll_id')
        .get(middleware.authorizeBasicAuth, payroll.getPayrollConfig)
        .delete(middleware.authorizeBasicAuth, payroll.Delete_Payroll)
        .put(middleware.authorizeBasicAuth, payroll.Update_PayrollConfig);


    // ======================== TOOLS ============================ //
    // ======================== TOOLS ============================ //
    // ======================== TOOLS ============================ //

    app.route(config.api_version + '/appsettings')
        .get(middleware.authorizeBasicAuth, appsettings.GetAllApplicationSettings)
        .post(middleware.authorizeBasicAuth, appsettings.Add_Application_Settings);

    app.route(config.api_version + '/appsettings/:setup_id')
        .get(middleware.authorizeBasicAuth, appsettings.GetApplicationSettingsByID)
        .delete(middleware.authorizeBasicAuth, appsettings.Delete_Application_Settings)
        .put(middleware.authorizeBasicAuth, appsettings.Update_Application_Settings);

    app.route(config.api_version + '/appsettings/:setup_id/active')
        .put(middleware.authorizeBasicAuth, appsettings.Activate_Settings);


    // ======================== ACCOUNTING ============================ //
    // ======================== ACCOUNTING ============================ //
    // ======================== ACCOUNTING ============================ //

    app.route(config.api_version + '/sss')
        .get(middleware.authorizeBasicAuth, sss.getAllSSS)
        .post(middleware.authorizeBasicAuth, validatorSSS.validateSSS, sss.AddSSSTable);
    app.route(config.api_version + '/sss/:sss_id')
        .get(middleware.authorizeBasicAuth, sss.getSSSByID)
        .put(middleware.authorizeBasicAuth, validatorSSS.validateSSS, sss.UpdateSSSTable)
        .delete(middleware.authorizeBasicAuth, sss.DelSSSTable);
    app.route(config.api_version + '/sss/:sss_id/bracket')
        .post(middleware.authorizeBasicAuth, sss.AddSSSBracket);
    app.route(config.api_version + '/sss/:sss_id/bracket/:sb_id')
        .get(middleware.authorizeBasicAuth, sss.GetSSSBracketByID)
        .delete(middleware.authorizeBasicAuth, sss.DelSSSBracket)
        .put(middleware.authorizeBasicAuth, sss.UpdateSSSBracket);

    app.route(config.api_version + '/hdmf')
        .get(middleware.authorizeBasicAuth, pagibig.getAllPagIBIG)
        .post(middleware.authorizeBasicAuth, validatorHDMF.validateHDMF, pagibig.Add_HDMF);
    app.route(config.api_version + '/hdmf/:hdmf_id')
        .get(middleware.authorizeBasicAuth, pagibig.GetHDMF_ById)
        .put(middleware.authorizeBasicAuth, validatorHDMF.validateHDMF, pagibig.Update_HDMF)
        .delete(middleware.authorizeBasicAuth, pagibig.DeleteHDMF);


    app.route(config.api_version + '/phic')
        .get(middleware.authorizeBasicAuth, phic.getAllPHIC)
        .post(middleware.authorizeBasicAuth, validatorPHIC.validatePHIC, phic.AddPHTable);
    app.route(config.api_version + '/phic/:phic_id')
        .get(middleware.authorizeBasicAuth, phic.getPHTableById)
        .delete(middleware.authorizeBasicAuth, phic.DelPHTable)
        .put(middleware.authorizeBasicAuth, validatorPHIC.validatePHIC, phic.UpdatePHTable);
    app.route(config.api_version + '/phic/:phic_id/bracket')
        .get(middleware.authorizeBasicAuth, phic.GetPHDetailsByPHICID)
        .post(middleware.authorizeBasicAuth, validatorPHIC.validatePhicBrackets, phic.AddPHDetails);
    app.route(config.api_version + '/phic/:phic_id/bracket/:indexid')
        .put(middleware.authorizeBasicAuth, phic.UpdatePHDetails)
        .delete(middleware.authorizeBasicAuth, phic.DelPHDetails);

    app.route(config.api_version + '/wtax')
        .get(middleware.authorizeBasicAuth, wtax.getAllWithholdingTax)
        .post(middleware.authorizeBasicAuth, validatorTAX.validateWithHoldingTax, wtax.newWithholdingTax);
    app.route(config.api_version + '/wtax/:tax_id')
        .get(middleware.authorizeBasicAuth, wtax.getTaxTableByID)
        .put(middleware.authorizeBasicAuth, validatorTAX.validateWithHoldingTax, wtax.updateWithholdingTax)
        .delete(middleware.authorizeBasicAuth, wtax.removeWithholdingTax);

    app.route(config.api_version + '/wtax/:tax_id/bracket')
        .get(middleware.authorizeBasicAuth, wtax.getTaxDetails)
        .post(middleware.authorizeBasicAuth, validatorTAX.validateTaxDetails, wtax.newTaxDetails)
        .delete(middleware.authorizeBasicAuth, wtax.removeTaxDetailsByTAXID);
    app.route(config.api_version + '/wtax/:tax_id/bracket/:index_id')
        .put(middleware.authorizeBasicAuth, validatorTAX.validateTaxDetails, wtax.updateTaxDetails);


    app.route(config.api_version + '/wtax/:tax_id/ceiling')
        .get(middleware.authorizeBasicAuth, wtax.getTaxCeilingByID)
        .post(middleware.authorizeBasicAuth, validatorTAX.validateTaxCeils, wtax.newTaxCeil)
        .delete(middleware.authorizeBasicAuth, wtax.removeTaxCeilByTAXID);
    app.route(config.api_version + '/wtax/:tax_id/ceiling/:index_id')
        .put(middleware.authorizeBasicAuth, validatorTAX.validateTaxCeils, wtax.updateTaxCeil);

    app.route(config.api_version + '/wtax/:tax_id/percent')
        .get(middleware.authorizeBasicAuth, wtax.getTaxPercentByID)
        .post(middleware.authorizeBasicAuth, validatorTAX.validateTaxPercent, wtax.newTaxPercent)
        .delete(middleware.authorizeBasicAuth, wtax.removeTaxPercentByTAXID);
    app.route(config.api_version + '/wtax/:tax_id/percent/:index_id')
        .put(middleware.authorizeBasicAuth, validatorTAX.validateTaxPercent, wtax.updateTaxPercent);

    app.route(config.api_version + '/wtax/:tax_id/due')
        .get(middleware.authorizeBasicAuth, wtax.getTaxDueByID)
        .post(middleware.authorizeBasicAuth, validatorTAX.validateTaxDue, wtax.newTaxDue)
        .delete(middleware.authorizeBasicAuth, wtax.removeTaxdueByTAXID);
    app.route(config.api_version + '/wtax/:tax_id/due/:index_id')
        .put(middleware.authorizeBasicAuth, validatorTAX.validateTaxDue, wtax.updateTaxDue)
        .delete(middleware.authorizeBasicAuth, wtax.removeTaxdue);


    app.route(config.api_version + '/accountclass')
        .get(middleware.authorizeBasicAuth, accounts.getAllAccountClass)
        .post(middleware.authorizeBasicAuth, validatorAccounts.validateAccountClassfication, accounts.Add_Account_Class);
    app.route(config.api_version + '/accountclass/:class_id')
        .get(middleware.authorizeBasicAuth, accounts.getAccountClass)
        .put(middleware.authorizeBasicAuth, validatorAccounts.validateAccountClassfication, accounts.Update_Account_Class)
        .delete(middleware.authorizeBasicAuth, accounts.Delete_Account_Class);

    app.route(config.api_version + '/accountgroups')
        .get(middleware.authorizeBasicAuth, accounts.getAllAccountGroups)
        .post(middleware.authorizeBasicAuth, validatorAccounts.validateAccountGroup, accounts.Add_Account_Groups);
    app.route(config.api_version + '/accountgroups/:group_id')
        .get(middleware.authorizeBasicAuth, accounts.getAccountGroup)
        .put(middleware.authorizeBasicAuth, validatorAccounts.validateAccountGroup, accounts.Update_Account_Groups)
        .delete(middleware.authorizeBasicAuth, accounts.Delete_Account_Groups);

    app.route(config.api_version + '/accounttypes')
        .get(middleware.authorizeBasicAuth, accounts.getAllAccountTypes)
        .post(middleware.authorizeBasicAuth, validatorAccounts.validateAccountType, accounts.Add_Account_Types);
    app.route(config.api_version + '/accounttypes/:type_id')
        .get(middleware.authorizeBasicAuth, accounts.getAccountType)
        .put(middleware.authorizeBasicAuth, validatorAccounts.validateAccountType, accounts.Update_Account_Types)
        .delete(middleware.authorizeBasicAuth, accounts.Delete_Account_Types);

    app.route(config.api_version + '/accounts')
        .get(middleware.authorizeBasicAuth, accounts.getAllAccounts)
        .post(middleware.authorizeBasicAuth, validatorAccounts.validateAccounts, accounts.Add_Accounts);
    app.route(config.api_version + '/accounts/:acct_id')
        .get(middleware.authorizeBasicAuth, accounts.GetAccountByID)
        .put(middleware.authorizeBasicAuth, validatorAccounts.validateAccounts, accounts.Update_Accounts)
        .delete(middleware.authorizeBasicAuth, accounts.Delete_Accounts);
};