'use strict';

var async = require('async');
var settingsDao = require('../services/appsettings');

function AppSettings() {
    this.settingsDao = settingsDao;
}

AppSettings.prototype.GetAllApplicationSettings = (next) => {
    settingsDao.GetAllApplicationSettings((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

AppSettings.prototype.Delete_Application_Settings = (SetupID, next) => {
    settingsDao.Delete_Application_Settings(SetupID, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

AppSettings.prototype.Activate_Settings = (SetupID, next) => {
    async.waterfall([
        (callback) => {
            settingsDao.DeActivate_Settings((err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }
                callback();
            });
        },
        (callback) => {
            settingsDao.Activate_Settings(SetupID, (err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }
                callback(null, {
                    msg: 'Record successfully activated',
                    result: null,
                    success: true
                });
            })
        }
    ], next);
};

AppSettings.prototype.GetApplicationSettingsByID = (SetupID, next) => {
    settingsDao.GetApplicationSettingsByID(SetupID, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

AppSettings.prototype.Add_Application_Settings = (data, next) => {
    settingsDao.Add_Application_Settings(data, (err, response) => {
        // settingsDao.Add_Application_Settings2(data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record Successfully saved',
            result: response,
            success: true
        });
    });
};

AppSettings.prototype.Update_Application_Settings = (SetupID, data, next) => {
    settingsDao.Update_Application_Settings(SetupID, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record Successfully updated',
            result: response,
            success: true
        });
    });
};

exports.AppSettings = AppSettings;