'use strict';

var workschedulesDao = require('../services/workschedules');
var async = require('async');

function WorkSchedules() {
    this.workschedulesDao = workschedulesDao;
}

WorkSchedules.prototype.getAllDays = (next) => {
    workschedulesDao.getAllDays((err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WorkSchedules.prototype.getAllWorkSchedules = (next) => {
    workschedulesDao.getAllWorkSchedules((err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WorkSchedules.prototype.getWorkSchedulesTemplates = (sched_id, next) => {
    workschedulesDao.getWorkSchedulesTemplates(sched_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WorkSchedules.prototype.getWorkScheduleDetails = (sched_id, next) => {
    workschedulesDao.getWorkScheduleDetails(sched_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WorkSchedules.prototype.Add_Work_Schedule = (data, next) => {
    workschedulesDao.Add_Work_Schedule(data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

WorkSchedules.prototype.Delete_Work_Schedule = (sched_id, next) => {
    workschedulesDao.Delete_Work_Schedule(sched_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

WorkSchedules.prototype.Update_Work_Schedule = (sched_id, data, next) => {
    workschedulesDao.Update_Work_Schedule(sched_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

WorkSchedules.prototype.saveWorkScheduleTemplates = (sched_id, data, next) => {
    async.waterfall([
        (callback) => {
            workschedulesDao.getWorkSchedulesTemplates(sched_id, (err, response) => {
                if (err) {
                    next({
                        msg: '',
                        result: err,
                        success: false
                    }, null);
                }
                if (response && response.length > 0) {
                    callback(null, 'update');
                } else {
                    callback(null, 'save');
                }
            });
        },
        (action, callback) => {
            console.log('action: ', action);
            if (action == 'update') {
                workschedulesDao.updateWorkScheduleTemplates(sched_id, data, (err, response) => {
                    if (err) {
                        next({
                            msg: '',
                            result: err,
                            success: false
                        }, null);
                    }
                    next(null, {
                        msg: 'Record successfully updated',
                        result: response,
                        success: true
                    });
                });
            } else if (action == 'save') {
                workschedulesDao.saveWorkScheduleTemplates(sched_id, data, (err, response) => {
                    if (err) {
                        next({
                            msg: '',
                            result: err,
                            success: false
                        }, null);
                    }
                    next(null, {
                        msg: 'Record successfully saved',
                        result: response,
                        success: true
                    });
                });
            }
        }
    ], next);
};



exports.WorkSchedules = WorkSchedules;