'use strict';

var branchDao = require('../services/branch');

function Branch() {
    this.branchDao = branchDao;
}



Branch.prototype.updateBranch = (branch_id, data, next) => {
    branchDao.updateBranch(branch_id, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};
Branch.prototype.Add_Branch = (data, next) => {
    branchDao.Add_Branch(data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

Branch.prototype.getBranchById = (branch_id, next) => {
    branchDao.getBranchById(branch_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Branch.prototype.getAllBranch = (next) => {
    branchDao.getAllBranch((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};




Branch.prototype.deleteBranch = (branch_id, next) => {
    branchDao.deleteBranch(branch_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

exports.Branch = Branch;