'use strict';

var philhealthDao = require('../services/philhealth');

function PHIC() {
    this.philhealthDao = philhealthDao;
}

PHIC.prototype.getAllPHIC = (next) => {
    philhealthDao.getAllPHIC((err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

PHIC.prototype.GetPHDetailsByPHICID = (phic_id, next) => {
    philhealthDao.GetPHDetailsByPHICID(phic_id, (err, response) => {
       if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

PHIC.prototype.getPhilHealthDetails = (phic_id, next) => {
    philhealthDao.getPhilHealthDetails(phic_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

PHIC.prototype.getPHTableById = (phic_id, next) => {
    philhealthDao.getPHTableById(phic_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

PHIC.prototype.AddPHTable = (data, next) => {
    philhealthDao.AddPHTable(data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

PHIC.prototype.UpdatePHTable = (phic_id, data, next) => {
    philhealthDao.UpdatePHTable(phic_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

PHIC.prototype.DelPHTable = (phic_id, next) => {
    philhealthDao.DelPHTable(phic_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};


PHIC.prototype.GetPHDetailsByID = (phic_id,indexid , next) => {
    philhealthDao.GetPHDetailsByID(phic_id, indexid , (err, response) => {
        if (err) {
            msg : 'Error Occured' ,
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

PHIC.prototype.AddPHDetails = (phic_id , data, next) => {
    philhealthDao.AddPHDetails(phic_id , data, (err, response) => {
        if (err) {
            msg:'Error Occured',
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

PHIC.prototype.UpdatePHDetails = (IndexID, phic_id , data, next) => {
    philhealthDao.UpdatePHDetails(IndexID, phic_id , data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

PHIC.prototype.DelPHDetails = (IndexID, phic_id , next) => {
    philhealthDao.DelPHDetails(IndexID, phic_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};


exports.PHIC = PHIC;