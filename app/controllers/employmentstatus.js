'use strict';

var employmentStatusDao = require('../services/employmentstatus');

function EmploymentStatus() {
    this.employmentStatusDao = employmentStatusDao;
}

EmploymentStatus.prototype.Add_Employstat = (data, next) => {
    employmentStatusDao.Add_Employstat(data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

EmploymentStatus.prototype.getAllEmploymentStatus = (next) => {
    employmentStatusDao.getAllEmploymentStatus((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


EmploymentStatus.prototype.getEmploymentStatusById = (empstat_id, next) => {
    employmentStatusDao.getEmploymentStatusById(empstat_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

EmploymentStatus.prototype.deleteEmploymentStatus = (empstat_id, next) => {
    employmentStatusDao.deleteEmploymentStatus(empstat_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

EmploymentStatus.prototype.updateEmploymentStatus = (empstat_id, data, next) => {
    employmentStatusDao.updateEmploymentStatus(empstat_id, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfull deleted',
            result: response,
            success: true
        });
    });
};

exports.EmploymentStatus = EmploymentStatus;