'use strict';

var departmentsDao = require('../services/departments');


function Departments() {
    this.departmentsDao = departmentsDao;
}

Departments.prototype.Add_Departments = (data, next) => {
    departmentsDao.Add_Departments(data, (err, response) => {
        if (err) {
           next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

Departments.prototype.getAllDepartments = (next) => {
    departmentsDao.getAllDepartments((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


Departments.prototype.deleteDepartments = (dep_id, next) => {
    departmentsDao.deleteDepartments(dep_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};


Departments.prototype.getDepartmentsById = (dep_id, next) => {
    departmentsDao.getDepartmentsById(dep_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


Departments.prototype.updateDepartments = (dep_id, data, next) => {
    departmentsDao.updateDepartments(dep_id, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

exports.Departments = Departments;