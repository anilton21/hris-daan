'use strict';

var async = require('async');
var accountsDao = require('../services/accounts');

function Accounts() {
    this.accountsDao = accountsDao;
}

Accounts.prototype.getAllAccountClass = (next) => {
    accountsDao.getAllAccountClass((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.getAccountClass = (ClassID, next) => {
    accountsDao.getAccountClass(ClassID, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Add_Account_Class = (data, next) => {
    accountsDao.Add_Account_Class(data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Update_Account_Class = (ClassID, data, next) => {
    accountsDao.Update_Account_Class(ClassID, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Delete_Account_Class = (ClassID, next) => {
    accountsDao.Delete_Account_Class(ClassID, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};


Accounts.prototype.getAllAccountGroups = (next) => {
    accountsDao.getAllAccountGroups((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.getAccountGroup = (GroupID, next) => {
    accountsDao.getAccountGroup(GroupID, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Add_Account_Groups = (data, next) => {
    accountsDao.Add_Account_Groups(data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Update_Account_Groups = (GroupID, data, next) => {
    accountsDao.Update_Account_Groups(GroupID, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Delete_Account_Groups = (GroupID, next) => {
    accountsDao.Delete_Account_Groups(GroupID, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};


Accounts.prototype.getAllAccountTypes = (next) => {
    accountsDao.getAllAccountTypes((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.getAccountType = (TypeID, next) => {
    accountsDao.getAccountType(TypeID, (err, response) => {
        if (err) {
            next(err, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Add_Account_Types = (data, next) => {
    accountsDao.Add_Account_Types(data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Update_Account_Types = (TypeID, data, next) => {
    accountsDao.Update_Account_Types(TypeID, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Delete_Account_Types = (TypeID, next) => {
    accountsDao.Delete_Account_Types(TypeID, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};


Accounts.prototype.getAllAccounts = (next) => {
    accountsDao.getAllAccounts((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.GetAccountByID = (AcctID, next) => {
    accountsDao.GetAccountByID(AcctID, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Add_Accounts = (data, next) => {
    accountsDao.Add_Accounts(data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Update_Accounts = (AcctID, data, next) => {
    accountsDao.Update_Accounts(AcctID, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

Accounts.prototype.Delete_Accounts = (AcctID, next) => {
    accountsDao.Delete_Accounts(AcctID, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};


exports.Accounts = Accounts;