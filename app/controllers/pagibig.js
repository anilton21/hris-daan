'use strict';

var pagibigDao = require('../services/pagibig');

function PAGIBIG() {
    this.pagibigDao = pagibigDao;
}

PAGIBIG.prototype.getAllPagIBIG = (next) => {
    pagibigDao.getAllPagIBIG((err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

PAGIBIG.prototype.Add_HDMF = (data, next) => {
    pagibigDao.Add_HDMF(data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

PAGIBIG.prototype.Update_HDMF = (hdmf_id, data, next) => {
    pagibigDao.Update_HDMF(hdmf_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

PAGIBIG.prototype.GetHDMF_ById = (hdmf_id, next) => {
    pagibigDao.GetHDMF_ById(hdmf_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

PAGIBIG.prototype.DeleteHDMF = (hdmf_id, next) => {
    pagibigDao.DeleteHDMF(hdmf_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

exports.PAGIBIG = PAGIBIG;