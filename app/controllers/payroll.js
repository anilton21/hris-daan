'use strict';

var payrollDao = require('../services/payroll');
var _ = require('lodash-node');

function Payroll() {
    this.payrollDao = payrollDao;
}

Payroll.prototype.getAllMonths = (next) => {
    payrollDao.getAllMonths((err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Payroll.prototype.GetAllPayrollTypes = (next) => {
    payrollDao.GetAllPayrollTypes((err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Payroll.prototype.GetAllPayrollTypesID = (type_id, next) => {
    payrollDao.GetAllPayrollTypesID(type_id, (err, response) => {
        if (err) {
            next(err, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Payroll.prototype.GetAllPaymentTypes = (next) => {
    payrollDao.GetAllPaymentTypes((err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Payroll.prototype.payrollConfig = (next) => {
    payrollDao.payrollConfig((err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Payroll.prototype.createPayrollConfig = (data, next) => {
    payrollDao.createPayrollConfig(data, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'Record succesfully saved',
            result: response,
            success: true
        });
    });
};

Payroll.prototype.getPayrollConfig = (PayrollID, next) => {
    payrollDao.getPayrollConfig(PayrollID, (err, response) => {
        if (err) {
            next(err, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Payroll.prototype.Delete_Payroll = (PayrollID, next) => {
    payrollDao.Delete_Payroll(PayrollID, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'Record succesfully deleted',
            result: response,
            success: true
        });
    });
};

Payroll.prototype.Update_PayrollConfig = (PayrollID, data, next) => {
    payrollDao.Update_PayrollConfig(PayrollID, data, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'Record succesfully updated',
            result: response,
            success: true
        });
    });
};


exports.Payroll = Payroll;