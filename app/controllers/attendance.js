'use strict';

var attendanceDao = require('../services/attendance');
var payrollDao = require('../services/payroll');
var employeeDao = require('../services/employees');
var func = require('../utils/func');

var _ = require('lodash-node');
var async = require('async');
var ejs = require('ejs');
var path = require('path');
var moment = require('moment');

function Attendance() {
    this.attendanceDao = attendanceDao;
    this.payrollDao = payrollDao;
    this.employeeDao = employeeDao;
}

Attendance.prototype.employeeRawTimelogs = (DeviceNo, params, next) => {
    if (!params.datefrom) {
        return next(null, {
            msg: 'WARNING!!! Unable to proceed.The system found that the you have not set DATE FROM to get the employee ATTENDANCE.',
            result: null,
            succes: false
        });
    }

    if (!params.dateto) {
        return next(null, {
            msg: 'WARNING!!! Unable to proceed.The system found that the you have not set DATE TO to get the employee ATTENDANCE.',
            result: null,
            succes: false
        });
    }

    attendanceDao.employeeRawTimelogs(DeviceNo, params.datefrom, params.dateto, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Attendance.prototype.saveEmployeeRawTimeLogs = (DeviceNo, data, next) => {
    attendanceDao.saveEmployeeRawTimeLogs(DeviceNo, data, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'Employee timelog successfully saved',
            result: response,
            success: true
        });
    });
};

Attendance.prototype.deleteEmployeeRawTimelogs = (EntryID, next) => {
    attendanceDao.deleteEmployeeRawTimelogs(EntryID, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'Employee timelog successfully deleted',
            result: response,
            success: true
        });
    });
};

Attendance.prototype.updateEmployeeRawTimelogs = (EntryID, data, next) => {
    attendanceDao.updateEmployeeRawTimelogs(EntryID, data, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'Employee timelog successfully updated',
            result: response,
            success: true
        });
    });
};

Attendance.prototype.GetEmployeeAttendance = (e_idno, PayrollID, next) => {
    attendanceDao.GetEmployeeAttendance(e_idno, PayrollID, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


Attendance.prototype.Add_TimeKeeping = (e_idno, PayrollID, user, data, next) => {
    async.waterfall([
        (callback) => {
            attendanceDao.IsTimeAttendancePostedByPayrollTerm(e_idno, PayrollID, (err, response) => {
                if (err) {
                    next(err, null);
                }

                if (response && response.length > 0) {
                    next(null, {
                        msg: 'Employee Attendance already posted',
                        result: null,
                        success: false
                    });
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            data.CURRENT_USER = user.user;
            attendanceDao.Add_TimeKeeping(e_idno, PayrollID, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }
                next(null, {
                    msg: 'Record successfully saved',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Attendance.prototype.Delete_TimeKeeping = (e_idno, PayrollID, next) => {
    async.waterfall([
        (callback) => {
            attendanceDao.IsTimeAttendancePostedByPayrollTerm(e_idno, PayrollID, (err, response) => {
                if (err) {
                    next(err, null);
                }

                if (response && response.length > 0) {
                    next(null, {
                        msg: 'Employee Attendance already posted',
                        result: null,
                        success: false
                    });
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            attendanceDao.Delete_TimeKeeping(e_idno, PayrollID, (err, response) => {
                if (err) {
                    next(err, null);
                }
                next(null, {
                    msg: 'Record successfully deleted',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Attendance.prototype.IsTimeAttendancePostedByPayrollTerm = (e_idno, PayrollID, next) => {
    attendanceDao.IsTimeAttendancePostedByPayrollTerm(e_idno, PayrollID, (err, response) => {
        if (err) {
            next(err, null);
        }

        if (response && response.length > 0) {
            next(null, {
                msg: 'Employee Attendance already posted',
                result: null,
                success: true
            });
        } else {
            next(null, {
                msg: '',
                result: null,
                success: false
            });
        }
    });
};

Attendance.prototype.Post_TimeKeeping = (e_idno, PayrollID, user, next) => {
    var data = {};
    data.CURRENT_USER = user.user;
    async.waterfall([
        (callback) => {
            attendanceDao.IsTimeAttendancePostedByPayrollTerm(e_idno, PayrollID, (err, response) => {
                if (err) {
                    next(err, null);
                }

                if (response && response.length > 0) {
                    next(null, {
                        msg: 'Employee Attendance already posted',
                        result: null,
                        success: true
                    });
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            attendanceDao.Post_TimeKeeping(e_idno, PayrollID, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }
                next(null, {
                    msg: 'Attendance successfully posted',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Attendance.prototype.getAllOvertime = (PayrollID, next) => {
    attendanceDao.getAllOvertime(PayrollID, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Attendance.prototype.getAllOvertimeTypes = (next) => {
    attendanceDao.getAllOvertimeTypes((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Attendance.prototype.Add_Overtime = (data, next) => {
    attendanceDao.OvertimeAlreadyExisted(data.RefNo, (err, reply) => {
        if (err) {
            next(err, null);
        }
        if (reply && reply.length > 0) {
            console.log('update OT')
            attendanceDao.Update_Overtime(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }
                next(null, {
                    msg: 'Record Successfully updated',
                    result: response,
                    success: true
                });
            });
        } else {
            console.log('insert OT')
            attendanceDao.Add_Overtime(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }
                next(null, {
                    msg: 'Record Successfully saved',
                    result: response,
                    success: true
                });
            });
        }
    })
};

Attendance.prototype.Add_Overtime_Details = (ot_id, data, next) => {
    attendanceDao.Add_Overtime_Details(ot_id, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record Successfully saved',
            result: response,
            success: true
        });
    });
};

Attendance.prototype.Update_OverTime_Details = (emp_id, payroll_id, ot_id, user, OTD, next) => {
    async.waterfall([
        (callback) => {
            attendanceDao.IsOverTimePreApproved2(ot_id, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    callback();
                } else {
                    next(null, {
                        msg: 'WARNING!!! Unable to proceed.The system found that the selected overtime record not PRE-APPROVED.',
                        result: null,
                        success: false
                    })
                }
            });
        },
        (callback) => {
            attendanceDao.IsOverTimeApproved2(ot_id, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    next(null, {
                        msg: 'WARNING!!! Unable to proceed.The system found that the selected overtime record already APPROVED/POSTED.',
                        result: null,
                        success: false
                    })
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            attendanceDao.Update_OverTime_Details(emp_id, payroll_id, ot_id, user.user, OTD, (err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }
                callback(null, {
                    msg: 'Record Successfully updated',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Attendance.prototype.Delete_OverTime_Details = (emp_id, payroll_id, ot_id, next) => {
    async.waterfall([
        (callback) => {
            attendanceDao.IsOverTimeApproved2(ot_id, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    next(null, {
                        msg: 'WARNING!!! Unable to proceed.The system found that the selected overtime record already APPROVED/POSTED.',
                        result: null,
                        success: false
                    })
                } else {
                    callback();
                }
            });
        }, (callback) => {
            attendanceDao.Delete_OverTime_Details(emp_id, payroll_id, ot_id, (err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }
                callback(null, {
                    msg: 'Record Successfully deleted',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Attendance.prototype.getEmployeeOvertime = (emp_id, payroll_id, next) => {
    attendanceDao.getEmployeeOvertime(emp_id, payroll_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Attendance.prototype.PreApprovedOvertime = (refno, user, next) => {
    async.waterfall([
        (callback) => {
            attendanceDao.GetOvertimeInfo(refno, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    response = response[0];
                    callback(null, response);
                } else {
                    next(null, {
                        msg: 'WARNING!!! Unable to proceed.The system found that overtime record not existed.',
                        result: null,
                        success: false
                    });
                }
            })
        },
        (info, callback) => {
            payrollDao.getPayrollConfig(info.Payroll_UUID, (err, response) => {
                if (err) {
                    next(err, null);
                }

                if (response && response.length > 0) {
                    response = response[0];
                }
                callback(null, info, response);
            });
        },
        (info, payroll, callback) => {
            employeeDao.GetEmployeeProfile(info.Emp_UUID, (err, response) => {
                if (err) {
                    next(err, null);
                }

                if (response && response.length > 0) {
                    response = response[0];
                }
                callback(null, info, payroll, response);
            });
        },
        (info, payroll, employee, callback) => {
            attendanceDao.IsOverTimePreApproved(refno, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    next(null, {
                        msg: 'WARNING!!! Unable to proceed.The system found that overtime record already PRE-APPROVED.',
                        result: null,
                        success: false
                    })
                } else {
                    callback(null, info, payroll, employee);
                }
            });
        },
        (info, payroll, employee, callback) => {
            attendanceDao.PreApprovedOvertime(refno, user.user, (err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }

                var template = path.join(__dirname, '..', 'views/emails/overtime-allpre-approved.ejs');
                var ejstemp = {
                    Fullname: employee.e_fname,
                    Email: employee.e_email,
                    curDate: moment().format('MM-DD-YYYY'),
                    PayrollCode: payroll.PayrollCode,
                    PayrollPeriod: moment(new Date(payroll.FirstDate)).format('MM/DD/YYYY') + '-' + moment(new Date(payroll.SecondDate)).format('MM/DD/YYYY'),
                    Overtimes: response,
                };
                console.log('ejstemp: ', ejstemp);
                ejs.renderFile(template, ejstemp, (err, html) => {
                    if (err) console.log(err); // Handle error

                    var mailData = {
                        from: 'info@4loop.ph',
                        email: employee.e_email,
                        subject: 'HRIS Overtime Batch PRE-APPROVAL notification',
                        message: html,
                        attachment: null
                    };
                    if (employee.e_email) {
                        func.sendMail(mailData, (err, mailresponse) => {
                            if (err) {
                                console.log('err: ', err.body);
                            }
                        });
                    }
                });

                callback(null, {
                    msg: 'Overtime successfully pre-approved',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Attendance.prototype.PreApprovedEmployeeOvertime = (emp_id, payroll_id, ot_id, user, next) => {
    async.waterfall([
        (callback) => {
            payrollDao.getPayrollConfig(payroll_id, (err, response) => {
                if (err) {
                    next(err, null);
                }

                if (response && response.length > 0) {
                    response = response[0];
                }
                callback(null, response);
            });
        },
        (payroll, callback) => {
            employeeDao.GetEmployeeProfile(emp_id, (err, response) => {
                if (err) {
                    next(err, null);
                }

                if (response && response.length > 0) {
                    response = response[0];
                }
                callback(null, payroll, response);
            });
        },
        (payroll, employee, callback) => {
            attendanceDao.IsOverTimePreApproved2(ot_id, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    next(null, {
                        msg: 'WARNING!!! Unable to proceed.The system found that overtime record already PRE-APPROVED.',
                        result: null,
                        success: false
                    })
                } else {
                    callback(null, payroll, employee);
                }
            });
        },
        (payroll, employee, callback) => {
            attendanceDao.PreApprovedEmployeeOvertime(ot_id, user.user, (err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }

                var template = path.join(__dirname, '..', 'views/emails/overtime-pre-approved.ejs');
                var ejstemp = {
                    Fullname: employee.e_fname,
                    Email: employee.e_email,
                    curDate: moment().format('MM-DD-YYYY'),
                    PayrollCode: payroll.PayrollCode,
                    PayrollPeriod: moment(new Date(payroll.FirstDate)).format('MM/DD/YYYY') + '-' + moment(new Date(payroll.SecondDate)).format('MM/DD/YYYY'),
                    InclusiveDates: response.InclusiveDates,
                    PreApprovedBy: response.PreApprovedBy,
                    PreApprovedDate: response.PreApprovedDate
                };
                console.log('ejstemp: ', ejstemp);
                ejs.renderFile(template, ejstemp, (err, html) => {
                    if (err) console.log(err); // Handle error

                    var mailData = {
                        from: 'info@4loop.ph',
                        email: employee.e_email,
                        subject: 'HRIS Overtime PRE-APPROVAL notification',
                        message: html,
                        attachment: null
                    };
                    if (employee.e_email) {
                        func.sendMail(mailData, (err, mailresponse) => {
                            if (err) {
                                console.log('err: ', err.body);
                            }
                        });
                    }
                });

                callback(null, {
                    msg: 'Selected Overtime successfully pre-approved',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Attendance.prototype.ApprovedOvertime = (refno, user, next) => {
    async.waterfall([
        (callback) => {
            attendanceDao.GetOvertimeInfo(refno, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    response = response[0];
                    callback(null, response);
                } else {
                    next(null, {
                        msg: 'WARNING!!! Unable to proceed.The system found that overtime record not existed.',
                        result: null,
                        success: false
                    });
                }
            })
        },
        (info, callback) => {
            payrollDao.getPayrollConfig(info.Payroll_UUID, (err, response) => {
                if (err) {
                    next(err, null);
                }

                if (response && response.length > 0) {
                    response = response[0];
                }
                callback(null, info, response);
            });
        },
        (info, payroll, callback) => {
            employeeDao.GetEmployeeProfile(info.Emp_UUID, (err, response) => {
                if (err) {
                    next(err, null);
                }

                if (response && response.length > 0) {
                    response = response[0];
                }
                callback(null, info, payroll, response);
            });
        },
        /*(info, payroll, employee, callback) => {
            attendanceDao.IsOverTimeApproved(refno, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    next(null, {
                        msg: 'WARNING!!! Unable to proceed.The system found that overtime record already POSTED.',
                        result: null,
                        success: false
                    })
                } else {
                    callback(null, info, payroll, employee);
                }
            });
        },*/
        (info, payroll, employee, callback) => {
            attendanceDao.ApprovedOvertime(refno, user.user, (err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }

                var template = path.join(__dirname, '..', 'views/emails/overtime-all-approved.ejs');
                var ejstemp = {
                    Fullname: employee.e_fname,
                    Email: employee.e_email,
                    curDate: moment().format('MM-DD-YYYY'),
                    PayrollCode: payroll.PayrollCode,
                    PayrollPeriod: moment(new Date(payroll.FirstDate)).format('MM/DD/YYYY') + '-' + moment(new Date(payroll.SecondDate)).format('MM/DD/YYYY'),
                    Overtimes: response,
                };
                console.log('ejstemp: ', ejstemp);
                ejs.renderFile(template, ejstemp, (err, html) => {
                    if (err) console.log(err); // Handle error

                    var mailData = {
                        from: 'info@4loop.ph',
                        email: employee.e_email,
                        subject: 'HRIS Overtime Batch APPROVAL notification',
                        message: html,
                        attachment: null
                    };
                    if (employee.e_email) {
                        func.sendMail(mailData, (err, mailresponse) => {
                            if (err) {
                                console.log('err: ', err.body);
                            }
                        });
                    }
                });

                callback(null, {
                    msg: 'Overtime successfully approved',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Attendance.prototype.ApprovedEmployeeOvertime = (emp_id, payroll_id, ot_id, user, next) => {
    async.waterfall([
        (callback) => {
            payrollDao.getPayrollConfig(payroll_id, (err, response) => {
                if (err) {
                    next(err, null);
                }

                if (response && response.length > 0) {
                    response = response[0];
                }
                callback(null, response);
            });
        },
        (payroll, callback) => {
            employeeDao.GetEmployeeProfile(emp_id, (err, response) => {
                if (err) {
                    next(err, null);
                }

                if (response && response.length > 0) {
                    response = response[0];
                }
                callback(null, payroll, response);
            });
        },
        (payroll, employee, callback) => {
            attendanceDao.IsOverTimeApproved2(ot_id, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    next(null, {
                        msg: 'WARNING!!! Unable to proceed.The system found that overtime record already POSTED.',
                        result: null,
                        success: false
                    })
                } else {
                    callback(null, payroll, employee);
                }
            });
        },
        (payroll, employee, callback) => {
            attendanceDao.ApprovedEmployeeOvertime(ot_id, user.user, (err, response) => {
                if (err) {
                    next({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }
                var template = path.join(__dirname, '..', 'views/emails/overtime-approved.ejs');
                var ejstemp = {
                    Fullname: employee.e_fname,
                    Email: employee.e_email,
                    curDate: moment().format('MM-DD-YYYY'),
                    PayrollCode: payroll.PayrollCode,
                    PayrollPeriod: moment(new Date(payroll.FirstDate)).format('MM/DD/YYYY') + '-' + moment(new Date(payroll.SecondDate)).format('MM/DD/YYYY'),
                    InclusiveDates: response.InclusiveDates,
                    TimeIn: response.F_TimeIn,
                    TimeOut: response.F_TimeIn,
                    ApprovedBy: response.ApprovedBy,
                    ApprovedDate: response.ApprovedDate
                };
                console.log('ejstemp: ', ejstemp);
                ejs.renderFile(template, ejstemp, (err, html) => {
                    if (err) console.log(err); // Handle error

                    var mailData = {
                        from: 'info@4loop.ph',
                        email: employee.e_email,
                        subject: 'HRIS Overtime APPROVAL notification',
                        message: html,
                        attachment: null
                    };
                    if (employee.e_email) {
                        func.sendMail(mailData, (err, mailresponse) => {
                            if (err) {
                                console.log('err: ', err.body);
                            }
                        });
                    }
                });
                callback(null, {
                    msg: 'Selected Overtime successfully approved',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Attendance.prototype.DeleteOvertime = (EntryID, next) => {
    async.waterfall([
        (callback) => {
            attendanceDao.IsOverTimeApproved3(EntryID, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    next(null, {
                        msg: 'WARNING!!! Unable to proceed.The system found that overtime record already POSTED.',
                        result: null,
                        success: false
                    })
                } else {
                    callback();
                }
            });
        }, (callback) => {
            attendanceDao.DeleteOvertime(EntryID, (err, response) => {
                if (err) {
                    next(err, null);
                }
                callback(null, {
                    msg: 'Overtime successfully deleted',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};


exports.Attendance = Attendance;