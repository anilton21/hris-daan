'use strict';

var religionsDao = require('../services/religions');

function Religions() {
    this.religionsDao = religionsDao;
}

Religions.prototype.Add_Religion = (data, next) => {
    religionsDao.Add_Religion(data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

Religions.prototype.updateReligion = (religion_id, data, next) => {
    religionsDao.updateReligion(religion_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

Religions.prototype.deleteReligion = (religion_id, next) => {
    religionsDao.deleteReligion(religion_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};
Religions.prototype.getAllReligions = (next) => {
    religionsDao.getAllReligions((err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Religions.prototype.getReligionById = (religion_id, next) => {
    religionsDao.getReligionById(religion_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


exports.Religions = Religions;