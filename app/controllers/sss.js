'use strict';

var sssDao = require('../services/sss');

function SSS() {
    this.sssDao = sssDao;
}

SSS.prototype.getAllSSS = (params , next) => {
    sssDao.getAllSSS(params , (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


SSS.prototype.getSSSAllDetails = (sss_id, next) => {
    sssDao.getSSSAllDetails(sss_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

SSS.prototype.AddSSSTable = (data, next) => {
    sssDao.AddSSSTable(data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    })
};

SSS.prototype.UpdateSSSTable = (sss_id, data, next) => {
    sssDao.UpdateSSSTable(sss_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

SSS.prototype.DelSSSTable = (sss_id, next) => {
    sssDao.DelSSSTable(sss_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

SSS.prototype.getSSSByID = (sss_id, params, next) => {
    sssDao.getSSSByID(sss_id, params , (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }else{

        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

SSS.prototype.AddSSSBracket = (sss_id , data, next) => {
    sssDao.AddSSSBracket(sss_id , data, (err, response) => {
         if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

SSS.prototype.GetSSSBracketByID = (sss_id , sb_id, next) => {
    sssDao.GetSSSBracketByID(sss_id , sb_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

SSS.prototype.DelSSSBracket = (sb_id , sss_id, next) => {
    sssDao.DelSSSBracket(sb_id , sss_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

SSS.prototype.UpdateSSSBracket = (sb_id, sss_id ,data, next) => {
    sssDao.UpdateSSSBracket(sb_id, sss_id ,  data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        
        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

exports.SSS = SSS;