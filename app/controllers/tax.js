'use strict';

var taxDao = require('../services/tax');

function WTAX() {
    this.taxDao = taxDao;
}

WTAX.prototype.getAllWithholdingTax = (next) => {
    taxDao.getAllWithholdingTax((err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.getTaxTableByID = (tax_id, next) => {
    taxDao.getTaxTableByID(tax_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.getAllTaxSummary = (tax_id, next) => {
    taxDao.getAllTaxSummary(tax_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.newWithholdingTax = (data, next) => {
    taxDao.newWithholdingTax(data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.updateWithholdingTax = (tax_id, data, next) => {
    taxDao.updateWithholdingTax(tax_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.removeWithholdingTax = (tax_id, next) => {
    taxDao.removeWithholdingTax(tax_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};


WTAX.prototype.newTaxDetails = (tax_id, data, next) => {
    taxDao.newTaxDetails(tax_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.updateTaxDetails = (IndexID, tax_id, data , next) => {
    taxDao.updateTaxDetails(IndexID, tax_id, data , (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.removeTaxDetailsByTAXID = (tax_id, next) => {
    taxDao.removeTaxDetailsByTAXID(tax_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.getTaxDetails = (IndexID, next) => {
    taxDao.getTaxDetails(IndexID, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.newTaxCeil = (tax_id, data, next) => {
    taxDao.newTaxCeil(tax_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.updateTaxCeil = (IndexID, tax_id, data, next) => {
    taxDao.updateTaxCeil(IndexID, tax_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.removeTaxCeilByTAXID = (tax_id, next) => {
    taxDao.removeTaxCeilByTAXID(tax_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.newTaxPercent = (tax_id, data, next) => {
    taxDao.newTaxPercent(tax_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.updateTaxPercent = (IndexID, tax_id, data, next) => {
    taxDao.updateTaxPercent(IndexID, tax_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.updateTaxDue = (IndexID, tax_id, data, next) => {
    taxDao.updateTaxDue(IndexID, tax_id, data, (err, response) => {
        if (err) {
            msg: 'Error Occured',
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {

            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.removeTaxPercentByTAXID = (tax_id, next) => {
    taxDao.removeTaxPercentByTAXID(tax_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.newTaxDue = (tax_id, data, next) => {
    taxDao.newTaxDue(tax_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.removeTaxdue = (index_id, next) => {
    taxDao.removeTaxdue(tax_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.getTaxesByID = (IndexID, next) => {
    taxDao.getTaxesByID(IndexID, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.getTaxDueByID = (IndexID, next) => {
    taxDao.getTaxDueByID(IndexID, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        } else {

        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.getTaxCeilingByID = (IndexID, next) => {
    taxDao.getTaxCeilingByID(IndexID, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        } else {

        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.getTaxPercentByID = (IndexID, next) => {
    taxDao.getTaxPercentByID(IndexID, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        } else {

        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

WTAX.prototype.removeTaxdueByTAXID = (tax_id, next) => {
    taxDao.removeTaxdueByTAXID(tax_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};



exports.WTAX = WTAX;