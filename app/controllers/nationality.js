'use strict';

var nationalityDao = require('../services/nationality');

function Nationality() {
    this.nationalityDao = nationalityDao;
}

Nationality.prototype.Add_Nationality = (data,next)=>{
    nationalityDao.Add_Nationality(data,(err,response)=>{
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};
Nationality.prototype.getNationalityById = (nat_id, next) => {
    nationalityDao.getNationalityById(nat_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Nationality.prototype.getAllNationality = (next) => {
    nationalityDao.getAllNationality((err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Nationality.prototype.deleteNationality = (nat_id, next) => {
    nationalityDao.deleteNationality(nat_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};
Nationality.prototype.updateNationality = (nat_id, data, next) => {
    nationalityDao.updateNationality(nat_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

exports.Nationality = Nationality;