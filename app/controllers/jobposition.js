'use strict';

var jobpositionDao = require('../services/jobposition');

function JobPosition() {
    this.jobpositionDao = jobpositionDao;
}

JobPosition.prototype.getAllJobPosition = (next) => {
    jobpositionDao.getAllJobPosition((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

JobPosition.prototype.getAllJobClassification = (next) => {
    jobpositionDao.getAllJobClassification((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


JobPosition.prototype.Add_Job_Position = (data, next) => {
    jobpositionDao.Add_Job_Position(data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

JobPosition.prototype.Add_Group = (data, next) => {
    jobpositionDao.Add_Group(data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

JobPosition.prototype.Add_Class = (data, next) => {
    jobpositionDao.Add_Class(data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};
JobPosition.prototype.getAllClass = (next) => {
    jobpositionDao.getAllClass((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

JobPosition.prototype.getAllGroup = (next) => {
    jobpositionDao.getAllGroup((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

JobPosition.prototype.getJobPositionById = (jp_id, next) => {
    jobpositionDao.getJobPositionById(jp_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

JobPosition.prototype.getClassById = (jclass_id, next) => {
    jobpositionDao.getClassById(jclass_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};
JobPosition.prototype.getGroupById = (group_id, next) => {
    jobpositionDao.getGroupById(group_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


JobPosition.prototype.deleteGroup = (group_id, next) => {
    jobpositionDao.deleteGroup(group_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};


JobPosition.prototype.deleteClass = (jclass_id, next) => {
    jobpositionDao.deleteClass(jclass_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

JobPosition.prototype.deleteJobPosition = (jp_id, next) => {
    jobpositionDao.deleteJobPosition(jp_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

JobPosition.prototype.updateJobPosition = (jp_id, data, next) => {
    jobpositionDao.updateJobPosition(jp_id, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

JobPosition.prototype.updateClass = (IndexID, data, next) => {
    jobpositionDao.updateClass(IndexID, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

JobPosition.prototype.updateGroup = (GroupID, data, next) => {
    jobpositionDao.updateGroup(GroupID, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        
        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

JobPosition.prototype.getJobPositionGroup = (jt_id, next) => {
    jobpositionDao.getJobPositionGroup(jt_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


exports.JobPosition = JobPosition;