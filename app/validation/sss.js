'use strict';

exports.validateSSS = function(req, res, next) {
    req.checkBody('SSSCode', 'Please Provide Social Security Code.').notEmpty();
    req.checkBody('SSSName', 'Please Provide Social Security  Name.').notEmpty();
    req.checkBody('Description', 'Please Provide SSS Description.').notEmpty();
    req.checkBody('Inactive', 'Please Confirm SSS Inactivity. ').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};

