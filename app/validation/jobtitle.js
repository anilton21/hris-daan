'use strict';

exports.validateJobTitle = function(req, res, next) {
    req.checkBody('jt_code', 'Please Provide Job Position Code.').notEmpty();
    req.checkBody('jt_name', 'Please Provide A Title.').notEmpty();
    req.checkBody('jt_shortname', 'Please Provide A Short Name.').notEmpty();
    req.checkBody('jt_desc', 'Please Provide A Job Position Description. ').notEmpty();
    req.checkBody('jt_classid', 'Please Provide A Classification.')
    req.checkBody('jt_groupid', 'Please Provide A Group. ').notEmpty();
    req.checkBody('inactive', 'Please Confirm Job Position In-activity.')

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};


exports.validateJobGroup = function(req, res, next) {
    req.checkBody('GroupCode', 'Please Provide A Group Code.').notEmpty();
    req.checkBody('GroupName', 'Please Provide Group Name.').notEmpty();
    req.checkBody('Description', 'Please Provide A Description.').notEmpty();
    req.checkBody('Inactive', 'Please Confirm Group Inactivity. ').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};




exports.validateJobClass = function(req, res, next) {
    req.checkBody('ClassCode', 'Please Provide A Class Code.').notEmpty();
    req.checkBody('Classification', 'Please Provide Class Name.').notEmpty();
    req.checkBody('Description', 'Please Provide A Description.').notEmpty();
    req.checkBody('Inactive', 'Please Confirm Class Inactivity. ').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};