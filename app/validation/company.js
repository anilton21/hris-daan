'use strict';

exports.validateCompany = function(req, res, next) {

    req.checkBody('ci_name', 'Please Provide Company Name').notEmpty();
    req.checkBody('ci_fax', 'Please Provide A Company Fax No.').notEmpty();
    req.checkBody('ci_telephone', 'Please Provide A Telephone No. ').notEmpty();
    req.checkBody('ci_website', 'Please Provide A Company Website')
    req.checkBody('office_code', 'Please Provide Company Office Code. ').notEmpty();
    req.checkBody('ci_owner', 'Please Provide A Company Owner')
    req.checkBody('office_code', 'Please Provide Company Office Code. ').notEmpty();
    req.checkBody('ci_city', 'Please Provide Company Town Or City Address') -
    req.checkBody('ci_streetadd', 'Please Provide Company Street Address. ').notEmpty();
    req.checkBody(' ci_provincialadd', 'Please Provide Company Provincial Address')
    req.checkBody('ci_zipcode', 'Please Provide Company ZIP CODE. ').notEmpty();
    req.checkBody('ci_ssno', 'Please Provide SS NO.')
    req.checkBody('ci_gsisno', 'Please Provide GSIS NO').notEmpty();
    req.checkBody('ci_pagibigno', 'Please Provide HDMF NO.').notEmpty();
    req.checkBody('ci_tinno', 'Please Provide Business TIN NO. ').notEmpty();
    req.checkBody('ci_philhealthno', 'Please Provide Phil-Health NO.').notEmpty();

    var errors = req.validationErrors();  
    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};