'use strict';

exports.validateReligion = function(req, res, next) {
    req.checkBody('Religion', 'Please Provide A Type Of Religion.').notEmpty();
    req.checkBody('ShortName', 'Please Provide A Religion Short Name.').notEmpty();
    req.checkBody('Description', 'Please Provide A Religion Description').notEmpty();

   
    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};