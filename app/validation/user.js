'use strict';
var _ = require('lodash-node');

exports.validateLogin = function (req, res, next) {

    req.checkBody('UserName', 'Please provide your Username').notEmpty();
    req.checkBody('Email', 'Please provide your Email Address').notEmpty();
    req.checkBody('Email', 'Email address needs to be in the format yourname@domain.com.').isEmail();
    req.checkBody('Password', 'Please provide your Password').notEmpty();
    req.checkBody('Password', 'Password should be between 6-12 characters long.').len(6, 12);

    req.assert('ConfirmPassword', 'Password and Confirm Password do not match').equals(req.body.Password);

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};


exports.validateBasicAuth = function (req, res, next) {
    var auth = req.headers['authorization'];

    var uname = {
        param: "username",
        msg: "Please provide your Username"
    };

    var pword = {
        param: "password",
        msg: "Please provide your Password"
    }

    if (!auth) {
        res.status(401).send({
            response: {
                result: [uname, pword],
                msg: '',
                success: false
            },
            statusCode: 401
        });
    } else if (auth) {
        var tmp = auth.split(' ');
        var buf = new Buffer(tmp[1], 'base64');
        var plain_auth = buf.toString();
        var creds = plain_auth.split(':');
        var username = creds[0];
        var password = creds[1];

        if (_.isEmpty(username) && _.isEmpty(password)) {
            res.status(401).send({
                response: {
                    result: [uname, pword],
                    msg: '',
                    success: false
                },
                statusCode: 401
            });
        } else if (_.isEmpty(username) && !_.isEmpty(password)) {
            res.status(401).send({
                response: {
                    result: [uname],
                    msg: '',
                    success: false
                },
                statusCode: 401
            });
        } else if (!_.isEmpty(username) && _.isEmpty(password)) {
            res.status(401).send({
                response: {
                    result: [pword],
                    msg: '',
                    success: false
                },
                statusCode: 401
            });
        } else if (username == 'undefined' && password == 'undefined') {
            res.status(401).send({
                response: {
                    result: [uname, pword],
                    msg: '',
                    success: false
                },
                statusCode: 401
            });
        } else if (!_.isEmpty(username) && password == 'undefined') {
            res.status(401).send({
                response: {
                    result: [pword],
                    msg: '',
                    success: false
                },
                statusCode: 401
            });
        } else if (username == 'undefined' && !_.isEmpty(password)) {
            res.status(401).send({
                response: {
                    result: [uname],
                    msg: '',
                    success: false
                },
                statusCode: 401
            });
        } else {
            next();
        }
    }
};

exports.validateForgot = (req, res, next) => {
    req.checkBody('username', 'Please provide your Account Username').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};