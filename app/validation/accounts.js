'use strict';

exports.validateAccounts = function(req, res, next) {
    req.checkBody('AcctCode', 'Please Provide An Account-Code').notEmpty();
    req.checkBody('AcctName', 'Please Provide An Account Name').notEmpty();
    req.checkBody('ShortName', 'Please Provide A Short Name').notEmpty();
    req.checkBody('TypeID', 'Please Provide Account Type').notEmpty();
    req.checkBody('GroupID', 'Please Provide Account Group').notEmpty();
    req.checkBody('ClassID', 'Please Provide Account Classification').notEmpty();
    req.checkBody('Inactive', 'Please Confrim Account Inactivity').notEmpty();

   var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({  
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};

exports.validateAccountType = function(req, res, next) {
    req.checkBody('CategoryCode', 'Please Provide An Account Type Category').notEmpty();
    req.checkBody('CategoryName', 'Please Provide An Account Type Name').notEmpty();
    req.checkBody('CategoryShort', 'Please Provide A Short Description').notEmpty();


   var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({  
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};



exports.validateAccountClassfication = function(req, res, next) {
    req.checkBody('ClassCode', 'Please Provide A Class Code').notEmpty();
    req.checkBody('ClassName', 'Please Provide A Class Name').notEmpty();
    req.checkBody('ClassShort', 'Please Provide A Class Short Name').notEmpty()

   var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({  
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};


exports.validateAccountGroup = function(req, res, next) {
    req.checkBody('GroupCode', 'Please Provide An Account Group Code').notEmpty();
    req.checkBody('GroupName', 'Please Provide An Account Group Name').notEmpty();
    req.checkBody('GroupShort', 'Please Provide A Short Group Description').notEmpty();
  
   var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({  
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};