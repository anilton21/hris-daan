'use strict';

exports.validateEmployee = function(req, res, next) {

    req.checkBody('e_idno', 'Please enter Employee ID').notEmpty();
    req.checkBody('e_lname', 'Please enter Employee Last Name').notEmpty();
    req.checkBody('e_fname', 'Please enter Employee First Name').notEmpty();
    req.checkBody('e_mname', 'Please enter Employee Middle Name').notEmpty();
    req.checkBody('e_civil_status', 'Please select Employee Civil Status').notEmpty();
    req.checkBody('tp_id', 'Please select Employee Payroll Term').notEmpty();
    req.checkBody('e_dailyrate', 'Please enter Employee Daily Rate').notEmpty();
    req.checkBody('e_hourlyrate', 'Please enter Employee Hourly Rate').notEmpty();
    req.checkBody('e_monthlyrate', 'Please enter Employee Monthly Rate').notEmpty();
    req.checkBody('s_id', 'Please select Employment Status').notEmpty();
    req.checkBody('branch_id', 'Please select Employment Branch').notEmpty();
    req.checkBody('DeptID', 'Please select Employment Department').notEmpty();
    req.checkBody('jt_id', 'Please enter Employment Position/Job Title').notEmpty();
    req.checkBody('tp_id', 'Please select Employee Exemption Bracket').notEmpty();

    if (req.body.IsSSS) {
        req.checkBody('e_ssno', 'Please enter SSS No.').notEmpty();
    }

    if (req.body.IsPh) {
        req.checkBody('e_ssno', 'Please enter PHIL HEALTH No.').notEmpty();
    }

    if (req.body.IsPagIbig) {
        req.checkBody('e_ssno', 'Please enter HDMF No.').notEmpty();
    }

    if (req.body.IsTin) {
        req.checkBody('e_ssno', 'Please enter TIN No.').notEmpty();
    }


    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({  
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};