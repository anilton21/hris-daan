'use strict';

var env = process.env.NODE_ENV || 'development';
var config = require('../../config/environment/' + env);

var mysql = require('mysql');
const Sequelize = require('sequelize');


var Database = function() {

    var self = this;
    self.configuration = {
        host: config.db_host,
        user: config.db_user,
        password: config.db_password,
        database: config.db_database,
        port: config.db_port,
        connectTimeout: 70000,
        dateStrings: true,
        multipleStatements: true
    };

    self.connect = function onConnect(cb) {
        const sequelize = new Sequelize(config.db_database, config.db_user, config.db_password, {
            host: config.db_host,
            dialect: 'mysql',
            pool: {
                max: 1000,
                min: 0,
                idle: 10000
            }
        });

        sequelize
            .authenticate()
            .then(() => {
                console.log('Connection has been established successfully.');
                return cb(null,true);
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
                return cb(err, false);
            });
    };

    self.query = function onQuery(sql, cb) {
        var connection = mysql.createConnection(self.configuration);
        connection.query(sql, function(err, rows) {
            if (err) {
                return cb(true, err);
            }
            return cb(false, rows);
        });
        connection.end();
    };

    self.insertWithId = function onQuery(sql, cb) {
        var configuration = {
            host: config.db_host,
            user: config.db_user,
            password: config.db_password,
            database: config.db_database
        };

        var connection = mysql.createConnection(configuration);
        connection.query(sql, function(err, result) {
            if (err) {
                return cb(true, err);
            }
            return cb(false, result.insertId);
        });
        connection.end();
    };

    self.insertBulkWithId = function onQuery(sql, data, cb) {
        var connection = mysql.createConnection(self.configuration);
        connection.query(sql, data, function(err, result) {
            if (err) {
                return cb(true, err);
            }
            return cb(false, result.insertId);
        });
        connection.end();
    };

    self.actionQuery = function onQuery(sql, cb) {
        var connection = mysql.createConnection(self.configuration);
        connection.query(sql, function(err, result) {
            if (err) {
                return cb(true, err);
            }
            return cb(false, result.affectedRows);
        });
        connection.end();
    };
};


exports.Database = Database;
