'use strict';

var env = process.env.NODE_ENV || 'development';
var config = require('../../config/environment/' + env);

var fs = require('fs');


var getTotalYears = function (dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
};

var existsSync = function (filePath) {
    if (!filePath) return false
    try {
        return fs.statSync(filePath).isFile()
    } catch (e) {
        return false
    }
};

var generateString = function (length) {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
};

var generateNumberString = function (length) {
    var text = '';
    var possible = '0123456789';

    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
};

var sendMail = function (data, next) {
    var request = require('request');
    var helper = require('sendgrid').mail;

    var objectData = {};

    if (data.attachment) {
        var file = request(data.attachment);
        objectData = {
            from: data.from,
            to: data.email,
            subject: data.subject,
            html: data.message,
            attachment: file
        };

    } else {
        objectData = {
            from: data.from,
            to: data.email,
            subject: data.subject,
            html: data.message,
            attachment: null
        };
    }

    var from_email = new helper.Email(objectData.from);
    var to_email = new helper.Email(objectData.to);
    var subject = objectData.subject;
    var content = new helper.Content('text/html', objectData.html);
    var mail = new helper.Mail(from_email, subject, to_email, content);

    if(objectData.attachment){
        var attachment = new helper.Attachment();
        var file = fs.readFileSync(objectData.attachment);
        var base64File = new Buffer(file).toString('base64');
        attachment.setContent(base64File);
        attachment.setType('application/text');
        attachment.setFilename(objectData.filename);
        attachment.setDisposition('attachment');
        mail.addAttachment(attachment);
    }

    var sg = require('sendgrid')(config.sendgrid_key);
    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON(),
    });

    sg.API(request, function (error, response) {
        if (error) {
            next(error, null);
        }

        console.log('statusCode: ', response.statusCode);
        console.log('body: ', response.body);
        console.log('headers: ', response.headers);

        if (response.statusCode == 200 || response.statusCode == 202) {
            next(null, {
                result: response.body,
                msg: 'Email Successfully Sent',
                success: true
            });
        } else {
            next({
                result: error,
                msg: error.message,
                success: false
            }, null);
        }
    });
};


exports.getTotalYears = getTotalYears;
exports.existsSync = existsSync;
exports.generateString = generateString;
exports.generateNumberString = generateNumberString;
exports.sendMail = sendMail;