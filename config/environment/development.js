/*jshint camelcase: false */

'use strict';

module.exports = {
    env : 'development',
    db_host: process.env.DB_HOST || 'localhost',
    db_user: process.env.DB_USER || 'root',
    db_password: process.env.DB_USER || '',
    db_database: 'hr_4loop',
    db_port: 3306,
    port: 3002, // PLEASE DONT REMOVE 'process.env.PORT'
    ip: process.env.IP,
    socket_port: process.env.SOCKET_PORT || 3333,
    app_name: process.env.APP_NAME || 'HRIS',
    api_host_url: process.env.API_HOST_URL || 'http://localhost:3002',
    frontend_host_url: process.env.FRONTEND_HOST_URL || 'http://localhost:9000',
    api_version: process.env.API_VERSION || '/api/v1',
    token_secret: 'HRIS',
    app_secretkey: 'ChickenPitaka?!56',
    sendgrid_key: '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',
    sendgrid_api_key: 'BrtUeIfqQbK8Y_sZo_lkxA',
};