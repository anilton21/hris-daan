(function () {
    'use strict';

    angular.module('hris')
        .controller('payrollSetupCtrl', payrollSetupCtrl);

    payrollSetupCtrl.$inject = ['$scope', '$state', 'Payroll', 'localStorageService', 'ngTableParams', '$filter', 'ngDialog', 'toastr', '$timeout'];

    function payrollSetupCtrl($scope, $state, Payroll, localStorageService, ngTableParams, $filter, ngDialog, toastr, $timeout) {
        $scope.payrolls = [];
        $scope.paytypes = [];
        $scope.paymenttypes = [];
        $scope.months = [];
        $scope.years = [];
        $scope.txtSearch = '';
        $scope.payroll = {};
        var curDate = new Date();

        $scope.IsDisable = true;


        $scope.popup1 = {
            opened: false
        };
        $scope.popup2 = {
            opened: false
        };
        $scope.dateOptions = {
            formatYear: 'yyyy'
        };

        function getMonthFromString(mon) {
            return new Date(Date.parse(mon + " 1, 2012")).getMonth()
        }

        for (var index = 0; index < 1980; index++) {
            var nowYear = curDate.getFullYear() - index;
            if (parseInt(nowYear) != 1980) {
                $scope.years.push(nowYear)
            } else {
                break;
            }
        }

        async.waterfall([
            function (callback) {
                Payroll.getAllPayTypes().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var paytypes = data.response.result;
                        if (!_.isEmpty(paytypes)) {
                            $scope.paytypes = paytypes;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                Payroll.getAllPaymentTypes().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var paymenttypes = data.response.result;
                        if (!_.isEmpty(paymenttypes)) {
                            _.remove(paymenttypes,{'PaymentID': 1})
                            $scope.paymenttypes = paymenttypes;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                Payroll.getAllMonths().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var months = data.response.result;
                        if (!_.isEmpty(months)) {
                            $scope.months = months;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                $scope.payroll.FirstDate = curDate;
                $scope.payroll.SecondDate = curDate;
                $scope.payroll.UsedYear = curDate.getFullYear();
                $scope.payroll.UsedMonth = moment(curDate).format("MMMM");
                $scope.payroll.PaymentType = 2;
                $scope.payroll.PayrollCode = $scope.payroll.UsedYear + $scope.payroll.UsedMonth + 'MONTH';
                $scope.payroll.CategoryID = 1;

            }
        ])


        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            filter: $scope.txtSearch
        }, {
            getData: function ($defer, params) {
                Payroll.getPayrollConfig().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var payrolls = data.response.result;
                        if (params.filter().text) {
                            $scope.payrolls = $filter('filter')(payrolls, params.filter().text);
                        } else {
                            $scope.payrolls = payrolls;
                        }
                        _.each($scope.payrolls, function (row) {
                            row.selected = false;
                        });
                        params.total($scope.payrolls.length);
                        $defer.resolve($scope.payrolls.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
            }
        });

        $scope.refreshData = function () {
            $scope.tableParams.reload();
        };

        $scope.addEntry = function () {
            $scope.IsDisable = false;
            $scope.payroll.FirstDate = new Date(curDate.getFullYear(), curDate.getMonth(), 1);
            $scope.payroll.SecondDate = new Date(curDate.getFullYear(), curDate.getMonth() + 1, 0);
        };

        $scope.changePeriod = function () {
            switch ($scope.payroll.PaymentTypes.EntryID) {
                case 2:
                    $scope.payroll.PayrollCode = $scope.payroll.UsedYear + $scope.payroll.UsedMonth + 'MONTH';
                    $scope.payroll.CategoryID = 1;
                    break;
                case 3:
                    $scope.payroll.PayrollCode = $scope.payroll.UsedYear + $scope.payroll.UsedMonth + '1STM';
                    $scope.payroll.CategoryID = 2;
                    break;
                case 4:
                    $scope.payroll.PayrollCode = $scope.payroll.UsedYear + $scope.payroll.UsedMonth + '2NDM';
                    $scope.payroll.CategoryID = 2;
                    break;
                default:
                    $scope.payroll.PayrollCode = $scope.payroll.UsedYear + $scope.payroll.UsedMonth + 'MONTH';
                    $scope.payroll.CategoryID = 1;
                    break;
            }
            $scope.payroll.PaymentType = $scope.payroll.PaymentTypes.EntryID;
            $scope.payroll.Term = $scope.payroll.PaymentTypes.PaymentTerm;
            $scope.payroll.FirstDate = new Date($scope.payroll.UsedYear, getMonthFromString($scope.payroll.UsedMonth), 1);
            $scope.payroll.SecondDate = new Date($scope.payroll.UsedYear, getMonthFromString($scope.payroll.UsedMonth) + 1, 0);
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.updatePayrollPeriod = function (payroll) {
            $scope.payroll = payroll;
            $scope.payroll.FirstDate = new Date(payroll.FirstDate);
            $scope.payroll.SecondDate = new Date(payroll.SecondDate);
            $scope.payroll.UsedYear = parseInt(payroll.UsedYear);
            $scope.IsDisable = false;
        };

        $scope.deletePayrollPeriod = function (payroll) {
            $scope.modal = {
                title: 'Payroll Setup Configuration',
                message: 'Are you sure to delete this Configuration?'
            };

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {
                Payroll.deletePayrollConfig(payroll.UUID).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $state.reload();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            });
        };

        $scope.savePayroll = function () {
            if ($scope.payroll) {
                if ($scope.payroll.PayrollID) {
                    Payroll.updatePayrollConfig($scope.payroll.UUID, $scope.payroll).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'SUCCESS');
                            $timeout(function () {
                                $state.reload();
                            }, 300);
                        } else {
                            toastr.error(data.response.msg, 'ERROR');
                            return;
                        }
                    });
                } else {
                    Payroll.createPayrollConfig($scope.payroll).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'SUCCESS');
                            $timeout(function () {
                                $state.reload();
                            }, 300);
                        } else {
                            toastr.error(data.response.msg, 'ERROR');
                            return;
                        }
                    });
                }
            }
        };

    }
})();