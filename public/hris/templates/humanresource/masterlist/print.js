(function() {
    'use strict';

    angular.module('hris')
        .controller('employeeMasterlistPrintCtrl', ['$scope', '$stateParams', 'employee', 'company', function($scope, $stateParams, employee, company) {
            console.log('employeeMasterlistPrintCtrl: ');

            $scope.employee = [];
            $scope.company = {};
            $scope.action = {};

            console.log('$stateParams: ', $stateParams);

            async.waterfall([
                function(callback) {
                    company.getCompanyInfo().then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            var company = data.response.result;
                            if (!_.isEmpty(company)) {
                                $scope.company = company;
                                if ($scope.company.ci_logo) {
                                    $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;
                                }
                                callback();
                            }
                        }
                    });
                },
                function(callback) {
                    $scope.action = {
                        action: $stateParams.action,
                        _id: $stateParams._id
                    };
                    employee.getEmployeeMasterlist($scope.action).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            var employees = data.response.result;
                            if (!_.isEmpty(employees)) {
                                $scope.employee = employees;
                            }
                        }
                    });
                }
            ]);





            $scope.refreshData = function() {
                $scope.employee = [];
                $scope.company = {};

                async.waterfall([
                    function(callback) {
                        company.getCompanyInfo().then(function(data) {
                            if (data.statusCode == 200 && data.response.success) {
                                var company = data.response.result;
                                if (!_.isEmpty(company)) {
                                    $scope.company = company;
                                    if ($scope.company.ci_logo) {
                                        $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;
                                    }
                                    callback();
                                }
                            }
                        });
                    },
                    function(callback) {
                        $scope.action = {
                            action: $stateParams.action,
                            _id: $stateParams._id
                        };
                        employee.getEmployeeMasterlist($scope.action).then(function(data) {
                            if (data.statusCode == 200 && data.response.success) {
                                var employees = data.response.result;
                                if (!_.isEmpty(employees)) {
                                    $scope.employee = employees;
                                }
                            }
                        });
                    }
                ]);
            };
        }]);
})();