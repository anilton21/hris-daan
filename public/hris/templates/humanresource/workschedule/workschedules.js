(function () {
    'use strict';

    angular.module('hris')
        .controller('worksSchedulesCtrl', worksSchedulesCtrl)
        .controller('worksSchedulesModalCtrl', worksSchedulesModalCtrl)
        .controller('worksSchedulesTemplateModalCtrl', worksSchedulesTemplateModalCtrl);

    worksSchedulesCtrl.$inject = ['$scope', 'workschedules', 'ngTableParams', '$uibModal', 'ngDialog', 'toastr', '$timeout'];
    worksSchedulesModalCtrl.$inject = ['$scope', '$uibModalInstance', 'workschedules', 'toastr', 'SchedID', '$timeout'];
    worksSchedulesTemplateModalCtrl.$inject = ['$scope', '$uibModalInstance', 'workschedules', 'toastr', 'template', '$timeout'];


    function worksSchedulesCtrl($scope, workschedules, ngTableParams, $uibModal, ngDialog, toastr, $timeout) {
        $scope.workschedules = [];

        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 20, // count per page
        }, {
            getData: function ($defer, params) {
                workschedules.getAllWorkSchedules().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var workschedule = data.response.result;
                        $scope.workschedules = workschedule;
                        params.total($scope.workschedules.length);
                        $defer.resolve($scope.workschedules.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
            }
        });

        $scope.refreshData = function () {
            $scope.tableParams.reload();
        };

        $scope.addEntry = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/workschedule/workschedule.modal.html',
                controller: 'worksSchedulesModalCtrl',
                size: 'md',
                resolve: {
                    SchedID: function () {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.refreshData();
            }, function () {});
        };

        $scope.generateTemplate = function (template) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/workschedule/template.modal.html',
                controller: 'worksSchedulesTemplateModalCtrl',
                size: 'lg',
                resolve: {
                    template: function () {
                        return template;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.refreshData();
            }, function () {});
        };

        $scope.updateSchedule = function (SchedID) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/workschedule/workschedule.modal.html',
                controller: 'worksSchedulesModalCtrl',
                size: 'md',
                resolve: {
                    SchedID: function () {
                        return SchedID;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.refreshData();
            }, function () {});
        };

        $scope.deleteSchedule = function (SchedID) {
            $scope.modal = {
                title: 'Employee Attendance',
                message: 'Are you sure to delete this work schedule?'
            };

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {
                workschedules.Delete_Work_Schedule(SchedID).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $scope.refreshData();
                        }, 300);
                    } else {
                        toastr.warning(data.response.msg, 'WARNING');
                        return;
                    }
                });
            });
        };
    }


    function worksSchedulesModalCtrl($scope, $uibModalInstance, workschedules, toastr, SchedID, $timeout) {
        console.log('worksSchedulesModalCtrl')
        $scope.workTemplate = {};

        if (SchedID) {
            workschedules.getWorkScheduleDetails(SchedID).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    var workTemplate = data.response.result;
                    if (!_.isEmpty(workTemplate)) {
                        $scope.workTemplate = workTemplate;
                    }
                }
            });
        }

        $scope.saveEntry = function () {
            if (SchedID) {
                workschedules.Update_Work_Schedule(SchedID, $scope.workTemplate).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $timeout(function () {
                            $uibModalInstance.close('updated');
                        }, 300);
                    }
                });
            } else {
                workschedules.Add_Work_Schedule($scope.workTemplate).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $timeout(function () {
                            $uibModalInstance.close('save');
                        }, 300);
                    }
                });
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }


    function worksSchedulesTemplateModalCtrl($scope, $uibModalInstance, workschedules, toastr, template, $timeout) {
        $scope.workTemplate = template;
        $scope.workDays = [];

        $scope.mytime = new Date();
        $scope.hstep = 1;
        $scope.mstep = 1
        $scope.ismeridian = false;

        workschedules.getAllDays().then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var workDays = data.response.result;
                if (!_.isEmpty(workDays)) {
                    $scope.workDays = workDays;
                }
            }
        });

        if ($scope.workTemplate) {
            workschedules.getWorkSchedulesTemplates($scope.workTemplate.UUID).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    var templates = data.response.result;
                    if (!_.isEmpty(templates)) {
                        var template = {
                            IN01: new Date('2010-01-01 ' + templates.IN01),
                            OUT01: new Date('2010-01-01 ' + templates.OUT01),
                            IN02: new Date('2010-01-01 ' + templates.IN02),
                            OUT02: new Date('2010-01-01 ' + templates.OUT02),
                            IN03: new Date('2010-01-01 ' + templates.IN03),
                            OUT03: new Date('2010-01-01 ' + templates.OUT03),
                            IN11: new Date('2010-01-01 ' + templates.IN11),
                            OUT11: new Date('2010-01-01 ' + templates.OUT11),
                            IN12: new Date('2010-01-01 ' + templates.IN12),
                            OUT12: new Date('2010-01-01 ' + templates.OUT12),
                            IN13: new Date('2010-01-01 ' + templates.IN13),
                            OUT13: new Date('2010-01-01 ' + templates.OUT13),
                            IN21: new Date('2010-01-01 ' + templates.IN21),
                            OUT21: new Date('2010-01-01 ' + templates.OUT21),
                            IN22: new Date('2010-01-01 ' + templates.IN22),
                            OUT22: new Date('2010-01-01 ' + templates.OUT22),
                            IN23: new Date('2010-01-01 ' + templates.IN23),
                            OUT23: new Date('2010-01-01 ' + templates.OUT23),
                            IN31: new Date('2010-01-01 ' + templates.IN31),
                            OUT31: new Date('2010-01-01 ' + templates.OUT31),
                            IN32: new Date('2010-01-01 ' + templates.IN32),
                            OUT32: new Date('2010-01-01 ' + templates.OUT32),
                            IN33: new Date('2010-01-01 ' + templates.IN33),
                            OUT33: new Date('2010-01-01 ' + templates.OUT33),
                            IN41: new Date('2010-01-01 ' + templates.IN41),
                            OUT41: new Date('2010-01-01 ' + templates.OUT41),
                            IN42: new Date('2010-01-01 ' + templates.IN42),
                            OUT42: new Date('2010-01-01 ' + templates.OUT42),
                            IN43: new Date('2010-01-01 ' + templates.IN43),
                            OUT43: new Date('2010-01-01 ' + templates.OUT43),
                            IN51: new Date('2010-01-01 ' + templates.IN51),
                            OUT51: new Date('2010-01-01 ' + templates.OUT51),
                            IN52: new Date('2010-01-01 ' + templates.IN52),
                            OUT52: new Date('2010-01-01 ' + templates.OUT52),
                            IN53: new Date('2010-01-01 ' + templates.IN53),
                            OUT53: new Date('2010-01-01 ' + templates.OUT53),
                            IN61: new Date('2010-01-01 ' + templates.IN61),
                            OUT61: new Date('2010-01-01 ' + templates.OUT61),
                            IN62: new Date('2010-01-01 ' + templates.IN62),
                            OUT62: new Date('2010-01-01 ' + templates.OUT62),
                            IN63: new Date('2010-01-01 ' + templates.IN63),
                            OUT63: new Date('2010-01-01 ' + templates.OUT63)
                        };

                        _.each($scope.workDays, function (row) {
                            if (row.IndexID == 1) {
                                row.AMIn = template.IN01;
                                row.AMOut = template.OUT01;
                                row.PMIn = template.IN02;
                                row.PMOut = template.OUT02;
                                row.BRIn = template.IN03;
                                row.BROut = template.OUT03;
                            } else if (row.IndexID == 2) {
                                row.AMIn = template.IN11;
                                row.AMOut = template.OUT11;
                                row.PMIn = template.IN12;
                                row.PMOut = template.OUT12;
                                row.BRIn = template.IN13;
                                row.BROut = template.OUT13;
                            } else if (row.IndexID == 3) {
                                row.AMIn = template.IN21;
                                row.AMOut = template.OUT21;
                                row.PMIn = template.IN22;
                                row.PMOut = template.OUT22;
                                row.BRIn = template.IN23;
                                row.BROut = template.OUT23;
                            } else if (row.IndexID == 4) {
                                row.AMIn = template.IN31;
                                row.AMOut = template.OUT31;
                                row.PMIn = template.IN32;
                                row.PMOut = template.OUT32;
                                row.BRIn = template.IN33;
                                row.BROut = template.OUT33;
                            } else if (row.IndexID == 5) {
                                row.AMIn = template.IN41;
                                row.AMOut = template.OUT41;
                                row.PMIn = template.IN42;
                                row.PMOut = template.OUT42;
                                row.BRIn = template.IN43;
                                row.BROut = template.OUT43;
                            } else if (row.IndexID == 6) {
                                row.AMIn = template.IN51;
                                row.AMOut = template.OUT51;
                                row.PMIn = template.IN52;
                                row.PMOut = template.OUT52;
                                row.BRIn = template.IN53;
                                row.BROut = template.OUT53;
                            } else if (row.IndexID == 7) {
                                row.AMIn = template.IN61;
                                row.AMOut = template.OUT61;
                                row.PMIn = template.IN62;
                                row.PMOut = template.OUT62;
                                row.BRIn = template.IN63;
                                row.BROut = template.OUT63;
                            }
                        });
                    }else{
                        var template = {
                            IN01: new Date('2010-01-01 00:00:00'),
                            OUT01: new Date('2010-01-01 00:00:00'),
                            IN02: new Date('2010-01-01 00:00:00'),
                            OUT02: new Date('2010-01-01 00:00:00'),
                            IN03: new Date('2010-01-01 00:00:00'),
                            OUT03: new Date('2010-01-01 00:00:00'),
                            IN11: new Date('2010-01-01 00:00:00'),
                            OUT11: new Date('2010-01-01 00:00:00'),
                            IN12: new Date('2010-01-01 00:00:00'),
                            OUT12: new Date('2010-01-01 00:00:00'),
                            IN13: new Date('2010-01-01 00:00:00'),
                            OUT13: new Date('2010-01-01 00:00:00'),
                            IN21: new Date('2010-01-01 00:00:00'),
                            OUT21: new Date('2010-01-01 00:00:00'),
                            IN22: new Date('2010-01-01 00:00:00'),
                            OUT22: new Date('2010-01-01 00:00:00'),
                            IN23: new Date('2010-01-01 00:00:00'),
                            OUT23: new Date('2010-01-01 00:00:00'),
                            IN31: new Date('2010-01-01 00:00:00'),
                            OUT31: new Date('2010-01-01 00:00:00'),
                            IN32: new Date('2010-01-01 00:00:00'),
                            OUT32: new Date('2010-01-01 00:00:00'),
                            IN33: new Date('2010-01-01 00:00:00'),
                            OUT33: new Date('2010-01-01 00:00:00'),
                            IN41: new Date('2010-01-01 00:00:00'),
                            OUT41: new Date('2010-01-01 00:00:00'),
                            IN42: new Date('2010-01-01 00:00:00'),
                            OUT42: new Date('2010-01-01 00:00:00'),
                            IN43: new Date('2010-01-01 00:00:00'),
                            OUT43: new Date('2010-01-01 00:00:00'),
                            IN51: new Date('2010-01-01 00:00:00'),
                            OUT51: new Date('2010-01-01 00:00:00'),
                            IN52: new Date('2010-01-01 00:00:00'),
                            OUT52: new Date('2010-01-01 00:00:00'),
                            IN53: new Date('2010-01-01 00:00:00'),
                            OUT53: new Date('2010-01-01 00:00:00'),
                            IN61: new Date('2010-01-01 00:00:00'),
                            OUT61: new Date('2010-01-01 00:00:00'),
                            IN62: new Date('2010-01-01 00:00:00'),
                            OUT62: new Date('2010-01-01 00:00:00'),
                            IN63: new Date('2010-01-01 00:00:00'),
                            OUT63: new Date('2010-01-01 00:00:00')
                        };

                        _.each($scope.workDays, function (row) {
                            if (row.IndexID == 1) {
                                row.AMIn = template.IN01;
                                row.AMOut = template.OUT01;
                                row.PMIn = template.IN02;
                                row.PMOut = template.OUT02;
                                row.BRIn = template.IN03;
                                row.BROut = template.OUT03;
                            } else if (row.IndexID == 2) {
                                row.AMIn = template.IN11;
                                row.AMOut = template.OUT11;
                                row.PMIn = template.IN12;
                                row.PMOut = template.OUT12;
                                row.BRIn = template.IN13;
                                row.BROut = template.OUT13;
                            } else if (row.IndexID == 3) {
                                row.AMIn = template.IN21;
                                row.AMOut = template.OUT21;
                                row.PMIn = template.IN22;
                                row.PMOut = template.OUT22;
                                row.BRIn = template.IN23;
                                row.BROut = template.OUT23;
                            } else if (row.IndexID == 4) {
                                row.AMIn = template.IN31;
                                row.AMOut = template.OUT31;
                                row.PMIn = template.IN32;
                                row.PMOut = template.OUT32;
                                row.BRIn = template.IN33;
                                row.BROut = template.OUT33;
                            } else if (row.IndexID == 5) {
                                row.AMIn = template.IN41;
                                row.AMOut = template.OUT41;
                                row.PMIn = template.IN42;
                                row.PMOut = template.OUT42;
                                row.BRIn = template.IN43;
                                row.BROut = template.OUT43;
                            } else if (row.IndexID == 6) {
                                row.AMIn = template.IN51;
                                row.AMOut = template.OUT51;
                                row.PMIn = template.IN52;
                                row.PMOut = template.OUT52;
                                row.BRIn = template.IN53;
                                row.BROut = template.OUT53;
                            } else if (row.IndexID == 7) {
                                row.AMIn = template.IN61;
                                row.AMOut = template.OUT61;
                                row.PMIn = template.IN62;
                                row.PMOut = template.OUT62;
                                row.BRIn = template.IN63;
                                row.BROut = template.OUT63;
                            }
                        });
                    }
                    console.log('$scope.workDays: ', $scope.workDays);
                }
            })
        }


        $scope.saveEntry = function () {
            var template = {};
            template.SchedID = $scope.workTemplate.SchedID;
            _.each($scope.workDays, function (row) {
                if (row.IndexID == 1) {
                    template.IN01 = moment(row.AMIn).format('HH:mm:ss');
                    template.OUT01 = moment(row.AMOut).format('HH:mm:ss');
                    template.IN02 = moment(row.PMIn).format('HH:mm:ss');
                    template.OUT02 = moment(row.PMOut).format('HH:mm:ss');
                    template.IN03 = moment(row.BRIn).format('HH:mm:ss');
                    template.OUT03 = moment(row.BROut).format('HH:mm:ss');
                } else if (row.IndexID == 2) {
                    template.IN11 = moment(row.AMIn).format('HH:mm:ss');
                    template.OUT11 = moment(row.AMOut).format('HH:mm:ss');
                    template.IN12 = moment(row.PMIn).format('HH:mm:ss');
                    template.OUT12 = moment(row.PMOut).format('HH:mm:ss');
                    template.IN13 = moment(row.BRIn).format('HH:mm:ss');
                    template.OUT13 = moment(row.BROut).format('HH:mm:ss');
                } else if (row.IndexID == 3) {
                    template.IN21 = moment(row.AMIn).format('HH:mm:ss');
                    template.OUT21 = moment(row.AMOut).format('HH:mm:ss');
                    template.IN22 = moment(row.PMIn).format('HH:mm:ss');
                    template.OUT22 = moment(row.PMOut).format('HH:mm:ss');
                    template.IN23 = moment(row.BRIn).format('HH:mm:ss');
                    template.OUT23 = moment(row.BROut).format('HH:mm:ss');
                } else if (row.IndexID == 4) {
                    template.IN31 = moment(row.AMIn).format('HH:mm:ss');
                    template.OUT31 = moment(row.AMOut).format('HH:mm:ss');
                    template.IN32 = moment(row.PMIn).format('HH:mm:ss');
                    template.OUT32 = moment(row.PMOut).format('HH:mm:ss');
                    template.IN33 = moment(row.BRIn).format('HH:mm:ss');
                    template.OUT33 = moment(row.BROut).format('HH:mm:ss');
                } else if (row.IndexID == 5) {
                    template.IN41 = moment(row.AMIn).format('HH:mm:ss');
                    template.OUT41 = moment(row.AMOut).format('HH:mm:ss');
                    template.IN42 = moment(row.PMIn).format('HH:mm:ss');
                    template.OUT42 = moment(row.PMOut).format('HH:mm:ss');
                    template.IN43 = moment(row.BRIn).format('HH:mm:ss');
                    template.OUT43 = moment(row.BROut).format('HH:mm:ss');
                } else if (row.IndexID == 6) {
                    template.IN51 = moment(row.AMIn).format('HH:mm:ss');
                    template.OUT51 = moment(row.AMOut).format('HH:mm:ss');
                    template.IN52 = moment(row.PMIn).format('HH:mm:ss');
                    template.OUT52 = moment(row.PMOut).format('HH:mm:ss');
                    template.IN53 = moment(row.BRIn).format('HH:mm:ss');
                    template.OUT53 = moment(row.BROut).format('HH:mm:ss');
                } else if (row.IndexID == 7) {
                    template.IN61 = moment(row.AMIn).format('HH:mm:ss');
                    template.OUT61 = moment(row.AMOut).format('HH:mm:ss');
                    template.IN62 = moment(row.PMIn).format('HH:mm:ss');
                    template.OUT62 = moment(row.PMOut).format('HH:mm:ss');
                    template.IN63 = moment(row.BRIn).format('HH:mm:ss');
                    template.OUT63 = moment(row.BROut).format('HH:mm:ss');
                }
            });
            console.log('$scope.workDays: ', $scope.workDays);
            console.log('template: ', template);

            workschedules.SetWorkScheduleTemplates($scope.workTemplate.UUID, template).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'SUCCESS');
                    $timeout(function () {
                        $uibModalInstance.close('save');
                    }, 300);
                }else{
                    toastr.error(data.response.msg, 'ERROR');
                    return;
                }
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();