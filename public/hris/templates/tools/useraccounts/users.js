(function () {
    'use strict';

    angular.module('hris')
        .controller('usersCtrl', usersCtrl);

    usersCtrl.$inject = ['$scope', '$state', 'users', 'localStorageService', '$location', '$uibModal', 'ngTableParams', 'ngDialog', 'toastr', '$timeout', '$filter'];

    function usersCtrl($scope, $state, users, localStorageService, $location, $uibModal, ngTableParams, ngDialog, toastr, $timeout, $filter) {
        $scope.useraccounts = [];
        $scope.usergroups = [];
        $scope.txtSearch = {
            text: ''
        };
        $scope.selectedItem = {};
        $scope.branches = [];

        $scope.branches.push({
            tree_name: 'Users',
            tree_id: 1
        }, {
            tree_name: 'User Groups',
            tree_id: 2
        });

        async.waterfall([
            function (callback) {
                users.GetAllUserGroups().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var usergroups = data.response.result;
                        if (!_.isEmpty(usergroups)) {
                            $scope.usergroups = usergroups;
                        }

                        _.each($scope.branches, function (row) {
                            if (row.tree_id == 2) {
                                row.departments = _.map($scope.usergroups, function (item) {
                                    return {
                                        tree_name: item.GroupName,
                                        tree_id: item.GroupID
                                    }
                                });
                            }
                        })
                        callback();
                    }
                });
            },
            function (callback) {
                $scope.tableParams = new ngTableParams({
                    page: 1, // show first page
                    count: 20, // count per page
                    filter: $scope.txtSearch,
                    counts: []
                }, {
                    total: 0,
                    counts: [],
                    getData: function ($defer, params) {
                        users.GetAllUsers().then(function (data) {
                            if (data.statusCode == 200 && data.response.success) {
                                var usersD = data.response.result;
                                _.each($scope.branches, function (row) {
                                    if (row.tree_id == 1) {
                                        row.departments = _.map(usersD, function (item) {
                                            return {
                                                tree_name: item.FullName,
                                                tree_id: item.UserID
                                            }
                                        });
                                    }
                                })

                                if (params.filter().text) {
                                    $scope.useraccounts = $filter('filter')(usersD, params.filter().text);
                                } else {
                                    $scope.useraccounts = usersD;
                                }

                                params.total($scope.useraccounts.length);
                                $defer.resolve($scope.useraccounts.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            } else {
                                toastr.error(data.response.msg, 'ERROR');
                                returnl
                            }
                        });
                    }
                });
            }
        ]);


        $scope.newUser = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/tools/useraccounts/user.entry.modal.html',
                controller: 'userModalCtrl',
                size: 'md',
                resolve: {
                    userAcct: function () {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.refreshData();
            }, function () {});
        };

        $scope.updateUser = function (row) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/tools/useraccounts/user.entry.modal.html',
                controller: 'userModalCtrl',
                size: 'md',
                resolve: {
                    userAcct: function () {
                        return row;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.refreshData();
            }, function () {});
        };

        $scope.deleteUser = function (row) {
            $scope.modal = {
                title: 'User Account',
                message: 'Are you sure to delete this user account?'
            };

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {
                users.Delete_User(row.UUID).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $state.reload();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            });
        };

        $scope.setUserGroup = function(UUID){
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/tools/useraccounts/user.setgroup.modal.html',
                controller: 'userSetGroupModalCtrl',
                size: 'md',
                resolve:{
                    user_id: function(){
                        return UUID;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                if (selectedItem) {
                    $scope.tableParams.reload();
                }
            }, function () {});
        };

        $scope.showUserGroups = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/tools/useraccounts/user.groups.modal.html',
                controller: 'userGroupsModalCtrl',
                size: 'md'
            });

            modalInstance.result.then(function (selectedItem) {
                if (selectedItem) {
                    $timeout(function(){
                        $state.go('main.groupprivilege', {
                            group_id: selectedItem
                        });
                    },300);
                }
            }, function () {});
        };

        $scope.searchData = function () {
            $scope.tableParams.reload();
        };

        $scope.refreshData = function () {
            $scope.tableParams.reload();
        };

    }
})();