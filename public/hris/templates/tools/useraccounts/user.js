(function() {
    'use strict';

    angular.module('hris')
        .controller('userModalCtrl', userModalCtrl)
        .controller('userSetGroupModalCtrl', userSetGroupModalCtrl);

    userModalCtrl.$inject = ['$scope', '$state', 'users', 'localStorageService', 'toastr', '$uibModal', '$timeout', '$uibModalInstance', 'userAcct'];

    function userModalCtrl($scope, $state, users, localStorageService, toastr, $uibModal, $timeout, $uibModalInstance, userAcct) {
        $scope.employee = {};
        $scope.user = {};
        $scope.user.IsEmployee = 0;

        $scope.frmEmployeePicker = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/pickemployee.modal.html',
                controller: 'employeePickerCtrl',
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.employee = selectedItem;
                }
            }, function() {});
        };

        $scope.selectEntry = function() {
            $scope.user.FullName = $scope.employee.employee_name;

            if ($scope.user.ConfirmPassword !== $scope.user.Password) {
                toastr.error('User Password confirmation must be the same with User Password', 'ERROR');
                return;
            }

            console.log('$scope.user: ', $scope.user);
            if (userAcct) {
                users.Update_User(userAcct.UUID, $scope.user).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function() {
                            $uibModalInstance.close('update');
                        }, 300)
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            } else {
                users.Add_User($scope.user).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300)
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(resp.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            }
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }

    userSetGroupModalCtrl.$inject = ['$scope', '$uibModalInstance', 'toastr', 'users', 'user_id','$timeout'];

    function userSetGroupModalCtrl($scope, $uibModalInstance, toastr, users, user_id,$timeout) {
        console.log('userSetGroupModalCtrl: ', user_id);
        $scope.usergroups = [];
        $scope.user = {};

        users.GetAllUserGroups().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var usergroups = data.response.result;
                if (!_.isEmpty(usergroups)) {
                    $scope.usergroups = usergroups;
                }
            }
        });

        $scope.selectEntry = function(){
            users.setUserGroup(user_id,$scope.user).then(function(data){
                if(data.statusCode == 200 && data.response.success){
                    toastr.success(data.response.msg,'SUCCESS');
                    $timeout(function(){
                        $uibModalInstance.close('save');
                    },300);
                }else{
                    toastr.error(data.response.msg,'ERROR');
                    return;
                }
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();