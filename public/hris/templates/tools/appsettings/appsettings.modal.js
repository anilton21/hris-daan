(function () {
    'use strict';

    angular.module('hris')
        .controller('appSettingsModalCtrl', appSettingsModalCtrl);

    appSettingsModalCtrl.$inject = ['$scope', '$state', 'appsettings', '$stateParams', '$filter', '$uibModal', '$uibModalInstance', 'toastr', '$timeout', 'details', 'Payroll', 'sss', 'phic', 'wtax', 'accounts'];

    function appSettingsModalCtrl($scope, $state, appsettings, $stateParams, $filter, $uibModal, $uibModalInstance, toastr, $timeout, details, Payroll, sss, phic, wtax, accounts) {
        $scope.setup = {};
        $scope.paytype = {};

        $scope.paymenttypes = [];
        $scope.months = [];
        $scope.paytypes = [];
        $scope.sssData = [];
        $scope.phicData = [];
        $scope.wtaxData = [];
        $scope.accounts = [];
        $scope.accountgroups = [];

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];

        async.waterfall([
            function (callback) {
                Payroll.getAllPaymentTypes().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var paymenttypes = data.response.result;
                        if (!_.isEmpty(paymenttypes)) {
                            $scope.paymenttypes = paymenttypes;
                        }
                    }
                    callback();
                });
            },
            function (callback) {
                accounts.getAllAccounts().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var accounts = data.response.result;
                        if (!_.isEmpty(accounts)) {
                            $scope.accounts = accounts;
                        }
                    }
                    callback();
                })
            },
            function (callback) {
                accounts.getAllAccountGroups().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var accountgroups = data.response.result;
                        if (!_.isEmpty(accountgroups)) {
                            $scope.accountgroups = accountgroups;
                        }
                    }
                    callback();
                });
            },
            function (callback) {
                Payroll.getAllMonths().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var months = data.response.result;
                        if (!_.isEmpty(months)) {
                            $scope.months = months;
                        }
                    }
                    callback();
                });
            },
            function (callback) {
                Payroll.getAllPayTypes().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var paytypes = data.response.result;
                        if (!_.isEmpty(paytypes)) {
                            $scope.paytypes = paytypes;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                sss.getAllSSS().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var sssData = data.response.result;
                        if (!_.isEmpty(sssData)) {
                            $scope.sssData = sssData;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                phic.getAllPHIC().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var phicData = data.response.result;
                        if (!_.isEmpty(phicData)) {
                            $scope.phicData = phicData;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                wtax.getAllWithholdingTax().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var wtaxData = data.response.result;
                        if (!_.isEmpty(wtaxData)) {
                            $scope.wtaxData = wtaxData;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                if (details) {
                    $scope.paytype = {
                        monthly: $scope.paytypes[0].tp_divisor,
                        semimonthly: $scope.paytypes[1].tp_divisor,
                        weekly: $scope.paytypes[2].tp_divisor,
                        daily: $scope.paytypes[3].tp_divisor,
                    };

                    $scope.setup = details;
                    $scope.setup._1STCUTOFF_FROM = $scope.setup['1STCUTOFF_FROM'];
                    $scope.setup._1STCUTOFF_TO = $scope.setup['1STCUTOFF_TO'];
                    $scope.setup._2NDCUTOFF_FROM = $scope.setup['2NDCUTOFF_FROM'];
                    $scope.setup._2NDCUTOFF_TO = $scope.setup['2NDCUTOFF_TO'];
                    $scope.setup._13thMonthPay_AcctID = $scope.setup['13thMonthPay_AcctID'];

                    $scope.setup.BasicPay_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.BasicPay_AccountID
                    });
                    $scope.setup._13thMonthPay_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup['13thMonthPay_AcctID']
                    });
                    $scope.setup.Overtime_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.Overtime_AccountID
                    });
                    $scope.setup.Holiday_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.Holiday_AccountID
                    });
                    $scope.setup.Sunday_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.Sunday_AccountID
                    });
                    $scope.setup.SpecialHoliday_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.SpecialHoliday_AccountID
                    });



                    $scope.setup.SSS_EE_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.SSS_EE_AccountID
                    });
                    $scope.setup.SSS_ER_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.SSS_ER_AccountID
                    });
                    $scope.setup.SSS_EC_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.SSS_EC_AccountID
                    });

                    $scope.setup.PHIC_EE_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.PHIC_EE_AccountID
                    });
                    $scope.setup.PHIC_ER_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.PHIC_ER_AccountID
                    });

                    $scope.setup.Withholding_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.Withholding_AccountID
                    });

                    $scope.setup.PAGIBIG_EE_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.PAGIBIG_EE_AccountID
                    });
                    $scope.setup.PAGIBIG_ER_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.PAGIBIG_ER_AccountID
                    });

                    $scope.setup.Absent_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.Absent_AccountID
                    });

                    $scope.setup.Tardiness_Acct = _.find($scope.accounts, {
                        'AcctID': $scope.setup.Tardiness_AccountID
                    });
                    // console.log('$scope.setup: ', $scope.setup);
                }

            }
        ]);

        $scope.frmAccountPicker = function (action) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/accounting/pickaccount.modal.html',
                controller: 'accountsPickerCtrl',
                size: 'md'
            });
            modalInstance.result.then(function (selectedItem) {
                if (action == 'basic_pay') {
                    $scope.setup.BasicPay_Acct = selectedItem;
                    $scope.setup.BasicPay_AccountID = selectedItem.AcctID;
                } else if (action == '13th') {
                    $scope.setup._13thMonthPay_Acct = selectedItem;
                    $scope.setup['13thMonthPay_AcctID'] = selectedItem.AcctID;
                } else if (action == 'overtime') {
                    $scope.setup.Overtime_Acct = selectedItem;
                    $scope.setup.Overtime_AccountID = selectedItem.AcctID;
                } else if (action == 'holiday') {
                    $scope.setup.Holiday_Acct = selectedItem;
                    $scope.setup.Holiday_AccountID = selectedItem.AcctID;
                } else if (action == 'sunday') {
                    $scope.setup.Sunday_Acct = selectedItem;
                    $scope.setup.Sunday_AccountID = selectedItem.AcctID;
                } else if (action == 'splholiday') {
                    $scope.setup.SpecialHoliday_Acct = selectedItem;
                    $scope.setup.SpecialHoliday_AccountID = selectedItem.AcctID;
                } else if (action == 'sss_ee') {
                    $scope.setup.SSS_EE_Acct = selectedItem;
                    $scope.setup.SSS_EE_AccountID = selectedItem.AcctID;
                } else if (action == 'sss_er') {
                    $scope.setup.SSS_ER_Acct = selectedItem;
                    $scope.setup.SSS_ER_AccountID = selectedItem.AcctID;
                } else if (action == 'sss_ec') {
                    $scope.setup.SSS_EC_Acct = selectedItem;
                    $scope.setup.SSS_EC_AccountID = selectedItem.AcctID;
                } else if (action == 'phic_er') {
                    $scope.setup.PHIC_ER_Acct = selectedItem;
                    $scope.setup.PHIC_ER_AccountID = selectedItem.AcctID;
                } else if (action == 'phic_ee') {
                    $scope.setup.PHIC_EE_Acct = selectedItem;
                    $scope.setup.PHIC_EE_AccountID = selectedItem.AcctID;
                } else if (action == 'hdmf_er') {
                    $scope.setup.PAGIBIG_ER_Acct = selectedItem;
                    $scope.setup.PAGIBIG_EE_AccountID = selectedItem.AcctID;
                } else if (action == 'hdmf_ee') {
                    $scope.setup.PAGIBIG_EE_Acct = selectedItem;
                    $scope.setup.PAGIBIG_EE_AccountID = selectedItem.AcctID;
                } else if (action == 'wtax') {
                    $scope.setup.Withholding_Acct = selectedItem;
                    $scope.setup.Withholding_AccountID = selectedItem.AcctID;
                } else if (action == 'absent') {
                    $scope.setup.Absent_Acct = selectedItem;
                    $scope.setup.Absent_AccountID = selectedItem.AcctID;
                } else if (action == 'tardy') {
                    $scope.setup.Tardiness_Acct = selectedItem;
                    $scope.setup.Tardiness_AccountID = selectedItem.AcctID;
                }
            }, function () {});
        };

        $scope.saveEntry = function () {
            console.log('$scope.setup: ', $scope.setup);

            async.waterfall([
                function (callback) {
                    Payroll.updatePayTypes(1, {
                        divisor: $scope.paytype.monthly
                    }).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            callback();
                        }
                    });
                },
                function (callback) {
                    Payroll.updatePayTypes(2, {
                        divisor: $scope.paytype.semimonthly
                    }).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            callback();
                        }
                    });
                },
                function (callback) {
                    Payroll.updatePayTypes(3, {
                        divisor: $scope.paytype.weekly
                    }).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            callback();
                        }
                    });
                },
                function (callback) {
                    Payroll.updatePayTypes(4, {
                        divisor: $scope.paytype.daily
                    }).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            callback();
                        }
                    });
                },
                function (callback) {
                    appsettings.Update_Application_Settings($scope.setup.UUID, $scope.setup).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'SUCCESS');
                            $timeout(function () {
                                $uibModalInstance.close('save');
                            }, 300);
                        } else {
                            toastr.error(data.response.msg, 'ERROR');
                            return;
                        }
                    });
                }
            ])
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();