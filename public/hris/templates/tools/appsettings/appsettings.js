(function () {
    'use strict';

    angular.module('hris')
        .controller('appSettingsCtrl', appSettingsCtrl);

    appSettingsCtrl.$inject = ['$scope', '$state', 'appsettings', 'ngTableParams', '$filter', 'ngDialog', 'toastr', '$timeout', '$uibModal'];

    function appSettingsCtrl($scope, $state, appsettings, ngTableParams, $filter, ngDialog, toastr, $timeout, $uibModal) {
        $scope.settings = [];
        $scope.txtSearch = '';
        $scope.setup = {};

        $scope.IsDisable = true;

        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            filter: $scope.txtSearch
        }, {
            getData: function ($defer, params) {
                appsettings.GetAllApplicationSettings().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var settings = data.response.result;
                        if (params.filter().text) {
                            $scope.settings = $filter('filter')(settings, params.filter().text);
                        } else {
                            $scope.settings = settings;
                        }
                        params.total($scope.settings.length);
                        $defer.resolve($scope.settings.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
            }
        });

        $scope.addEntry = function () {
            $scope.setup = {};
            $scope.IsDisable = false;
        }

        $scope.refreshData = function () {
            $scope.tableParams.reload();
        };

        $scope.saveAppSettings = function () {
            if ($scope.setup) {
                if ($scope.setup.SetupID) {
                    appsettings.Update_Application_Settings($scope.setup.UUID, $scope.setup).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'SUCCESS');
                            $timeout(function () {
                                $state.reload();
                            }, 300);
                        } else {
                            toastr.error(data.response.msg, 'ERROR');
                            return;
                        }
                    });
                } else {
                    appsettings.Add_Application_Settings($scope.setup).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'SUCCESS');
                            $timeout(function () {
                                $state.reload();
                            }, 300);
                        } else {
                            toastr.error(data.response.msg, 'ERROR');
                            return;
                        }
                    });
                }
            } else {
                toastr.warning('Please fill up all the required fields', 'WARNING');
                return;
            }
        };

        $scope.deletePayrollSetup = function (row) {
            $scope.modal = {
                title: 'Application Settings',
                message: 'Are you sure to delete this Configuration?'
            };

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {
                appsettings.Delete_Application_Settings(row.UUID).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $state.reload();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            });
        };

        $scope.activatePayrollSetup = function (row) {
            $scope.modal = {
                title: 'Application Settings',
                message: 'Are you sure to activate this Configuration?'
            };

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {
                appsettings.Activate_Settings(row.UUID).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $state.reload();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            });
        };

        $scope.setPayrollSetup = function (row) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/tools/appsettings/appsettings.modal.html',
                controller: 'appSettingsModalCtrl',
                size: 'lg',
                resolve: {
                    details: function () {
                        return row;
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                if(selectedItem == 'save'){
                    $scope.refreshData();
                }
            }, function () {});
        }

        $scope.updatePayrollSetup = function (row) {
            $scope.setup = row;
            $scope.IsDisable = false;
            console.log('$scope.setup: ', $scope.setup);
        }
    }
})();