(function() {
    'use strict';

    angular.module('hris')
        .controller('overtimeListCtrl', overtimeListCtrl);

    overtimeListCtrl.$inject = ['$scope', 'attendance', 'localStorageService', '$uibModal', '$state', 'ngDialog', 'toastr', '$timeout','branch', 'department'];

    function overtimeListCtrl($scope, attendance, localStorageService, $uibModal, $state, ngDialog, toastr, $timeout, branch, department) {
        $scope.overtimes = [];
        $scope.overtimesCopy = [];
        $scope.branches = [];
        $scope.currentPayroll = {};
        $scope.action = {};

        $scope.refreshData = function() {
            $scope.overtimes = [];
            attendance.getAllOvertime($scope.currentPayroll.UUID).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var overtimes = data.response.result;
                    if (!_.isEmpty(overtimes)) {
                        $scope.overtimesCopy = angular.copy(overtimes);
                        $scope.overtimes = overtimes;
                        
                        if($scope.action.action == 'branch'){
                            $scope.overtimes = _.filter(overtimes,{'branch_id': $scope.action._id});
                        }else if($scope.action.action == 'department'){
                            $scope.overtimes = _.filter(overtimes,{'department_id': $scope.action._id});
                        }else{
                            $scope.overtimes = angular.copy($scope.overtimesCopy);
                        }                     
                        
                        _.each($scope.overtimes, function(row) {
                            if (row.details && row.details.length > 0) {
                                var result = _.reject(row.details, { 'ApprovedDate': null, 'IsApproved': 0 });
                                var regData = _.filter(result, { 'OTType': 1 });
                                var holData = _.filter(result, { 'OTType': 2 });
                                var splData = _.filter(result, { 'OTType': 3 });
                                var sunData = _.filter(result, { 'OTType': 4 });

                                row.TotalNoHrs = _.sumBy(row.details, function(o) { return o.NoHrs }) || 0;
                                row.TotalApprovedNoHrs = _.sumBy(result, function(o) { return o.NoHrs }) || 0;
                                row.TotalRegOT = _.sumBy(regData, function(o) { return o.NoHrs }) || 0;
                                row.TotalHolOT = _.sumBy(holData, function(o) { return o.NoHrs }) || 0;
                                row.TotalSplOT = _.sumBy(splData, function(o) { return o.NoHrs }) || 0;
                                row.TotalSunOT = _.sumBy(sunData, function(o) { return o.NoHrs }) || 0;
                            } else {
                                row.TotalApprovedNoHrs = 0;
                            }
                        });
                        console.log('$scope.overtimes: ', $scope.overtimes);
                    }
                }
            });
        };
        
        async.waterfall([
            function (callback) {
                branch.getAllBranch().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var branches = data.response.result;
                        if (!_.isEmpty(branches)) {
                            callback(null, branches);
                        }
                    }
                });
            },
            function (branches, callback) {
                department.getAllDepartments().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var departments = data.response.result;
                        if (!_.isEmpty(departments)) {
                            callback(null, branches, departments);
                        }
                    }
                });
            },
            function (branches, departments, callback) {
                _.each(departments, function (row) {
                    row.tree_name = row.d_name;
                    row.tree_id = row.d_id;
                    row.tree_type = 'department';
                });

                _.each(branches, function (row) {
                    row.tree_name = row.branch_name;
                    row.tree_id = row.branch_id;
                    row.tree_type = 'branch';
                    row.departments = _.filter(departments, {
                        'branch_id': row.branch_id
                    }) || [];
                });
                $scope.branches = branches;
            }
        ]);

        if (localStorageService.get('hris.payroll')) {
            $scope.currentPayroll = localStorageService.get('hris.payroll');
            $scope.refreshData();
        }
        
        $scope.$watch('mytree.currentNode', function (newObj, oldObj) {
            if ($scope.mytree && angular.isObject($scope.mytree.currentNode)) {
                $scope.action = {
                    action: $scope.mytree.currentNode.tree_type,
                    _id: $scope.mytree.currentNode.tree_id
                };
                $scope.refreshData();
            }
        }, false);


        $scope.payrollOptions = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/attendance/DTR/payrolloption.modal.html',
                controller: 'dtrPayrollOptionCtrl',
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.currentPayroll = selectedItem;
                    localStorageService.set('hris.payroll', selectedItem);

                    $scope.refreshData();
                }
            }, function() {});
        };

        $scope.viewEmployeeOvertime = function(EmployeeID, PayrollID) {
            var hashEmployeeID = CryptoJS.MD5(EmployeeID.toString());
            var hashPayrollID = CryptoJS.MD5(PayrollID.toString());
            $state.go('main.overtime_term', {
                emp_id: hashEmployeeID,
                payroll_id: hashPayrollID
            });
        };

        $scope.deleteOvertime = function(RefNo) {
            $scope.modal = {
                title: 'Employee Overtime',
                message: 'Are you sure to delete this overtime?'
            };

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                attendance.DeleteOvertime(RefNo).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function() {
                            $scope.refreshData();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            });
        };
    }

})();