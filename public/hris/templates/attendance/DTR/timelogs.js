(function () {
    'use strict';

    angular.module('hris')
        .controller('dtrEmployeeLogsCtrl', dtrEmployeeLogsCtrl)
        .controller('dtrRawLogModalCtrl', dtrRawLogModalCtrl);

    dtrEmployeeLogsCtrl.$inject = ['$scope', '$uibModalInstance', 'Payroll', '$timeout', 'attendance', 'Employee', 'localStorageService', 'toastr', 'ngTableParams', '$uibModal', 'ngDialog'];
    dtrRawLogModalCtrl.$inject = ['$scope', '$uibModalInstance', '$timeout', 'attendance', 'toastr', 'rawlogs'];

    function dtrEmployeeLogsCtrl($scope, $uibModalInstance, Payroll, $timeout, attendance, Employee, localStorageService, toastr, ngTableParams, $uibModal, ngDialog) {
        $scope.currentPayroll = {};
        $scope.rawLogs = [];

        if (localStorageService.get('hris.payroll')) {
            $scope.currentPayroll = localStorageService.get('hris.payroll');
        }

        if (Employee) {
            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 50, // count per page
            }, {
                getData: function ($defer, params) {
                    attendance.getEmployeeRawTimelogs(Employee.device_no, {
                        datefrom: $scope.currentPayroll.FirstDate,
                        dateto: $scope.currentPayroll.SecondDate
                    }).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            var result = data.response.result;
                            $scope.rawLogs = result;
                            _.each($scope.rawLogs,function(row){
                                row.TimeLog = new Date(row.TimeLog);
                            });

                            params.total(result.length);
                            $defer.resolve(result.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                        } else {
                            toastr.warning(data.response.msg, 'WARNING');
                            return;
                        }
                    });
                }
            });
        }

        $scope.myFunction = function () {
            angular.element(document).ready(function () {
                $('#myTable').each(function () {
                    var Column_number_to_Merge = 1;
                    var Previous_TD = null;
                    var i = 1;
                    $("tbody", this).find('tr').each(function () {
                        var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge + ')');
                        if (Previous_TD == null) {
                            Previous_TD = Current_td;
                            i = 1;
                        } else if ($(Current_td).scope().row.formattedTimeLog == $(Previous_TD).scope().row.formattedTimeLog) {
                            Current_td.remove();
                            Previous_TD.attr('rowspan', i + 1);
                            i = i + 1;
                        } else {
                            Previous_TD = Current_td;
                            i = 1;
                        }
                    });
                });
            });
        };

        $scope.refreshData = function(){
            $scope.tableParams.reload();
        };

        $scope.insertTimeLog = function () {
            var raw = {
                device_no: Employee.device_no,
                EmployeeID: Employee.e_idno
            };

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/attendance/DTR/timelog.entry.modal.html',
                controller: 'dtrRawLogModalCtrl',
                size: 'sm',
                resolve: {
                    rawlogs: function () {
                        return raw;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                if (selectedItem == 'save') {
                    $scope.tableParams.reload();
                }
            }, function () {});
        };

        $scope.updateTimeLog = function (raw) {
            raw.device_no = Employee.device_no;
            raw.EmployeeID = Employee.e_idno;

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/attendance/DTR/timelog.entry.modal.html',
                controller: 'dtrRawLogModalCtrl',
                size: 'sm',
                resolve: {
                    rawlogs: function () {
                        return raw;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                if (selectedItem == 'update') {
                    $scope.tableParams.reload();
                }
            }, function () {});
        };

        $scope.deleteRawTimeLog = function (row) {
            $scope.modal = {
                title: 'Employee Attendance',
                message: 'Are you sure to delete this time log?'
            };

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {
                attendance.deleteEmployeeRawTimelogs(row.UUID, Employee.device_no).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $scope.tableParams.reload();
                        }, 300);
                    } else {
                        toastr.warning(data.response.msg, 'WARNING');
                        return;
                    }
                });
            });
        };

        $scope.selectEntry = function () {
            $uibModalInstance.close('save');
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    function dtrRawLogModalCtrl($scope, $uibModalInstance, $timeout, attendance, toastr, rawlogs) {
        $scope.hstep = 1;
        $scope.mstep = 1
        $scope.ismeridian = true;

        $scope.logs = {};
        $scope.logs.LogDate = new Date();
        $scope.logs.LogType = 0;
        $scope.logs.device_no = rawlogs.device_no;
        $scope.logs.EmployeeID = rawlogs.EmployeeID;

        $scope.popup2 = {
            opened: false
        };

        if (rawlogs.TimeLog) {
            $scope.logs.LogDate = new Date(rawlogs.TimeLog);
            $scope.logs.TimeLog = new Date(rawlogs.TimeLog);
            $scope.logs.LogTime = new Date(rawlogs.TimeLog);
            $scope.logs.EntryID = rawlogs.EntryID;
            $scope.logs.UUID = rawlogs.UUID;
        }

        $scope.open2 = function(){
            $scope.popup2.opened = true;
        };

        $scope.saveEntry = function () {
            $scope.logs.TimeLog = $scope.logs.LogDate;
            $scope.logs.LogDate = moment($scope.logs.LogDate).format('YYYY-MM-DD');
            $scope.logs.LogTime = moment($scope.logs.TimeLog).format('HH:mm:ss A');
            $scope.logs.TimeLog = moment($scope.logs.TimeLog).format('YYYY-MM-DD HH:mm:ss');
            $scope.logs.MACID = '001';

            if (rawlogs.TimeLog) {
                attendance.updateEmployeeRawTimelogs($scope.logs.UUID, $scope.logs.device_no, $scope.logs).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $uibModalInstance.close('update');
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            } else {
                attendance.saveEmployeeRawTimelogs($scope.logs.device_no, $scope.logs).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();