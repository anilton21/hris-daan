(function () {
    'use strict';

    angular.module('hris')
        .controller('dailyTimeRecordCtrl', dailyTimeRecordCtrl);


    dailyTimeRecordCtrl.$inject = ['$scope', '$uibModal', 'toastr', 'localStorageService', 'attendance', '$state', '$stateParams', 'employee', '$timeout', 'workschedules', '$filter', 'ngDialog'];

    function dailyTimeRecordCtrl($scope, $uibModal, toastr, localStorageService, attendance, $state, $stateParams, employee, $timeout, workschedules, $filter, ngDialog) {
        $scope.currentPayroll = {};
        $scope.employee = {};
        $scope.attendanceStat = {};
        $scope.attendanceStat.totalHrs = 0;
        $scope.attendanceStat.totalMinLate = 0;
        $scope.attendanceStat.totalMinUndertime = 0;
        $scope.attendanceStat.totalHalfDay = 0;
        $scope.attendanceStat.totalAbsent = 0;
        $scope.attendanceStat.totalHolidays = 0;
        $scope.attendanceStat.totalNonWorking = 0;
        $scope.attendanceStat.totalDays = 0;

        $scope.attendance = [];
        $scope.overtimes = [];
        $scope.summary = [];

        $scope.dateArray = [];
        $scope.workDays = [];
        $scope.schedules = {};

        $scope.refNo = '';
        $scope.TotalNoHrs = 0;

        $scope.IsAttendancePosted = false;
        $scope.applyLogs = true;

        var Days = {
            Sunday: 1,
            Monday: 2,
            Tuesday: 3,
            Wednesday: 4,
            Thursday: 5,
            Friday: 6,
            Saturday: 7
        };

        var TimeType = {
            AM: 0,
            PM: 1
        };

        Date.prototype.addDays = function (days) {
            var dat = new Date(this.valueOf())
            dat.setDate(dat.getDate() + days);
            return dat;
        };

        function getDates(startDate, stopDate) {
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push(currentDate)
                currentDate = currentDate.addDays(1);
            }
            return dateArray;
        }

        if (localStorageService.get('hris.payroll')) {
            $scope.currentPayroll = localStorageService.get('hris.payroll');
        }

        workschedules.getAllDays().then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var workDays = data.response.result;
                if (!_.isEmpty(workDays)) {
                    $scope.workDays = workDays;
                }
            }
        });



        $scope.getEmployeeProfile = function (e_id) {
            employee.getEmployeeProfile(e_id, {}).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    var employeeD = data.response.result;
                    if (!_.isEmpty(employeeD)) {
                        $scope.employee = employeeD;
                        if ($scope.employee.Photo) {
                            $scope.profileImage = 'data:image/png;base64,' + $scope.employee.Photo;
                        }

                        var dateArray = getDates(new Date($scope.currentPayroll.FirstDate), new Date($scope.currentPayroll.SecondDate));
                        for (var i = 0; i < dateArray.length; i++) {
                            var date = new Date(dateArray[i]);
                            $scope.dateArray.push({
                                Days: dateArray[i],
                                weekDays: _.find($scope.workDays, {
                                    'IndexID': date.getDay() + 1
                                }).Days || ''
                            });
                        }

                        var hashTemplateID = CryptoJS.MD5($scope.employee.ScheduleID.toString());
                        workschedules.getWorkScheduleDetails(hashTemplateID).then(function (data) {
                            if (data.statusCode == 200 && data.response.success) {
                                var schedules = data.response.result;
                                if (!_.isEmpty(schedules)) {
                                    $scope.schedules = schedules;
                                }
                            }
                        });

                        async.waterfall([
                            function (callback) {
                                attendance.getEmployeeAttendance($scope.employee.Emp_UUID, $scope.currentPayroll.UUID).then(function (data) {
                                    if (data.statusCode == 200 && data.response.success) {
                                        var empattendance = data.response.result;
                                        if (!_.isEmpty(empattendance)) {
                                            $scope.applyLogs = false;

                                            $scope.attendance = empattendance;
                                            _.each($scope.attendance, function (row) {
                                                row.F_TransDate = new Date(row.TransDate);
                                                row.RegHrs = 8;
                                                row.IsAbsent = 0;
                                                row.Totalhrs = Math.floor((row.Totalhrs / 60));
                                                row.IsPosted = _.isEmpty(row.PostedDate) ? 0 : 1;
                                                // row.str_TransDate = $filter('date')(row.TransDate, 'yyyy-MM-dd');
                                                row.str_TransDate = moment(row.TransDate).format('YYYY-MM-DD');
                                            });
                                            console.log('$scope.attendance: ', $scope.attendance);
                                        } else {
                                            _.each($scope.dateArray, function (row) {
                                                var date = new Date(row.Days);
                                                $scope.attendance.push({
                                                    F_TransDate: row.Days,
                                                    DAYNAME: row.weekDays,
                                                    Day: date.getDay() + 1,
                                                    Year: date.getFullYear(),
                                                    MonthID: date.getMonth(),
                                                    TimeInActualDate: row.Days,
                                                    TimeOutActualDate: row.Days,
                                                    RegHrs: 8,
                                                    TotalHrs: 0,
                                                    IsPosted: 0,
                                                    IsAbsent: 0,
                                                    PayrollID: $scope.currentPayroll.PayrollID,
                                                    EmployeeID: $scope.employee.e_idno
                                                });
                                            });
                                        }

                                        attendance.isEmployeeAttendancePosted($scope.employee.Emp_UUID, $scope.currentPayroll.UUID).then(function (data) {
                                            if (data.statusCode == 200 && data.response.success) {
                                                $scope.IsAttendancePosted = true;
                                            } else if (data.statusCode == 200 && !data.response.success) {
                                                $scope.IsAttendancePosted = false;
                                            }
                                        });
                                        callback();
                                    }
                                });
                            },
                            function (callback) {
                                attendance.getEmployeeOvertime($scope.employee.Emp_UUID, $scope.currentPayroll.UUID).then(function (data) {
                                    if (data.statusCode == 200 && data.response.success) {
                                        var result = data.response.result;
                                        if (!_.isEmpty(result)) {
                                            $scope.overtimes = result.details;
                                            _.each($scope.overtimes, function (row) {
                                                // row.str_TransDate = $filter('date')(row.InclusiveDates, 'yyyy-MM-dd');
                                                row.str_TransDate = moment(row.TransDate).format('YYYY-MM-DD');
                                                if (row.TimeIn) {
                                                    row.TimeIn = new Date(row.TimeIn);
                                                }
                                                if (row.TimeOut) {
                                                    row.TimeOut = new Date(row.TimeOut);
                                                }
                                            });
                                            $scope.refNo = result.RefNo;
                                            $scope.TotalNoHrs = result.TotalNoHrs;
                                            console.log('$scope.overtimes: ', $scope.overtimes);
                                        }
                                        callback();
                                    }
                                });
                            },
                            function (callback) {
                                $scope.attendanceStat.totalHrs = _.sumBy($scope.attendance, function (o) {
                                    return o.Totalhrs;
                                }) || 0;
                                $scope.attendanceStat.totalMinLate = _.sumBy($scope.attendance, function (o) {
                                    return o.TotalLate;
                                }) + _.sumBy($scope.attendance, function (o) {
                                    return o.TotalLate2;
                                }) || 0;
                                $scope.attendanceStat.totalMinUndertime = _.sumBy($scope.attendance, function (o) {
                                    return o.TotalUnderTime;
                                }) + _.sumBy($scope.attendance, function (o) {
                                    return o.TotalUnderTime2;
                                }) || 0;
                                $scope.attendanceStat.totalHalfDay = _.filter($scope.attendance, {
                                    'IsHalfday': 1
                                }).length;
                                $scope.attendanceStat.totalAbsent = _.filter($scope.attendance, {
                                    'IsAbsent': 1
                                }).length;
                                $scope.attendanceStat.totalHolidays = _.filter($scope.attendance, {
                                    'isHoliday': 1
                                }).length;
                                $scope.attendanceStat.totalNonWorking = _.filter($scope.attendance, {
                                    'IsNonWorking': 1
                                }).length;
                                $scope.attendanceStat.totalDays = $scope.attendance.length;

                                console.log('$scope.overtimes:', $scope.overtimes);
                                _.each($scope.dateArray, function (row) {
                                    var data = {};
                                    data.Days = row.Days;
                                    data.weekDays = row.weekDays;
                                    data.TotalRegHrs = 0;
                                    data.TotalOTHrs = 0;
                                    data.TotalHrs = 0;
                                    var date1 = $filter('date')(row.Days, 'yyyy-MM-dd');
                                    var att = _.find($scope.attendance, {
                                        'str_TransDate': date1
                                    });
                                    if (att) {
                                        var date2 = $filter('date')(att.F_TransDate, 'yyyy-MM-dd');
                                        if (date1 == date2) {
                                            data.TimeIn1 = att.F_TimeIn;
                                            data.TimeOut1 = att.F_TimeOut;
                                            data.TimeIn2 = att.F_TimeIn2;
                                            data.TimeOut2 = att.F_TimeOut2;
                                            data.TotalRegHrs = att.Totalhrs;
                                            data.Remarks = att.Remarks;
                                            data.IsPosted = att.IsPosted;
                                        }
                                    }

                                    var over = _.find($scope.overtimes, {
                                        'InclusiveDates': date1
                                    });
                                    if (over) {
                                        data.overTimeIn = new Date(over.TimeIn);
                                        data.overTimeOut = new Date(over.TimeOut);
                                        data.OTType = over.OTType.OTTypeCode;
                                        data.TotalOTHrs = over.NoHrs;
                                        data.IsPosted = (!_.isEmpty(over.ApprovedDate)) ? 1 : 0;
                                        data.overTimeApprovedDate = (!_.isEmpty(over.ApprovedDate)) ? new Date(over.ApprovedDate) : '';
                                    }
                                    data.TotalHrs = data.TotalRegHrs + data.TotalOTHrs;
                                    $scope.summary.push(data);
                                });
                                _.each($scope.summary, function (row) {
                                    if (row.weekDays == 'Saturday') {
                                        row.IsNonWorking = 1;
                                    } else if (row.weekDays == 'Sunday') {
                                        row.IsNonWorking = 1;
                                    } else {
                                        row.IsNonWorking = 0;
                                    }
                                });
                                // console.log('$scope.summary: ', $scope.summary);

                            }
                        ])



                    }
                } else {
                    toastr.warning(data.response.msg +
                        "Please contact the HUMAN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                    $timeout(function () {
                        $state.go('main.dailytimerecord', {
                            emp_id: null
                        });
                    }, 300);
                    return;
                }
            });
        };


        if ($stateParams.emp_id) {
            $scope.getEmployeeProfile($stateParams.emp_id);
        }

        $scope.browseEmployee = function () {
            if (_.isEmpty($scope.currentPayroll)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected ACTIVE PAYROLL OPTION to get the employee ATTENDANCE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/pickemployee.modal.html',
                controller: 'employeePickerCtrl',
                size: 'lg'
            });

            modalInstance.result.then(function (selectedItem) {
                if (selectedItem) {
                    $state.go('main.dailytimerecord', {
                        emp_id: selectedItem.UUID
                    });
                }
            }, function () {});
        };

        $scope.clearEmployee = function () {
            $state.go('main.dailytimerecord', {
                emp_id: null
            });
        };

        $scope.refreshData = function () {
            $scope.attendance = [];
            $scope.attendance = [];
            $scope.overtimes = [];
            $scope.summary = [];

            $scope.dateArray = [];
            $scope.workDays = [];

            workschedules.getAllDays().then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    var workDays = data.response.result;
                    if (!_.isEmpty(workDays)) {
                        $scope.workDays = workDays;
                    }
                }
            });

            if ($stateParams.emp_id) {
                $scope.getEmployeeProfile($stateParams.emp_id);
            }
        };

        $scope.payrollOptions = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/attendance/DTR/payrolloption.modal.html',
                controller: 'dtrPayrollOptionCtrl',
                size: 'md'
            });

            modalInstance.result.then(function (selectedItem) {
                if (selectedItem) {
                    $scope.currentPayroll = selectedItem;
                    localStorageService.set('hris.payroll', selectedItem);

                    if ($stateParams.emp_id) {
                        // $scope.getEmployeeProfile($stateParams.emp_id);
                        $state.reload();
                    }
                }
            }, function () {});
        };

        $scope.attendanceSettings = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/attendance/DTR/attendance.settings.modal.html',
                controller: 'dtrAttendanceSettingsCtrl',
                size: 'md'
            });

            modalInstance.result.then(function (selectedItem) {}, function () {});
        };

        $scope.viewEmployeeWorkSchedule = function () {
            if (_.isEmpty($scope.employee)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected EMPLOYEE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            if (_.isEmpty($scope.currentPayroll)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected ACTIVE PAYROLL OPTION to get the employee ATTENDANCE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            if ($scope.employee) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: './public/hris/templates/attendance/DTR/dtr.worksched.modal.html',
                    controller: 'dtrEmployeeSchedCtrl',
                    size: 'lg',
                    resolve: {
                        TemplateID: function () {
                            return $scope.employee.ScheduleID;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {}, function () {});
            } else {
                toastr.warning('Please select Employee', 'Warning');
                return;
            }

        };

        $scope.viewEmployeeTimeLogs = function () {
            if (_.isEmpty($scope.employee)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected EMPLOYEE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            if (_.isEmpty($scope.currentPayroll)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected ACTIVE PAYROLL OPTION to get the employee ATTENDANCE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            if ($scope.employee) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: './public/hris/templates/attendance/DTR/timelogs.modal.html',
                    controller: 'dtrEmployeeLogsCtrl',
                    size: 'lg',
                    resolve: {
                        Employee: function () {
                            return $scope.employee;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {}, function () {});
            } else {
                toastr.warning('Please select Employee', 'Warning');
                return;
            }
        };

        $scope.applyRawLogs = function () {
            if (_.isEmpty($scope.employee)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected EMPLOYEE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            if (_.isEmpty($scope.currentPayroll)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected ACTIVE PAYROLL OPTION to get the employee ATTENDANCE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }


            attendance.getEmployeeRawTimelogs($scope.employee.device_no, {
                datefrom: $scope.currentPayroll.FirstDate,
                dateto: $scope.currentPayroll.SecondDate
            }).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    var timelogs = data.response.result;
                    var tempLogs = [];

                    _.each(timelogs, function (row) {
                        var dayofweek = moment(row.formattedTimeLog);
                        row.dayofweek = dayofweek.format('dddd');
                    });
                    var newlogs = _.uniq(_.map(timelogs, function (row) {
                        return row.formattedTimeLog
                    }));
                    _.each(newlogs, function (row) {
                        var result = _.filter(timelogs, {
                            'formattedTimeLog': row
                        });
                        if (result) {
                            var data = {};
                            data.date = row;
                            data.dayofweek = result.dayofweek;

                            if (result[0]) {
                                data.F_TimeIn = result[0].TimeLog;
                                data.LogTimeAM_IN = new Date(result[0].TimeLog);
                                data.dayofweek = result[0].dayofweek;
                                data.formattedLogTimeAM_IN = result[0].formattedLogTime;
                                data.formattedTimeLog = result[0].formattedTimeLog;
                                data.EntryID = result[0].EntryID;
                            }

                            if (result[1]) {
                                data.F_TimeOut = result[1].TimeLog;
                                data.LogTimeAM_OUT = new Date(result[1].TimeLog);
                                data.dayofweek = result[1].dayofweek;
                                data.formattedLogTimeAM_OUT = result[1].formattedLogTime;
                                data.formattedTimeLog = result[1].formattedTimeLog;
                                data.EntryID = result[1].EntryID;
                            }

                            if (result[2]) {
                                data.F_TimeIn2 = result[2].TimeLog;
                                data.LogTimePM_IN = new Date(result[2].TimeLog);
                                data.dayofweek = result[2].dayofweek;
                                data.formattedLogTimePM_IN = result[2].formattedLogTime;
                                data.formattedTimeLog = result[2].formattedTimeLog;
                                data.EntryID = result[2].EntryID;
                            }

                            if (result[3]) {
                                data.F_TimeOut2 = new Date(result[3].TimeLog);
                                data.LogTimePM_OUT = new Date(result[3].TimeLog);
                                data.dayofweek = result[3].dayofweek;
                                data.formattedLogTimePM_OUT = result[3].formattedLogTime;
                                data.formattedTimeLog = result[3].formattedTimeLog;
                                data.EntryID = result[3].EntryID;
                            }

                            /* var hoursAM = Math.floor(Math.abs(new Date(data.F_TimeIn) - new Date(data.F_TimeOut)) / 36e5);
                            console.log('hoursAM: ',hoursAM);
                            var hoursPM = Math.floor(Math.abs(new Date(data.F_TimeIn2) - new Date(data.F_TimeOut2)) / 36e5);
                            console.log('hoursPM: ',hoursPM); */

                            var startTime1 = new Date(data.F_TimeIn);
                            var endTime1 = new Date(data.F_TimeOut);
                            var diffMs1 = endTime1.getTime() - startTime1.getTime();
                            var hoursAM = (diffMs1 / 60000);

                            var startTime2 = new Date(data.F_TimeIn2);
                            var endTime2 = new Date(data.F_TimeOut2);
                            var diffMs2 = endTime2.getTime() - startTime2.getTime();
                            var hoursPM = (diffMs2 / 60000);

                            var totalHours = 0;
                            if (_.isNaN(hoursAM) && !_.isNaN(hoursPM)) {
                                totalHours = 0 + hoursPM
                            } else if (!_.isNaN(hoursAM) && _.isNaN(hoursPM)) {
                                totalHours = hoursAM + 0
                            } else {
                                totalHours = hoursAM + hoursPM
                            }
                            data.totalHours = totalHours;
                            tempLogs.push(data);
                        }
                    });

                    _.each($scope.attendance, function (row) {
                        var date = $filter('date')(row.F_TransDate, 'yyyy-MM-dd');
                        var item = _.find(tempLogs, {
                            'date': date
                        });
                        if (item) {
                            row.F_TimeIn = item.LogTimeAM_IN;
                            row.F_TimeOut = item.LogTimeAM_OUT;
                            row.F_TimeIn2 = item.LogTimePM_IN;
                            row.F_TimeOut2 = item.LogTimePM_OUT;
                            row.Totalhrs = Math.floor((item.totalHours / 60));
                            row.TotalHrs = item.totalHours;
                            row.TimeIn = item.F_TimeIn;
                            row.TotalLate = 0;
                            row.TimeOut = item.F_TimeOut;
                            row.TotalUnderTime = 0;
                            row.TimeIn2 = item.F_TimeIn2;
                            row.TotalLate2 = 0;
                            row.TimeOut2 = item.F_TimeOut2;
                            row.TotalUnderTime2 = 0;
                            row.Remarks = '';
                            row.isHoliday = 0;
                            row.isOnTravel = 0;
                            row.IsHalfDay = 0;
                            row.isAdjusted = 0;
                            row.IsLeave = 0;
                            row.IsLate = 0;
                            row.IsAbsent = 0;
                            row.IsNonWorking = 0;
                        } else {
                            var dayofweek = moment(row.F_TransDate);
                            row.dayofweek = dayofweek.format('dddd');

                            row.Totalhrs = 0;
                            row.TotalHrs = 0;
                            row.TotalLate = 0;
                            row.TotalUnderTime = 0;
                            row.TotalLate2 = 0;
                            row.TotalUnderTime2 = 0;
                            row.Remarks = '';
                            row.isHoliday = 0;
                            row.isOnTravel = 0;
                            row.IsHalfDay = 0;
                            row.isAdjusted = 0;
                            row.IsLeave = 0;
                            row.IsLate = 0;
                            row.IsAbsent = 0;
                            row.IsUnderTime = 0;

                            if (row.dayofweek == 'Saturday') {
                                row.IsNonWorking = 1;
                            } else if (row.dayofweek == 'Sunday') {
                                row.IsNonWorking = 1;
                            } else {
                                row.IsNonWorking = 0;
                            }
                        }
                    });

                }
            });
        };

        $scope.deleteTimeKeeping = function () {
            $scope.modal = {
                title: 'Employee Attendance',
                message: 'Are you sure to delete this attendance?'
            };

            if (_.isEmpty($scope.employee)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected EMPLOYEE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            if (_.isEmpty($scope.currentPayroll)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected ACTIVE PAYROLL OPTION to get the employee ATTENDANCE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {
                attendance.deleteEmployeeAttendance($scope.employee.Emp_UUID, $scope.currentPayroll.UUID).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $state.reload();
                        }, 300);
                    } else {
                        toastr.warning(data.response.msg, 'WARNING');
                        return;
                    }
                });
            });
        };

        $scope.postTimeKeeping = function () {
            $scope.modal = {
                title: 'Employee Attendance',
                message: 'Are you sure to post this attendance?'
            };

            if (_.isEmpty($scope.employee)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected EMPLOYEE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            if (_.isEmpty($scope.currentPayroll)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected ACTIVE PAYROLL OPTION to get the employee ATTENDANCE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {
                attendance.postEmployeeAttendance($scope.employee.Emp_UUID, $scope.currentPayroll.UUID).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $state.reload();
                        }, 300);
                    } else {
                        toastr.warning(data.response.msg, 'WARNING');
                        return;
                    }
                });
            });
        };

        $scope.saveTimeKeeping = function () {
            $scope.modal = {
                title: 'Employee Attendance',
                message: 'Save this attendance?'
            };

            if (_.isEmpty($scope.employee)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected EMPLOYEE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            if (_.isEmpty($scope.currentPayroll)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected ACTIVE PAYROLL OPTION to get the employee ATTENDANCE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {

                _.each($scope.attendance, function (row) {
                    var dayofweek = moment(row.F_TransDate);
                    var date = new Date(row.F_TransDate);

                    row.PayrollID = $scope.currentPayroll.PayrollID;
                    row.EmployeeID = $scope.employee.e_idno;
                    row.dayofweek = dayofweek.format('dddd');
                    row.Day = date.getDay() + 1;
                    row.Year = date.getFullYear();
                    row.MonthID = date.getMonth();
                    if (row.dayofweek == 'Saturday') {
                        row.IsNonWorking = 1;
                    } else
                    if (row.dayofweek == 'Sunday') {
                        row.IsNonWorking = 1;
                    } else {
                        row.IsNonWorking = 0;
                    }
                });

                if ($scope.IsAttendancePosted) {
                    toastr.warning('Employee Attendance already posted', 'WARNING');
                    return;
                } else {
                    async.eachSeries($scope.attendance, function (item, callback) {
                        attendance.saveEmployeeTimeKeeping($scope.employee.Emp_UUID, $scope.currentPayroll.UUID, item).then(function (data) {
                            if (data.statusCode == 200 && data.response.success) {
                                callback()
                            } else {
                                toastr.warning(data.response.msg, 'WARNING');
                                return;
                            }
                        });
                    }, function (resp) {
                        toastr.success('Record successfully saved', 'SUCCESS');
                        $timeout(function () {
                            $state.reload();
                        }, 300);
                    })
                }
            });
        };
    }

})();
