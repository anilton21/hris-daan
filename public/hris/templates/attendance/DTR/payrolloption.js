(function () {
    'use strict';

    angular.module('hris')
        .controller('dtrPayrollOptionCtrl', dtrPayrollOptionCtrl);

    dtrPayrollOptionCtrl.$inject = ['$scope', '$uibModalInstance', 'Payroll', '$timeout'];

    function dtrPayrollOptionCtrl($scope, $uibModalInstance, Payroll, $timeout) {
        $scope.payrollConfig = [];
        $scope.options = {};
        $scope.selected = {};

        $scope.popup1 = {
            opened: false
        };
        $scope.popup2 = {
            opened: false
        };
        $scope.dateOptions = {
            formatYear: 'yyyy'
        };

        Payroll.getPayrollConfig().then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var config = data.response.result;
                if (!_.isEmpty(config)) {
                    $scope.payrollConfig = config;
                }
            }
        });

        $scope.$watch('options.config', function (newObj, oldObj) {
            if (newObj) {
                $timeout(function () {
                    $scope.$apply(function () {
                        $scope.selected = $scope.options.config;
                        $scope.selected.F_FirstDate = new Date($scope.selected.FirstDate);
                        $scope.selected.F_SecondDate = new Date($scope.selected.SecondDate);
                    });
                }, 10);
            }
        });

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };


        $scope.selectEntry = function () {
            $uibModalInstance.close($scope.selected);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();
