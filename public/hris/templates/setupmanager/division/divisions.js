(function() {
    'use strict';

    angular.module('hris')
        .controller('divisionsCtrl', divisionsCtrl)
        .controller('divisionModalCtrl', divisionModalCtrl);

    divisionsCtrl.$inject = ['$scope', '$state', '$uibModal', 'division', 'ngTableParams', 'toastr', 'ngDialog', '$filter'];

    function divisionsCtrl($scope, $state, $uibModal, division, ngTableParams, toastr, ngDialog, $filter) {


        $scope.division = [];
        $scope.dataSearch = {
            text: ''
        };
        $scope.selectedItem = {};

        $scope.tblDivisions = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            filter: $scope.dataSearch,
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function($defer, params) {
                division.getAllDivisions({
                    action: 'picker',
                    limit: 1000
                }).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var divisions = data.response.result;

                        if (params.filter().text) {
                            $scope.divisions = $filter('filter')(divisions, params.filter().text);
                        } else {
                            $scope.divisions = divisions;
                        }

                        _.each($scope.divisions, function(row) {
                            row.selected = false;
                        });
                        console.log('$scope.divisions;', $scope.divisions)
                        params.total($scope.divisions.length);
                        $defer.resolve($scope.divisions.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        });

        $scope.Reload = function() {
            $scope.tblDivisions.reload();
        };
        $scope.searchDivisions = function() {
            $scope.tblDivisions.reload();
        }
        $scope.deleteDivision = function(id) {
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                division.deleteDivision(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success')
                        $scope.tblDivisions.reload();
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            });
        }

        $scope.updateDivision = function(id) {
            console.log('id:', id)
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/division/division.modal.html',
                controller: 'divisionModalCtrl',
                resolve: {
                    div_id: function() {
                        return id;
                    },

                },
                size: 'md'
            });
            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.tblDivisions.reload();
                }
            }, function() {});
        };

        $scope.addEntry = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/division/division.modal.html',
                controller: 'divisionModalCtrl',
                resolve: {
                    div_id: function() {
                        return null
                    }
                },
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.tblDivisions.reload();
                }
            }, function() {});
        };
    }

    divisionModalCtrl.$inject = ['$scope', '$uibModalInstance', 'division', 'toastr', 'div_id','$timeout'];


    function divisionModalCtrl($scope, $uibModalInstance, division, toastr, div_id,$timeout) {
        console.log('div_id:', div_id)
        $scope.division = {};
        if (div_id) {
            division.getDivisionsById(div_id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.division = data.response.result;
                    console.log('$scope.division:', $scope.division);
                }
            });
        }

        $scope.saveEntry = function() {
                if (div_id) {
                    division.updateDivision(div_id, $scope.division).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $timeout(function(){
                                $uibModalInstance.close('save');
                            },300);
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                } else {
                    division.saveEntry($scope.division).then(function(data) {
                        console.log('data:', data)
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $timeout(function(){
                                $uibModalInstance.close('save');
                            },300);
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                }
            },

            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };


    }

})();