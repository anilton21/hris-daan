(function() {
    'use strict';

    angular.module('hris')
        .controller('religionsCtrl', religionsCtrl)
        .controller('religionModalCtrl', religionModalCtrl);

    religionsCtrl.$inject = ['$scope', '$state', '$uibModal', 'religion', 'ngTableParams', 'ngDialog', 'toastr' , '$filter'];

    function religionsCtrl($scope, $state, $uibModal, religion, ngTableParams, ngDialog, toastr , $filter) {

        $scope.releg = {};
        $scope.dataSearch = {
            text: ''
        };
        $scope.selectedItem = {};

        $scope.tblReligions = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            filter: $scope.dataSearch,
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function($defer, params) {
                religion.getAllReligions({
                    action: 'picker',
                    limit: 1000
                }).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var releg = data.response.result;

                        if(params.filter().text) {
                            $scope.releg = $filter('filter')(releg, params.filter().text);
                        } else {
                            $scope.releg = releg;
                        }

                        _.each($scope.releg, function(row) {
                            row.selected = false;
                        });
                        console.log('$scope.releg;', $scope.releg)
                        params.total($scope.releg.length);
                        $defer.resolve($scope.releg.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        });

        $scope.Reload = function() {
            $scope.tblReligions.reload();
        };
        $scope.searchReligion = function(){
           $scope.tblReligions.reload();
        }

        $scope.deleteReligion = function(id) {
            console.log('id:', id)
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                religion.deleteReligion(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success')
                        $scope.tblReligions.reload();
                    }
                });
            });
        }

        $scope.updateReligion = function(id) {
            console.log('id:', id)


            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/religion/religion.modal.html',
                controller: 'religionModalCtrl',
                resolve: {
                    religion_id: function() {
                        return id;
                    },

                },
                size: 'md'
            });
            $scope.cancel = function() {
                modalInstance.dismiss('cancel');
            };
        }

        $scope.addEntry = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/religion/religion.modal.html',
                controller: 'religionModalCtrl',
                resolve: {

                    religion_id: function() {

                        return null

                    },
                },

                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {

            }, function() {});
        };
    }

    religionModalCtrl.$inject = ['$scope', '$uibModalInstance', 'religion', 'toastr', 'religion_id'];



    function religionModalCtrl($scope, $uibModalInstance, religion, toastr, religion_id) {

        console.log('religionModalCtrl')
        console.log('religion_id:', religion_id)
        $scope.religion = {};
        if (religion_id) {
            religion.getReligionById(religion_id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.religion = data.response.result;
                    console.log('$scope.religion:', $scope.religion);
                }
            });
        }
        $scope.saveEntry = function() {
            if (religion_id) {
                console.log('$scope.religion:', $scope.religion)
                religion.updateReligion(religion_id, $scope.religion).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $uibModalInstance.close('save');
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                } else {

                console.log('religion:', $scope.religion)
                religion.saveEntry($scope.religion).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $uibModalInstance.close('save');
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                }
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };


    }








})();