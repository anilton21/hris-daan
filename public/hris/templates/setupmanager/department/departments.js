(function() {
    'use strict';

    angular.module('hris')
        .controller('departmentsCtrl', departmentsCtrl)
        .controller('departmentModalCtrl', departmentModalCtrl);

    departmentsCtrl.$inject = ['$scope', '$state', '$uibModal', 'department', 'ngTableParams', 'ngDialog', 'toastr', '$filter'];

    function departmentsCtrl($scope, $state, $uibModal, department, ngTableParams, ngDialog, toastr, $filter) {

        console.log('departmentsCtrl')
        $scope.departments = [];
        $scope.dataSearch = {
            text: ''
        };
        $scope.selectedItem = {};

        $scope.tblDepartments = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            filter: $scope.dataSearch,
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function($defer, params) {
                department.getAllDepartments({
                    action: 'picker',
                    limit: 1000
                }).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var departments = data.response.result;

                        if (params.filter().text) {
                            $scope.departments = $filter('filter')(departments, params.filter().text);
                        } else {
                            $scope.departments = departments;
                        }

                        _.each($scope.departments, function(row) {
                            row.selected = false;
                        });

                        params.total($scope.departments.length);
                        $defer.resolve($scope.departments.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        });

        $scope.Reload = function() {
            $scope.tblDepartments.reload();
        };

        $scope.searchDepartments = function() {
            $scope.tblDepartments.reload();
        };

        $scope.deleteDepartment = function(id) {
            console.log('id:', id)
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                department.deleteDepartments(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success')
                        $scope.tblDepartments.reload();
                    }else{
                        toastr.error(data.response.msg,'ERROR');
                        return;
                    }
                });
            });
        };


        $scope.updateDeparment = function(id) {
            console.log('id:', id)
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/department/department.modal.html',
                controller: 'departmentModalCtrl',
                resolve: {
                    dep_id: function() {
                        return id;
                    }
                },
                size: 'lg'
            });
            modalInstance.result.then(function(selectedItem) {
                if(selectedItem){
                    $scope.tblDepartments.reload();
                }
            }, function() {});
        };

        $scope.addEntry = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/department/department.modal.html',
                controller: 'departmentModalCtrl',
                resolve: {
                    dep_id: function() {
                        return null
                    }
                },
                size: 'lg'
            });

            modalInstance.result.then(function(selectedItem) {
                if(selectedItem){
                    $scope.tblDepartments.reload();
                }
            }, function() {});
        };
    }


    departmentModalCtrl.$inject = ['$scope', '$uibModalInstance', 'department', 'toastr', 'dep_id', 'branch'];

    function departmentModalCtrl($scope, $uibModalInstance, department, toastr, dep_id, branch) {
        console.log('departmentModalCtrl')

        $scope.department = {};

        if (dep_id) {
            department.getDepartmentsById(dep_id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var department= data.response.result;
                    if(!_.isEmpty(department)){
                        $scope.department = department;
                    }
                }
            });
        }

        branch.getAllBranch().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var branches = data.response.result;
                if (!_.isEmpty(branches)) {
                    $scope.branches = branches;
                }
            }
        });


        $scope.saveEntry = function() {
            if (dep_id) {
                department.updateDeparments(dep_id, $scope.department).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {
                department.saveEntry($scope.department).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }


})();