'use strict';

angular.module('hris')
    .controller('phicCtrl', phicCtrl)
    .controller('phicDetailCtrl', phicDetailCtrl)
    .controller('phicModalCtrl', phicModalCtrl);

phicCtrl.$inject = ['$scope', '$state', 'toastr', 'auth', '$timeout', 'phic', 'ngTableParams', 'ngDialog', '$uibModal', '$filter'];

function phicCtrl($scope, $state, toastr, auth, $timeout, phic, ngTableParams, ngDialog, $uibModal, $filter) {
    $scope.phicdata = [];
    $scope.phic = {};
    $scope.IsDisable = true;
    $scope.dataSearch = {
        text: ''
    };
    $scope.selectedItem = {};
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        filter: $scope.dataSearch,
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            phic.getAllPHIC({
                action: 'picker',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var phicdata = data.response.result;

                    if (params.filter().text) {
                        $scope.phicdata = $filter('filter')(phicdata, params.filter().text);
                    } else {
                        $scope.phicdata = phicdata;
                    }

                    _.each($scope.phicdata, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.phicdata;', $scope.phicdata)
                    params.total($scope.phicdata.length);
                    $defer.resolve($scope.phicdata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });


    $scope.viewPhic = function(id) {
        $state.go('main.phic_detail', {
            phic_id: id
        });
    };


    $scope.deletePHTable = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            phic.DelPHTable(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParams.reload();

                }
            });
        });
    }

    $scope.updatePhic = function(id) {
        $scope.IsDisable = false;
        console.log('id:', id)
        if (id) {
            phic.GetPHDetailsByID(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.phic = data.response.result;
                    console.log('$scope.phic:', $scope.phic);
                    $scope.tableParams.reload();
                }
            });
        }

    }

    $scope.saveEntry = function() {
        if ($scope.phic)
            if ($scope.phic.PHID) {
                console.log('$scope.phic:', $scope.phic)
                phic.UpdatePHTable($scope.phic.UUID, $scope.phic).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {

                console.log('phic:', $scope.phic)
                phic.saveEntry($scope.phic).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };




    $scope.searchPhilHealth = function() {
        $scope.tableParams.reload();

    };

    $scope.addEntry = function() {
        $scope.phic = {};
        $scope.IsDisable = false;

    };

}

phicDetailCtrl.$inject = ['$scope', '$state', 'toastr', 'auth', '$timeout', '$uibModal', '$stateParams', 'phic', 'ngTableParams', 'ngDialog', '$filter' , 'company'];

function phicDetailCtrl($scope, $state, toastr, auth, $timeout, $uibModal, $stateParams, phic, ngTableParams, ngDialog, $filter, company) {


    $scope.deletePhilBracket = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            phic.DelPHDetails(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tablePhicDetail.reload();

                }
            });
        });
    }


    $scope.addPhicBracket = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './public/hris/templates/accounting/phic/phic.detail.moda.html',
            controller: 'phicModalCtrl',
            resolve: {
                phic_id: function() {
                    return $scope.ph.PHID + (1000)
                }

            },
            size: 'md'
        });

        modalInstance.result.then(function(selectedItem) {
            $scope.refreshData();
        }, function() {});
    };


    $scope.updatePhilBracket = function(id) {
        console.log('id:', id)
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './public/hris/templates/accounting/phic/phic.detail.moda.html',
            controller: 'phicModalCtrl',
            resolve: {
                phic_id: function() {
                    return id;
                },

            },
            size: 'md'
        });
        $scope.cancel = function() {
            modalInstance.dismiss('cancel');
        };


    }


    if ($stateParams.phic_id) {
        phic.GetPHDetailsByID($stateParams.phic_id).then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var ph = data.response.result;
                console.log('ph: ', ph);
                if (!_.isEmpty(ph)) {
                    $scope.ph = ph;
                }
            }
        });
    }

    

    $scope.Refresh = function() {
        $scope.tablePhicDetail.reload();
        $scope.company = {};
        company.getCompanyInfo().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var company = data.response.result;
                if (!_.isEmpty(company)) {
                    $scope.company = company;
                    if ($scope.company.ci_logo) {
                        $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;

                    }
                }
            }
        });

    }



    $scope.phicdetail = [];
    $scope.IsDisable = true;
    $scope.dataSearch = {
        text : ''
    }
    $scope.selectedItem = {};
    $scope.tablePhicDetail = new ngTableParams({
        page: 1, // show first page
        count: 100, // count per page
        filter : $scope.dataSearch,
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            phic.getPhilHealthDetails($stateParams.phic_id, {
                action: 'PH',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var phicdetail = data.response.result;

                   if(params.filter().text) {
                        $scope.phicdetail = $filter('filter')(phicdetail, params.filter().text) 
                    } else {
                        $scope.phicdetail = phicdetail;
                    }

                    _.each($scope.phicdetail, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.phicdetail;', $scope.phicdetail)
                    params.total($scope.phicdetail.length);
                    $defer.resolve($scope.phicdetail.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    company.getCompanyInfo().then(function(data) {
        if (data.statusCode == 200 && data.response.success) {
            var company = data.response.result;
            if (!_.isEmpty(company)) {
                $scope.company = company;
                if ($scope.company.ci_logo) {
                    $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;

                }
            }
        }
    });


    $scope.refreshData = function() {
        $scope.tablePhicDetail.reload();

    };


    $scope.searchPhilBrackets = function() {
        $scope.tablePhicDetail.reload();

    };

    $scope.printPhicBrackets = function() {
        $state.go('main.phicbracket_print', {
            phic_id: $scope.ph.UUID
        });
    };


}


phicModalCtrl.$inject = ['$scope', '$uibModalInstance', '$uibModal', 'phic_id', 'toastr', 'phic'];

function phicModalCtrl($scope, $uibModalInstance, $uibModal, phic_id, toastr, phic) {

    console.log('phicModalCtrl')
    console.log('phic_id:', phic_id)
    $scope.phics = {};
    if (phic_id) {
        phic.GetPHDetailsByPHICID(phic_id).then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                $scope.phics = data.response.result;
                if ($scope.phics.length == 0) {
                    $scope.phics = {};
                }
                console.log('$scope.phics:', $scope.phics);

            }
        });
    }

    $scope.savePhilDetails = function() {
        if ($scope.phics)
            if ($scope.phics.IndexID) {
                console.log('$scope.phics:', $scope.phics)
                phic.UpdatePHDetails($scope.phics.IndexID, $scope.phics).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {

                console.log('phics:', $scope.phics)
                console.log('phic_id', phic_id)
                phic.saveBracket(phic_id, $scope.phics).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };


    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };





}