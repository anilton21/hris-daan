'use strict';

angular.module('hris')
    .controller('hdmfCtrl', hdmfCtrl)
    .controller('hdmfModalCtrl', hdmfModalCtrl)
    .controller('hdmfPrintCtrl', hdmfPrintCtrl);

hdmfCtrl.$inject = ['$scope', '$state', 'toastr', 'auth', '$timeout', '$uibModal', 'hdmf', 'ngTableParams', 'ngDialog', '$filter', ];

function hdmfCtrl($scope, $state, toastr, auth, $timeout, $uibModal, hdmf, ngTableParams, ngDialog, $filter) {
    $scope.hdmfdata = [];
    $scope.IsDisable = true;
    $scope.action = {};
    $scope.dataSearch = {
        text: ''
    };
    $scope.selectedItem = {};
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        filter: $scope.dataSearch,
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            hdmf.getAllPagIBIG({
                action: 'picker',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var hdmfdata = data.response.result;

                    if (params.filter().text) {
                        $scope.hdmfdata = $filter('filter')(hdmfdata, params.filter().text);
                    } else {
                        $scope.hdmfdata = hdmfdata;
                    }

                    _.each($scope.hdmfdata, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.hdmfdata;', $scope.hdmfdata)
                    params.total($scope.hdmfdata.length);
                    $defer.resolve($scope.hdmfdata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    $scope.updateHdmf = function(id) {
        console.log('id:', id)
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './public/hris/templates/accounting/hdmf/hdmf.modal.html',
            controller: 'hdmfModalCtrl',
            resolve: {
                hdmf_id: function() {
                    return id;
                },
            },
            size: 'md'
        });
        $scope.cancel = function() {
            modalInstance.dismiss('cancel');
        };


    }
    $scope.deleteHdmf = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            console.log('DELETE:')
            hdmf.DeleteHDMF(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParams.reload();

                }
            });
        });
    }

    $scope.addHdmf = function() {
        console.log('hello error')
        $scope.branch = {};
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './public/hris/templates/accounting/hdmf/hdmf.modal.html',
            controller: 'hdmfModalCtrl',
            resolve: {
                hdmf_id: function() {
                    return null
                }
            },
            size: 'md'
        });

        modalInstance.result.then(function(selectedItem) {}, function() {});
    };
    $scope.refreshData = function() {
        $scope.tableParams.reload();
    };

    $scope.searchPagibig = function() {
        $scope.tableParams.reload();
    };

    $scope.printHDMF = function() {
        $state.go('main.hdmf_print', { action: $scope.action.action, _id: $scope.action._id });
    };


}


hdmfModalCtrl.$inject = ['$scope', '$uibModalInstance', 'hdmf_id', 'hdmf', 'toastr'];

function hdmfModalCtrl($scope, $uibModalInstance, hdmf_id, hdmf, toastr) {
    $scope.hdmf = {};
    if (hdmf_id) {
        hdmf.GetHDMF_ById(hdmf_id).then(function(data) {
            console.log('data:', data);
            if (data.statusCode == 200 && data.response.success) {
                $scope.hdmf = data.response.result;
                console.log('$scope.hdmf:', $scope.hdmf);
            }
        });
    }

    $scope.saveEntry = function() {
        if (hdmf_id) {

            console.log('$scope.hdmf:', $scope.hdmf)
            hdmf.Update_HDMF(hdmf_id, $scope.hdmf).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $uibModalInstance.close('save');
                } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else {
                    toastr.error(data.msg, 'Error');
                }
            });
        } else {
            console.log('hdmf:', $scope.hdmf)
            hdmf.saveEntry($scope.hdmf).then(function(data) {
                console.log('data:', data)
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $uibModalInstance.close('save');
                } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else {
                    toastr.error(data.msg, 'Error');
                }
            });
        }
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };


}

hdmfPrintCtrl.$inject = ['$scope', '$state', 'auth', '$filter', 'hdmf', '$uibModal', 'ngDialog', 'company', '$stateParams'];


function hdmfPrintCtrl($scope, $state, auth, $filter, hdmf, $uibModal, ngDialog, company, $stateParams)

{

    $scope.company = {};
    /* $scope.todalData = 10;*/
    async.waterfall([
        function(callback) {
            company.getCompanyInfo().then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var company = data.response.result;
                    if (!_.isEmpty(company)) {
                        $scope.company = company;
                        if ($scope.company.ci_logo) {
                            $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;
                        }
                        callback();
                    }
                }
            });
        },
        function(callback) {
            $scope.action = {
                action: $stateParams.action,
                _id: $stateParams._id
            };
            hdmf.getAllPagIBIG($scope.action).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var pagibig = data.response.result;
                    console.log('pagibig:', pagibig)
                    if (!_.isEmpty(pagibig)) {
                        $scope.pagibig = pagibig;
                    }
                }
            });
        }
    ]);


    $scope.Refresh = function() {
        $scope.pagibig = [];
        $scope.company = {};

        async.waterfall([
            function(callback) {
                company.getCompanyInfo().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var company = data.response.result;
                        if (!_.isEmpty(company)) {
                            $scope.company = company;
                            if ($scope.company.ci_logo) {
                                $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;
                            }
                            callback();
                        }
                    }
                });
            },
            function(callback) {
                $scope.action = {
                    action: $stateParams.action,
                    _id: $stateParams._id
                };
                hdmf.getAllPagIBIG($scope.action).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var pagibig = data.response.result;
                        console.log('pagibig:', pagibig)
                        if (!_.isEmpty(pagibig)) {
                            $scope.pagibig = pagibig;
                        }
                    }
                });
            }
        ]);



    }


}