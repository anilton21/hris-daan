'use strict';

angular.module('hris')
    .controller('accountsPickerCtrl', accountsPickerCtrl);

accountsPickerCtrl.$inject = ['$scope', 'accounts', '$uibModalInstance', 'ngTableParams', '$timeout', '$filter'];

function accountsPickerCtrl($scope, accounts, $uibModalInstance, ngTableParams, $timeout, $filter) {
    $scope.accounts = [];
    $scope.txtFind = {
        text: ''
    };
    $scope.selectedItem = {};

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        filter: $scope.txtFind,
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function ($defer, params) {
            $timeout(function () {
                accounts.getAllAccounts().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var accountsD = data.response.result;
                        
                        if (params.filter().text) {
                            $scope.accounts = $filter('filter')(accountsD, params.filter().text);
                        } else {
                            $scope.accounts = accountsD;
                        }

                        _.each($scope.accounts, function (row) {
                            row.selected = false;
                        });

                        params.total($scope.accounts.length);
                        $defer.resolve($scope.accounts.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                })
            }, 300);
        }
    });

    $scope.searchData = function () {
        $scope.tableParams.reload();
    };

    $scope.setSelectedItem = function (item) {
        if (item.selected) {
            $scope.selectedItem = {};
            item.selected = false;
        } else {
            _.each($scope.employees, function (row) {
                row.selected = false;
            });
            $scope.selectedItem = item;
            item.selected = true;
        }
    };

    $scope.selectEntry = function () {
        $uibModalInstance.close($scope.selectedItem);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}