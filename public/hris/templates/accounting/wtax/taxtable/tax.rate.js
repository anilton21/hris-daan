'use strict';

angular.module('hris')
    .controller('taxRateCtrl', taxRateCtrl);

taxRateCtrl.$inject = ['$scope', '$uibModalInstance', 'wtax', 'toastr', 'tax_id'];

function taxRateCtrl($scope, $uibModalInstance, wtax, toastr, tax_id) {
    if (tax_id) {
        wtax.getTaxDueByID(tax_id).then(function(data) {
            console.log('wtax:', wtax)
            console.log('tax_id:', tax_id)
            if (data.statusCode == 200 && data.response.success) {
                $scope.taxationrate = data.response.result;
                if ($scope.taxationrate.length == 0) {
                    $scope.taxationrate = {};
                }
                console.log('data:', data);
                console.log('$scope.taxationrate:', $scope.taxationrate);
            }
        });
    }


    $scope.saveTaxDue = function() {
        console.log('wtax:', wtax)
        console.log('tax_id:', tax_id)

        if ($scope.taxationrate)
            if ($scope.taxationrate.IndexID) {
                console.log('UPDATE1')
                console.log('$scope.taxationrate:', $scope.taxationrate)
                wtax.updateTaxDue($scope.taxationrate.IndexID, $scope.taxationrate).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {

                console.log('SAVE')
                console.log('taxationrate:', $scope.taxationrate)
                console.log('tax_id:', tax_id)
                wtax.saveTaxDue(tax_id, $scope.taxationrate).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };


    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
};