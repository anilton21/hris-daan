(function () {
    'use strict';

    angular.module('hris')
        .factory('employmentstatus', ['Restangular', function (Restangular) {
            return {

                updateEmploymentStatus: function (id, data) {
                    return Restangular.all('employmentstatus/' + id).customPUT(data).then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },

                deleteEmploymentStatus: function (id) {
                    return Restangular.all('employmentstatus/' + id).customDELETE().then(function (res) {
                        return res;
                    }, function (err) {
                        return err.data;
                    });
                },

                getEmploymentStatusById: function (id) {
                    return Restangular.all('employmentstatus').customGET(id).then(function (res) {
                        return res;
                    }, function (err) {
                        return err.data;

                    });
                },

                saveEntry: function (data) {
                    return Restangular.all('employmentstatus').customPOST(data).then(function (res) {
                        return res;
                    }, function (err) {
                        return err.data;
                    });
                },
                getAllEmploymentStatus: function () {
                    return Restangular.all('employmentstatus').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                }
            };
        }]);
})();