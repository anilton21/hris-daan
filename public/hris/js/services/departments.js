(function() {
    'use strict';

    angular.module('hris')
        .factory('department', ['Restangular', function(Restangular) {
            return {

                saveEntry: function(data) {
                    return Restangular.all('departments').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },
                deleteDepartments: function(id) {
                    return Restangular.all('departments/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                updateDeparments: function(id, data) {
                    return Restangular.all('departments/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                getDepartmentsById: function(id) {
                    return Restangular.all('departments').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },
                getAllDepartments: function() {
                    return Restangular.all('departments').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }]);

})();