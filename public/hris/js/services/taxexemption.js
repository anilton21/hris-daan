(function() {
    'use strict';

    angular.module('hris')
        .factory('taxexemption', ['Restangular', function(Restangular) {
            return {

                saveEntry: function(data) {
                    return Restangular.all('taxexemption').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                Update_Tax_Exemption: function(id, data) {
                    return Restangular.all('taxexemption/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                GetExemptionByID: function(id) {
                    return Restangular.all('taxexemption').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                Delete_Tax_Exemption: function(id) {
                    return Restangular.all('taxexemption/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                getAllTaxExemptions: function() {
                    return Restangular.all('taxexemption').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }]);

})();