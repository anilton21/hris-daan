	(function() {
	    'use strict';

	    angular.module('hris')
	        .factory('hdmf', ['Restangular', function(Restangular) {
	            return {


	            Update_HDMF: function(id, data) {
                    return Restangular.all('hdmf/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },    GetHDMF_ById: function(id) {
	                    return Restangular.all('hdmf').customGET(id).then(function(res) {
	                        return res;
	                    }, function(err) {
	                        return err.data;

	                    });
	                },
	                saveEntry: function(data) {
	                    return Restangular.all('hdmf').customPOST(data).then(function(res) {
	                        return res;
	                    }, function(err) {
	                        return err.data;
	                    });
	                },

	                DeleteHDMF: function(id) {
	                    return Restangular.all('hdmf/' + id).customDELETE().then(function(res) {
	                        return res;
	                    }, function(err) {
	                        return err.data;
	                    });
	                },

	                getAllPagIBIG: function(params) {
	                    return Restangular.all('hdmf').customGET("", params).then(function(res) {
	                            return res;
	                        },
	                        function(err) {
	                            return err.data;


	                        });
	                }
	            };
	        }]);
	})();