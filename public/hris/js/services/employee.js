(function() {
    'use strict';

    angular.module('hris')
        .factory('employee', ['Restangular', function(Restangular) {
            return {
                getEmployeeMasterlist: function(params) {
                    return Restangular.all('employees').customGET("", params).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                getEmployeeProfile: function(e_id, params) {
                    return Restangular.all('employees').customGET(e_id, params).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                updateEmployee: function(e_id, data) {
                    return Restangular.all('employees/' + e_id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                correctionEmployee: function(e_id, data) {
                    return Restangular.all('employees/' + e_id + '/correction').customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                updateEmployeeFamily: function(e_id, data) {
                    return Restangular.all('employees/' + e_id + '/family').customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                updateEmployeeEducation: function(e_id, data) {
                    return Restangular.all('employees/' + e_id + '/education').customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                addEmployeeChildren: function(e_id, data) {
                    return Restangular.all('employees/' + e_id + '/children').customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                deleteEmployeeChildren: function(e_id, _id) {
                    return Restangular.all('employees/' + e_id + '/children/' + _id).customDELETE().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                TagEmployeeWorkSchedule: function(e_id, sched_id) {
                    return Restangular.all('employees/' + e_id + '/schedule/' + sched_id).customPUT().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                UnTagEmployeeWorkSchedule: function(e_id, sched_id) {
                    return Restangular.all('employees/' + e_id + '/schedule/' + sched_id).customDELETE().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                Add_Employee: function(data) {
                    return Restangular.all('employees').customPOST(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                Update_Employee_ID: function(e_id, data) {
                    return Restangular.all('employees/' + e_id + '/ID').customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                Upload_Employee_Photo: function(e_id, data) {
                    return Restangular.all('employees/' + e_id + '/image').customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                Remove_Employee_Photo: function(e_id) {
                    return Restangular.all('employees/' + e_id + '/image').customDELETE().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                Get_Employee_Fingerprint: function(e_id) {
                    return Restangular.all('employees/' + e_id + '/fingerprint').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }]);

})();