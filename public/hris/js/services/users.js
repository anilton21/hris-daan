(function () {
    'use strict';

    angular.module('hris')
        .factory('users', users);

    users.$inject = ['Restangular'];

    function users(Restangular) {
        return {
            GetAllUsers: function () {
                return Restangular.all('useraccounts').customGET().then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            Add_User: function (data) {
                return Restangular.all('useraccounts').customPOST(data).then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            GetUserByUserID: function (UserID) {
                return Restangular.all('useraccounts/' + UserID).customGET().then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            Delete_User: function (UserID) {
                return Restangular.all('useraccounts/' + UserID).customDELETE().then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            Update_User: function (UserID, data) {
                return Restangular.all('useraccounts/' + UserID).customPUT(data).then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            setUserPrivilege: function (UserID, data) {
                return Restangular.all('useraccounts/' + UserID + '/access').customPUT(data).then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            getUserPrivilege: function (UserID, data) {
                return Restangular.all('useraccounts/' + UserID + '/access').customGET().then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            setUserGroup: function(UserID,data){
                return Restangular.all('useraccounts/' + UserID + '/group').customPUT(data).then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            GetAllUserGroups: function () {
                return Restangular.all('usergroups').customGET().then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            Add_User_Group: function (data) {
                return Restangular.all('usergroups').customPOST(data).then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            GetUserGroupByID: function (GroupID) {
                return Restangular.all('usergroups/' + GroupID).customGET().then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            Update_User_Group: function (GroupID, data) {
                return Restangular.all('usergroups/' + GroupID).customPUT(data).then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            Delete_User_Group: function (GroupID) {
                return Restangular.all('usergroups/' + GroupID).customDELETE().then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            setGroupPrivilege: function (GroupID, data) {
                return Restangular.all('usergroups/' + GroupID + '/access').customPUT(data).then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            getGroupPrivilege: function (GroupID, data) {
                return Restangular.all('usergroups/' + GroupID + '/access').customGET().then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            getAccessPrivileges: function(){
                return Restangular.all('privileges').customGET().then(function (res) {
                    return res;
                },
                function (err) {
                    return err.data;
                });
            }
        }
    }

})();