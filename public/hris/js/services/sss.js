(function() {
    'use strict';

    angular.module('hris')
        .factory('sss', ['Restangular', function(Restangular) {
            return {

                getSSSAllDetails: function(id,obj) {
                    console.log('obj ',obj);
                    return Restangular.all('sss/'+id).customGET("",obj).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                saveBracket: function(id , data) {
                    return Restangular.all('sss/' + id + '/bracket').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },  

                getSSSBracketData: function(id,obj) {
                    console.log('obj ',obj);
                    return Restangular.all('sss/'+id).customGET("",obj).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },  

                UpdateSSSBracket: function(id, data) {
                    return Restangular.all('sss/' + id + '/bracket/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                GetSSSBracketByID: function(id) {
                    return Restangular.all('sss').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                saveEntry: function(data) {
                    return Restangular.all('sss').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                getSSTableById: function(id) {
                    return Restangular.all('sss').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                getSSSByID: function(id) {
                    return Restangular.all('sss').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                GetSSSBracketByID: function(id) {
                    return Restangular.all('sss/' + id + '/bracket/' + id).customGET().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                UpdateSSSTable: function(id, data) {
                    return Restangular.all('sss/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                DelSSSTable: function(id) {
                    return Restangular.all('sss/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                DelSSSTable: function(id) {
                    return Restangular.all('sss/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                DelSSSBracket: function(id) {
                    return Restangular.all('sss/' + id + '/bracket/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },
        
                getAllSSS: function(params) {
                    return Restangular.all('sss').customGET("", params).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;


                        });
                }
            };
        }]);

})();