(function() {
    'use strict';

    angular.module('hris')
        .factory('phic', ['Restangular', function(Restangular) {
            return {

                getPhilHealthDetails: function(id,obj) {
                    console.log('obj ',obj);
                    return Restangular.all('phic/'+id).customGET("",obj).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },  

                GetPHDetailsByID: function(id) {
                    return Restangular.all('phic').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                GetPHDetailsByPHICID: function(id) {
                    return Restangular.all('phic/' + id + '/bracket').customGET().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                saveEntry: function(data) {
                    return Restangular.all('phic').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                saveBracket: function(id , data) {
                    return Restangular.all('phic/' + id + '/bracket').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                UpdatePHTable: function(id, data) {
                    return Restangular.all('phic/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                UpdatePHDetails: function(id, data) {
                    return Restangular.all('phic/' + id + '/bracket/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                DelPHTable: function(id) {
                    return Restangular.all('phic/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                DelPHDetails: function(id) {
                    return Restangular.all('phic/' + id + '/bracket/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                getAllPHIC: function(params) {
                    return Restangular.all('phic').customGET("", params).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;


                        });
                }
            };
        }]);

})();