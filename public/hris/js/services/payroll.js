(function () {
    'use strict';

    angular.module('hris')
        .factory('Payroll', ['Restangular', function (Restangular) {
            return {
                getAllMonths: function () {
                    return Restangular.all('months').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                getAllPayTypes: function () {
                    return Restangular.all('paytypes').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                updatePayTypes: function (tp_id, data) {
                    return Restangular.all('paytypes/' + tp_id).customPUT(data).then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                getAllPaymentTypes: function () {
                    return Restangular.all('paymenttypes').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                getPayrollConfig: function () {
                    return Restangular.all('payroll/config').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                createPayrollConfig: function (data) {
                    return Restangular.all('payroll/config').customPOST(data).then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                updatePayrollConfig: function (PayrollID, data) {
                    return Restangular.all('payroll/config/' + PayrollID).customPUT(data).then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                deletePayrollConfig: function (PayrollID) {
                    return Restangular.all('payroll/config/' + PayrollID).customDELETE().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                }
            };
        }]);

})();