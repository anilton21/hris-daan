(function() {
    'use strict';

    angular.module('hris')
        .factory('accounts', ['Restangular', function(Restangular) {
            return {

                getAccountType: function(id) {
                    return Restangular.all('accounttypes').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                Update_Account_Types: function(id, data) {
                    return Restangular.all('accounttypes/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                Delete_Account_Types: function(id) {
                    return Restangular.all('accounttypes/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },


                Delete_Account_Class: function(id) {
                    return Restangular.all('accountclass/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                Update_Account_Class: function(id, data) {
                    return Restangular.all('accountclass/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                getAccountClass: function(id) {
                    return Restangular.all('accountclass').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                getAccountGroup: function(id) {
                    return Restangular.all('accountgroups').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                Update_Account_Groups: function(id, data) {
                    return Restangular.all('accountgroups/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                Update_Accounts: function(id, data) {
                    return Restangular.all('accounts/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                Delete_Account_Groups: function(id) {
                    return Restangular.all('accountgroups/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                Delete_Accounts: function(id) {
                    return Restangular.all('accounts/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                GetAccountByID: function(id) {
                    return Restangular.all('accounts').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                saveEntry: function(data) {
                    return Restangular.all('accounts').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                saveEntry2: function(data) {
                    return Restangular.all('accountgroups').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                saveEntry3: function(data) {
                    return Restangular.all('accountclass').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                saveEntry4: function(data) {
                    return Restangular.all('accounttypes').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                Delete_Accounts: function(id) {
                    return Restangular.all('accounts/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                getAllAccountClass: function() {
                    return Restangular.all('accountclass').customGET().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                getAllAccountGroups: function() {
                    return Restangular.all('accountgroups').customGET().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },


                getAllAccountTypes: function() {
                    return Restangular.all('accounttypes').customGET().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                getAllAccounts: function(params) {
                    return Restangular.all('accounts').customGET("", params).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;


                        });
                }
            };
        }]);

})();