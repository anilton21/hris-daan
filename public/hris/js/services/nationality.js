(function() {
    'use strict';

    angular.module('hris')
        .factory('nationality', ['Restangular', function(Restangular) {
            return {

                deleteNationality: function(id) {
                    return Restangular.all('nationality/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                updateNationality: function(id, data) {
                    return Restangular.all('nationality/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                getNationalityById: function(id) {
                    return Restangular.all('nationality').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                saveEntry: function(data) {
                    return Restangular.all('nationality').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },


                getAllNationality: function() {
                    return Restangular.all('nationality').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;


                        });
                }
            };
        }]);

})();