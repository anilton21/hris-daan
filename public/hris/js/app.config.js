(function() {
    'use strict';

    angular.module('hris')
        .config(appConfig);

    appConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', '$httpProvider', 'ngDialogProvider', '$activityIndicatorProvider', 'RestangularProvider', 'API_URL', 'API_VERSION', 'cfpLoadingBarProvider', 'toastrConfig'];

    function appConfig($stateProvider, $locationProvider, $urlRouterProvider, $httpProvider, ngDialogProvider, $activityIndicatorProvider, RestangularProvider, API_URL, API_VERSION, cfpLoadingBarProvider, toastrConfig) {

        $httpProvider.interceptors.push('authInterceptor');

        RestangularProvider.setBaseUrl(API_URL + API_VERSION);
        $activityIndicatorProvider.setActivityIndicatorStyle('SpinnerDark');
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });
        $locationProvider.hashPrefix("");

        cfpLoadingBarProvider.includeSpinner = true;

        ngDialogProvider.setDefaults({
            className: 'ngdialog-theme-default',
            plain: false,
            showClose: false,
            closeByDocument: true,
            closeByEscape: true,
            appendTo: false
        });

        angular.extend(toastrConfig, {
            'body-output-type': 'trustedHtml'
        });

        $urlRouterProvider.otherwise('/auth/login');

        $stateProvider
            .state('auth', {
                url: '/auth',
                abstract: true,
                templateUrl: 'public/hris/templates/auth/auth.html',
                controller: 'AuthCtrl'
            })
            .state('auth.login', {
                url: '/login',
                views: {
                    "authContent": {
                        templateUrl: 'public/hris/templates/auth/login.html'
                    }
                },
                data: {
                    module: '',
                    tab: 0
                }
            })
            .state('auth.forgot', {
                url: '/forgot',
                views: {
                    "authContent": {
                        templateUrl: 'public/kiosk/templates/auth/forgot.html',
                        controller: 'forgotCtrl'
                    }
                },
                data: {
                    module: '',
                    tab: 0
                }
            })
            .state('main', {
                url: '/main',
                abstract: true,
                templateUrl: 'public/hris/templates/main.html',
                controller: 'mainCtrl'
            })
            .state('main.home', {
                url: '/home',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/dashboard.html'
                    }
                },
                data: {
                    module: 'Overview',
                    tab: 0
                }
            })
            .state('main.employeeprofile', {
                url: '/employeeprofile/:e_id',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/humanresource/employee/employeeprofile/employeeprofile.html',
                        controller: 'employeeProfileCtrl'
                    }
                },
                data: {
                    module: 'Human Resource',
                    tab: 1
                }
            })
            .state('main.masterlist', {
                url: '/masterlist',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/humanresource/masterlist/masterlist.html',
                        controller: 'employeeCtrl'
                    }
                },
                data: {
                    module: 'Human Resource',
                    tab: 1
                }
            })
            .state('main.masterlist_print', {
                url: '/masterlist/print',
                params: {
                    action: null,
                    _id: null
                },
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/humanresource/masterlist/print.html',
                        controller: 'employeeMasterlistPrintCtrl'
                    }
                },
                data: {
                    module: 'Human Resource',
                    tab: 1
                }
            })
            .state('main.workschedules', {
                url: '/workschedules',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/humanresource/workschedule/workschedules.html',
                        controller: 'worksSchedulesCtrl'
                    }
                },
                data: {
                    module: 'Human Resource',
                    tab: 1
                }
            })
            .state('main.dailytimerecord', {
                url: '/dailytimerecord/:emp_id',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/attendance/DTR/dailytimerecord.html',
                        controller: 'dailyTimeRecordCtrl'
                    }
                },
                data: {
                    module: 'Attendance Management',
                    tab: 2
                }
            })
            .state('main.overtime', {
                url: '/overtime/:emp_id',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/attendance/Overtime/overtime.html',
                        controller: 'overtimeCtrl'
                    }
                },
                data: {
                    module: 'Attendance Management',
                    tab: 2
                }
            })
            .state('main.overtime_term', {
                url: '/overtime/:emp_id/term/:payroll_id',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/attendance/Overtime/overtime.html',
                        controller: 'overtimeCtrl'
                    }
                },
                data: {
                    module: 'Attendance Management',
                    tab: 2
                }
            })
            .state('main.overtime_list', {
                url: '/overtimelist',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/attendance/Overtime/overtime.list.html',
                        controller: 'overtimeListCtrl'
                    }
                },
                data: {
                    module: 'Attendance Management',
                    tab: 2
                }
            })
            .state('main.payrollsetup', {
                url: '/payrollsetup',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/payroll/payrollsetup/setup.html',
                        controller: 'payrollSetupCtrl'
                    }
                },
                data: {
                    module: 'Payroll',
                    tab: 3
                }
            })
            .state('main.manualpayroll', {
                url: '/manualpayroll/:emp_id',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/payroll/manual/manual.html',
                        controller: 'manualPayrollCtrl'
                    }
                },
                data: {
                    module: 'Payroll',
                    tab: 3
                }
            })
            .state('main.payrolllist', {
                url: '/payrolllist',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/payroll/listpayroll/payroll.html',
                        controller: 'payrollListCtrl'
                    }
                },
                data: {
                    module: 'Payroll',
                    tab: 3
                }
            })
            .state('main.generalpayroll', {
                url: '/generalpayroll',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/payroll/general/general.html',
                        controller: 'generalPayrollCtrl'
                    }
                },
                data: {
                    module: 'Payroll',
                    tab: 3
                }
            })
            .state('main.companyprofile', {
                url: '/companyprofile',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/setupmanager/company/company.html',
                        controller: 'companyCtrl'
                    }
                },
                data: {
                    module: 'Setup Manager',
                    tab: 4
                }
            })
            .state('main.branches', {
                url: '/branches',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/setupmanager/branch/branches.html',
                        controller: 'branchesCtrl'
                    }
                },
                data: {
                    module: 'Setup Manager',
                    tab: 4
                }
            })
            .state('main.divisions', {
                url: '/divisions',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/setupmanager/division/division.html',
                        controller: 'divisionsCtrl'
                    }
                },
                data: {
                    module: 'Setup Manager',
                    tab: 4
                }
            })
            .state('main.departments', {
                url: '/departments',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/setupmanager/department/departments.html',
                        controller: 'departmentsCtrl'
                    }
                },
                data: {
                    module: 'Setup Manager',
                    tab: 4
                }
            })
            .state('main.jobpositions', {
                url: '/jobpositions',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/setupmanager/jobtitle/jobpositions.html',
                        controller: 'jobpositionsCtrl'
                    }
                },
                data: {
                    module: 'Setup Manager',
                    tab: 4
                }
            })
            .state('main.employmenttypes', {
                url: '/employmenttypes',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/setupmanager/employmenttype/types.html',
                        controller: 'emptypesCtrl'
                    }
                },
                data: {
                    module: 'Setup Manager',
                    tab: 4
                }
            })
            .state('main.nationality', {
                url: '/nationality',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/setupmanager/nationality/nationality.html',
                        controller: 'nationalityCtrl'
                    }
                },
                data: {
                    module: 'Setup Manager',
                    tab: 4
                }
            })
            .state('main.religions', {
                url: '/religions',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/setupmanager/religion/religions.html',
                        controller: 'religionsCtrl'
                    }
                },
                data: {
                    module: 'Setup Manager',
                    tab: 4
                }
            })
            .state('main.appsettings', {
                url: '/appsettings',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/tools/appsettings/appsettings.html',
                        controller: 'appSettingsCtrl'
                    }
                },
                data: {
                    module: 'Tools',
                    tab: 5
                }
            })
            .state('main.useraccounts', {
                url: '/useraccounts',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/tools/useraccounts/users.html',
                        controller: 'usersCtrl'
                    }
                },
                data: {
                    module: 'Tools',
                    tab: 5
                }
            })
            .state('main.userprivilege', {
                url: '/userprivilege/:user_id',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/tools/useraccounts/user.access.html',
                        controller: 'userAccessCtrl'
                    }
                },
                data: {
                    module: 'Tools',
                    tab: 5
                }
            })
            .state('main.groupprivilege', {
                url: '/usergroupprivilege/:group_id',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/tools/useraccounts/group.access.html',
                        controller: 'groupAccessCtrl'
                    }
                },
                data: {
                    module: 'Tools',
                    tab: 5
                }
            })
            .state('main.chartsofaccounts', {
                url: '/chartsofaccounts',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/chartsofaccounts/chartsofaccounts.html',
                        controller: 'accountsCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            })
            .state('main.accounts_print', {
                url: '/chartsofaccounts/print',
                params: {
                    action: null,
                    _id:null
                },
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/chartsofaccounts/print.html',
                        controller: 'accountsPrintCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            })
            .state('main.sss', {
                url: '/sss',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/sss/sss.html',
                        controller: 'sssCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            })
            .state('main.sss_detail', {
                url: '/sss/:sss_id',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/sss/sss.detail.html',
                        controller: 'sssDetailCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            })
            .state('main.sssbracket_print', {
                url: '/sss/:sss_id/print',
                params: {
                    action: null,
            
                },
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/sss/print.html',
                        controller: 'sssDetailCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            })
            .state('main.phic', {
                url: '/phic',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/phic/phic.html',
                        controller: 'phicCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            })
            .state('main.phic_detail', {
                url: '/phic/:phic_id',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/phic/phic.detail.html',
                        controller: 'phicDetailCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            })
            .state('main.phicbracket_print', {
                url: '/phic/:phic_id/print',
                params: {
                    action: null,
            
                },
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/phic/print.html',
                        controller: 'phicDetailCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            })
            .state('main.hdmf', {
                url: '/hdmf',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/hdmf/hdmf.html',
                        controller: 'hdmfCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            })
            .state('main.hdmf_print', {
                url: '/hdmf/print',
                params: {
                    action: null,
                    _id:null
                },
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/hdmf/print.html',
                        controller: 'hdmfPrintCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            })
            .state('main.wtax', {
                url: '/wtax',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/wtax/taxtable/tax.html',
                        controller: 'taxCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            })
            .state('main.wtax_detail', {
                url: '/wtax/:tax_id',
                views: {
                    "mainContent": {
                        templateUrl: 'public/hris/templates/accounting/wtax/taxtable/tax.detail.html',
                        controller: 'taxDetailCtrl'
                    }
                },
                data: {
                    module: 'Accounting',
                    tab: 6
                }
            });

    }
})();