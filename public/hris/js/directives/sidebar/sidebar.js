(function () {
    'use strict';

    angular.module('hris')
        .directive('sidebar', function () {
            return {
                restrict: 'AE',
                templateUrl: 'public/hris/js/directives/sidebar/sidebar.html'
            };
        });

})();