(function () {
    'use strict';

    angular.module('hris')
        .directive('header', function () {
            return {
                restrict: 'AE',
                templateUrl: 'public/hris/js/directives/headers/header.html',
                replace: true
            };
        });

})();