(function () {
    'use strict';

    angular.module('hris')
        .run(appRun);

    appRun.$inject = ['$rootScope', '$state', 'toastr', 'localStorageService', '$location','auth'];

    function appRun($rootScope, $state, toastr, localStorageService, $location,auth) {

        $rootScope.$on('$stateChangeStart', function (event, toState) {
            if (toState.name !== 'auth.login') {
                $rootScope.module = toState.data.module;
                $rootScope.selectedTab = toState.data.tab;
            }
        });

        $rootScope.$on('unauthorized', function() {
            localStorageService.set('user',null);
            localStorageService.set('authdata', null);
            localStorageService.set('hris.payroll', null);            

            $state.go('login');
        });
    }
})();