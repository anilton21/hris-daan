'use strict';

angular.module('kiosk')
    .run(appConfig);

appConfig.$inject = ['$rootScope', '$state', 'toastr', 'localStorageService', '$location'];

function appConfig($rootScope, $state, toastr, localStorageService, $location) {
    $rootScope.$on('$stateChangeStart', function(event, toState) {
        if (toState.name !== 'auth.login' || toState.name !== 'auth.forgot') {
            $rootScope.module = toState.data.module;
            $rootScope.selectedTab = toState.data.tab;
        }

        /* if (_.isNull(localStorageService.get('kiosk.user'))) {
            $location.path('/login');
        } else if (!_.isNull(localStorageService.get('kiosk.user'))) {
            $state.go('main.home');
        } */
    });

    $rootScope.$on('unauthorized', function() {
        localStorageService.set('kiosk.user', null);
        localStorageService.set('kiosk.authdata', null);
        localStorageService.set('hris.payroll', null);

        $state.go('login');
    });
}