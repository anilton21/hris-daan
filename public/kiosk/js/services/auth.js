'use strict';

angular.module('kiosk')
    .factory('auth', auth);

auth.$inject = ['Restangular', 'Base64', 'localStorageService','$http'];

function auth(Restangular, Base64, localStorageService,$http) {
    return {
        isAuthenticated: false,
        login: function (username, password) {
            var authdata = Base64.encode(username + ':' + password);
                localStorageService.set('kiosk.authdata', authdata);
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
            return Restangular.all('login').customPOST().then(function (res) {
                    return res;
                },
                function (err) {
                    return err.data;
                });
        },
        storeUser: function (user) {
            localStorageService.set('kiosk.user', user);
            return true;
        },
        getUser: function (user) {
            return localStorageService.get('kiosk.user');
        },
        logout: function () {
            return Restangular.all('logout').customGET().then(function (res) {
                return res;
            },
            function (err) {
                return err.data;
            });
        },
        forgotPassword: function (data) {
            return Restangular.all('forgotpassword').customPOST(data).then(function (res) {
                return res;
            },
            function (err) {
                return err.data;
            });
        },
    }
}