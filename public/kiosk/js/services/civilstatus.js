(function() {
    'use strict';

    angular.module('kiosk')
        .factory('civilstatus', ['Restangular', function(Restangular) {
            return {
                getCivilStatusById: function(id) {
                    return Restangular.all('civilstatus').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },
                getAllCivilStatus: function() {
                    return Restangular.all('civilstatus').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }]);

})();