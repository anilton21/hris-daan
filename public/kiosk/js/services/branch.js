(function() {
    'use strict';

    angular.module('kiosk')
        .factory('branch', ['Restangular', function(Restangular) {
            return {
                getBranchId: function(id) {
                    return Restangular.all('branch').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },
                getAllBranch: function(params) {
                    return Restangular.all('branch').customGET("", params).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;


                        });
                }
            };
        }]);

})();