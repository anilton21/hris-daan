'use strict';

angular.module('kiosk')
    .factory('timelogs', ['$http', 'Restangular', function timelogs($http, Restangular) {
        return {
            getEmployeeTimeLogs: function(deviceno, dateFrom, dateTo) {
                return Restangular.all('attendance/' + deviceno + '/timelogs?datefrom=' + dateFrom + '&dateto=' + dateTo).customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getPayrollConfig: function() {
                return Restangular.all('payroll/config').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }])
    .factory('attendance', ['Restangular', function(Restangular) {
        return {
            saveEmployeeTimeKeeping: function(emp_id, payroll_id, data) {
                return Restangular.all('attendance/' + emp_id + '/payroll/' + payroll_id).customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getEmployeeAttendance: function(emp_id, payroll_id) {
                return Restangular.all('attendance/' + emp_id + '/payroll/' + payroll_id).customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deleteEmployeeAttendance: function(emp_id, payroll_id) {
                return Restangular.all('attendance/' + emp_id + '/payroll/' + payroll_id).customDELETE().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            postEmployeeAttendance: function(emp_id, payroll_id) {
                return Restangular.all('attendance/' + emp_id + '/payroll/' + payroll_id + '/post').customPOST().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            isEmployeeAttendancePosted: function(emp_id, payroll_id) {
                return Restangular.all('attendance/' + emp_id + '/payroll/' + payroll_id + '/post').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getEmployeeRawTimelogs: function(device_no, params) {
                return Restangular.all('attendance/' + device_no + '/timelogs').customGET("", params).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            saveEmployeeRawTimelogs: function(device_no, data) {
                return Restangular.all('attendance/' + device_no + '/timelogs').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deleteEmployeeRawTimelogs: function(EntryID, device_no) {
                return Restangular.all('attendance/' + device_no + '/timelogs/' + EntryID).customDELETE().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            updateEmployeeRawTimelogs: function(EntryID, device_no, data) {
                return Restangular.all('attendance/' + device_no + '/timelogs/' + EntryID).customPUT(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getAllOvertimeTypes: function() {
                return Restangular.all('overtime_types').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            createEmployeeOvertime: function(emp_id, payroll_id, data) {
                return Restangular.all('overtime/' + emp_id + '/payroll/' + payroll_id).customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getAllOvertime: function(payroll_id) {
                return Restangular.all('overtime/' + payroll_id + '/payroll').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getEmployeeOvertime: function(emp_id, payroll_id) {
                return Restangular.all('overtime/' + emp_id + '/payroll/' + payroll_id).customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            ApprovedOvertime: function(refno) {
                return Restangular.all('overtime/' + refno + '/approved').customPUT().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            ApprovedEmployeeOvertime: function(emp_id, payroll_id, IndexID) {
                return Restangular.all('overtime/' + emp_id + '/payroll/' + payroll_id + '/items/' + IndexID + '/approved').customPUT().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            DeleteOvertime: function(refno) {
                return Restangular.all('overtime/' + refno).customDELETE().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            Delete_OverTime_Details: function(emp_id, payroll_id, IndexID) {
                return Restangular.all('overtime/' + emp_id + '/payroll/' + payroll_id + '/items/' + IndexID).customDELETE().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            Update_OverTime_Details: function(emp_id, payroll_id, IndexID, data) {
                return Restangular.all('overtime/' + emp_id + '/payroll/' + payroll_id + '/items/' + IndexID + '/log').customPUT(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }]);