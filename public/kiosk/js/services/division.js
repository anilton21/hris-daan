(function() {
    'use strict';

    angular.module('kiosk')
        .factory('division', ['Restangular', function(Restangular) {
            return {
                getDivisionsById: function(id) {
                    return Restangular.all('divisions').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },
                getAllDivisions: function() {
                    return Restangular.all('divisions').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }]);

})();