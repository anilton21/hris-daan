(function () {
    'use strict';

    angular.module('kiosk')
        .factory('users', users);

    users.$inject = ['Restangular'];

    function users(Restangular) {
        return {
            GetUserByUserID: function (UserID) {
                return Restangular.all('useraccounts/' + UserID).customGET().then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            Update_User: function (UserID, data) {
                return Restangular.all('useraccounts/' + UserID).customPUT(data).then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            },
            setUserPrivilege: function (UserID, data) {
                return Restangular.all('useraccounts/' + UserID + '/access').customPUT(data).then(function (res) {
                        return res;
                    },
                    function (err) {
                        return err.data;
                    });
            }
        }
    }

})();