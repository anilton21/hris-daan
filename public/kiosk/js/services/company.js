(function() {
    'use strict';

    angular.module('kiosk')
        .factory('company', ['Restangular', function(Restangular) {
            return {

                getCompanyById: function(id) {
                    return Restangular.all('company').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                AddCompanyProfile: function(ci_id, data) {
                    return Restangular.all('company/' + ci_id + '/image').customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                deleteCompanyProfile: function(id) {
                    return Restangular.all('company/' + id + '/image').customDELETE().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                updateCompanyInfo: function(ci_id, data) {
                    return Restangular.all('company/' + ci_id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                saveEntry: function(data) {
                    return Restangular.all('company').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },
                getCompanyInfo: function(params) {
                    return Restangular.all('company').customGET("", params).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;


                        });
                }
            };
        }]);

})();