'use strict';

angular.module('kiosk')
    .directive('header', function() {
        return {
            restrict: 'AE',
            templateUrl: 'public/kiosk/js/directives/headers/header.html',
            replace: true
        };
    });
  