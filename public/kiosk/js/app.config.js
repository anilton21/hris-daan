'use strict';

angular.module('kiosk')
    .config(appConfig);

appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', 'ngDialogProvider', '$activityIndicatorProvider', '$locationProvider', 'RestangularProvider', 'API_URL', 'API_VERSION'];

function appConfig($stateProvider, $urlRouterProvider, $httpProvider, ngDialogProvider, $activityIndicatorProvider, $locationProvider, RestangularProvider, API_URL, API_VERSION) {

    $httpProvider.interceptors.push('authInterceptor');
    $activityIndicatorProvider.setActivityIndicatorStyle('SpinnerDark');

    RestangularProvider.setBaseUrl(API_URL + API_VERSION);
    $locationProvider.html5Mode({
        enabled: false,
        requireBase: false
    });
    $locationProvider.hashPrefix("");


    ngDialogProvider.setDefaults({
        className: 'ngdialog-theme-default',
        plain: false,
        showClose: false,
        closeByDocument: true,
        closeByEscape: true,
        appendTo: false
    });

    $urlRouterProvider.otherwise('/auth/login');




    $stateProvider
        .state('auth', {
            url: '/auth',
            abstract: true,
            templateUrl: 'public/kiosk/templates/auth/auth.html'
        })
        .state('auth.login', {
            url: '/login',
            views: {
                "authContent": {
                    templateUrl: 'public/kiosk/templates/auth/login.html',
                    controller: 'AuthCtrl'
                }
            },
            data: {
                module: '',
                tab: 0
            }
        })
        .state('auth.forgot', {
            url: '/forgot',
            views: {
                "authContent": {
                    templateUrl: 'public/kiosk/templates/auth/forgot.html',
                    controller: 'forgotCtrl'
                }
            },
            data: {
                module: '',
                tab: 0
            }
        })
        .state('main', {
            url: '/main',
            abstract: true,
            templateUrl: 'public/kiosk/templates/kiosk.html',
            controller: 'mainCtrl'
        })
        .state('main.home', {
            url: '/home',
            views: {
                "mainContent": {
                    templateUrl: 'public/kiosk/templates/main.html'
                }
            },
            data: {
                module: 'Overview',
                tab: 0
            }
        })
        .state('main.employeeprofile', {
            url: '/employeeprofile',
            views: {
                "mainContent": {
                    templateUrl: 'public/kiosk/templates/humanresource/employeeprofile/employeeprofile.html',
                    controller: 'employeeProfileCtrl'
                }
            },
            data: {
                module: 'Human Resource',
                tab: 1
            }
        })
        .state('main.masterlist', {
            url: '/masterlist',
            views: {
                "mainContent": {
                    templateUrl: 'public/kiosk/templates/humanresource/masterlist/masterlist.html',
                    controller: 'employeeCtrl'
                }
            },
            data: {
                module: 'Human Resource',
                tab: 1
            }
        })
        .state('main.dailytimerecord', {
            url: '/dailytimerecord',
            views: {
                "mainContent": {
                    templateUrl: 'public/kiosk/templates/attendance/DTR/dailytimerecord.html',
                    controller: 'dailyTimeRecordCtrl'
                }
            },
            data: {
                module: 'Attendance Management',
                tab: 2
            }
        })
        .state('main.dailytimerecord_print', {
            url: '/dailytimerecord/print',
            params: {
                payroll: null,
                employee: null,
                attendance: null
            },
            views: {
                "mainContent": {
                    templateUrl: 'public/kiosk/templates/attendance/DTR/print_dtr.html',
                    controller: 'dailyTimeRecordPrintCtrl'
                }
            },
            data: {
                module: 'Attendance Management',
                tab: 2
            }
        })
        .state('main.print_dtr', {
            url: '/dailytimerecord/:payrollID/print',
            views: {
                "mainContent": {
                    templateUrl: 'public/kiosk/templates/timelogs/pdf.html',
                    controller: 'timelogsExportCtrl'
                }
            },
            data: {
                module: 'TimeLogs',
                tab: 1
            }
        })
        .state('main.overtime', {
            url: '/overtime',
            views: {
                "mainContent": {
                    templateUrl: 'public/kiosk/templates/attendance/overtime/overtime.html',
                    controller: 'overtimeCtrl'
                }
            },
            data: {
                module: 'Attendance Management',
                tab: 2
            }
        })
        .state('main.manualpayroll', {
            url: '/manualpayroll',
            views: {
                "mainContent": {
                    templateUrl: 'public/kiosk/templates/payroll/manual/manual.html',
                    controller: 'manualPayrollCtrl'
                }
            },
            data: {
                module: 'Payroll',
                tab: 3
            }
        });
    /*.state('main.timelogs', {
        url: '/timelogs',
        views: {
            "mainContent": {
                templateUrl: 'public/kiosk/templates/timelogs/timelogs.html',
                controller: 'timelogsCtrl'
            }
        },
        data: {
            module: 'TimeLogs',
            tab: 1
        }
    });*/

};