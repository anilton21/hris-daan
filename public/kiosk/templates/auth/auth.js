'use strict';

angular.module('kiosk')
    .controller('AuthCtrl', ['$scope', '$state', 'toastr', 'auth',
        function AuthCtrl($scope, $state, toastr, auth) {
            if (auth.getUser()) {
                $state.go('main.home');
            }

            $scope.login = function (username, password) {
                $scope.submitting = true;
                auth.login(username, password).then(function (resp) {
                    if (_.isArray(resp)) {
                        _.each(resp, function (row) {
                            toastr.error(row.msg, 'Error');
                        });
                    }
                    if (resp.statusCode === 200 && resp.response.success) {
                        auth.storeUser(resp.response.result);
                        $state.go('main.home');
                    } else if (resp.statusCode === 400 && !resp.response.success && _.isArray(resp.response.result)) {
                        _.each(resp.response.result, function (msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (resp.statusCode === 401 && !resp.response.success && _.isArray(resp.response.result)) {
                        _.each(resp.response.result, function (msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        $scope.submitting = false;
                        toastr.error(resp.response.msg, 'Error');
                    }
                });
            };
        }
    ])
    .controller('forgotCtrl', ['$scope', '$state', 'toastr', 'auth',
        function AuthCtrl($scope, $state, toastr, auth) {
            $scope.user = {};

            $scope.forgotpass = function() {                
                auth.forgotPassword($scope.user).then(function (resp) {
                    if (resp.statusCode === 200 && resp.response.success) {
                        toastr.success(resp.response.msg, 'Success');
                        $scope.user = {};
                    } else if (resp.statusCode === 400 && !resp.response.success && _.isArray(resp.response.result)) {
                        _.each(resp.response.result, function (msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (resp.statusCode === 401 && !resp.response.success && _.isArray(resp.response.result)) {
                        _.each(resp.response.result, function (msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(resp.response.msg, 'Error');
                    }
                });
            };
        }
    ]);