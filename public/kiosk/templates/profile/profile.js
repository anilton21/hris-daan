(function() {
    'use strict';

    angular.module('kiosk')
        .controller('profileCtrl', profileCtrl);

    profileCtrl.$inject = ['$scope', '$state', 'users', 'localStorageService', '$uibModalInstance', '$uibModal', 'employee', 'auth', 'toastr', '$timeout'];

    function profileCtrl($scope, $state, users, localStorageService, $uibModalInstance, $uibModal, employee, auth, toastr, $timeout) {
        $scope.currentUser = {};
        $scope.profileImage = null;
        $scope.user = {};

        $scope.getEmployeeProfile = function(e_id) {
            employee.getEmployeeProfile(e_id, {}).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var employeeD = data.response.result;
                    if (!_.isEmpty(employeeD)) {
                        $scope.employee = employeeD;

                        if ($scope.employee.Photo) {
                            $scope.profileImage = 'data:image/png;base64,' + $scope.employee.Photo;
                        }
                    }
                }
            });
        };

        $scope.GetUserByUserID = function(UUID) {
            users.GetUserByUserID(UUID).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var user = data.response.result;
                    if (!_.isEmpty(user)) {
                        $scope.user = user;
                    }
                }
            });
        };

        if (auth.getUser()) {
            $scope.currentUser = auth.getUser().user;

            $scope.getEmployeeProfile($scope.currentUser.E_UUID);
            $scope.GetUserByUserID($scope.currentUser.UUID);
        }

        $scope.gotoProfile = function() {
            $state.go('main.employeeprofile', {
                e_id: $scope.currentUser.E_UUID
            }, null, { reload: true });
        };

        $scope.selectEntry = function() {
            users.Update_User($scope.currentUser.UUID, $scope.user).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'SUCCESS');

                    $timeout(function() {
                        if (auth.getUser()) {
                            $scope.currentUser = auth.getUser().user;

                            $scope.getEmployeeProfile($scope.currentUser.E_UUID);
                            $scope.GetUserByUserID($scope.currentUser.UUID);
                        }
                    }, 300);
                } else {
                    toastr.error(data.response.msg, 'ERROR');
                    return;
                }
            })
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();