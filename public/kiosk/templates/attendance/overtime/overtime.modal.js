(function() {
    'use strict';

    angular.module('kiosk')
        .controller('overtimeModalCtrl', overtimeModalCtrl);

    overtimeModalCtrl.$inject = ['$scope', '$uibModalInstance', 'attendance', 'details', 'localStorageService', 'dateArray', 'toastr','$uibModal'];

    function overtimeModalCtrl($scope, $uibModalInstance, attendance, details, localStorageService, dateArray, toastr,$uibModal) {
        $scope.overtime = {};
        $scope.overtimeTypes = [];
        $scope.currentPayroll = {};
        $scope.popup2 = {
            opened: false
        };

        if (localStorageService.get('hris.payroll')) {
            $scope.currentPayroll = localStorageService.get('hris.payroll');
        }

        Date.prototype.toIsoString = function() {
            var tzo = -this.getTimezoneOffset(),
                dif = tzo >= 0 ? '+' : '-',
                pad = function(num) {
                    var norm = Math.floor(Math.abs(num));
                    return (norm < 10 ? '0' : '') + norm;
                };
            return this.getFullYear() +
                '-' + pad(this.getMonth() + 1) +
                '-' + pad(this.getDate()) +
                'T' + pad(this.getHours()) +
                ':' + pad(this.getMinutes()) +
                ':' + pad(this.getSeconds()) +
                dif + pad(tzo / 60) +
                ':' + pad(tzo % 60);
        };

        async.waterfall([
            function(callback) {
                attendance.getAllOvertimeTypes().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var types = data.response.result;
                        if (!_.isEmpty(types)) {
                            $scope.overtimeTypes = types;
                        }
                        callback();
                    }
                });
            },
            function(callback) {
                if (details) {
                    $scope.overtime.InclusiveDates = new Date(details.InclusiveDates);
                    $scope.overtime.OTType = details.OTType;
                    $scope.overtime.Remarks = details.Remarks;
                    $scope.overtime.RefNo = details.RefNo;
                    $scope.overtime.IsApproved = details.IsApproved;
                    $scope.overtime.IsPreApproved = details.IsPreApproved;
                    $scope.overtime.Requestor = details.Requestor;
                    $scope.overtime.Project = details.Project;
                    $scope.overtime.OTExpiry = details.OTExpiry;
                    $scope.overtime.IndexID = details.IndexID;
                    $scope.overtime.IsExpired = details.IsExpired;
                    $scope.overtime.PreApprovedUserID = details.PreApprovedUserID;
                    $scope.overtime.ApprovedUserID = details.ApprovedUserID;
                } else {
                    $scope.overtime.TransDate = new Date();
                    $scope.overtime.InclusiveDates = new Date();
                    $scope.overtime.IsApproved = 0;
                    $scope.overtime.IsPreApproved = 0;
                }
            }
        ]);

        $scope.frmEmployeePicker = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/pickemployee.modal.html',
                controller: 'employeePickerCtrl',
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.overtime.Requestor = selectedItem;
                }
            }, function() {});
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };

        $scope.saveEntry = function() {
            var formatt = moment($scope.overtime.InclusiveDates).format('YYYY-MM-DD');

            var result = _.find(dateArray, { 'Days': formatt });
            if (_.isEmpty(result)) {
                toastr.warning('OVERTIME DATE is not belong on the current PAYROLL PERIOD');
                return;
            }
            $uibModalInstance.close($scope.overtime);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();