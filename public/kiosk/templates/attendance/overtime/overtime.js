(function() {
    'use strict';

    angular.module('kiosk')
        .controller('overtimeCtrl', overtimeCtrl);

    overtimeCtrl.$inject = ['$scope', '$uibModal', '$stateParams', 'localStorageService', '$state', 'employee', 'ngDialog', 'attendance', 'toastr', '$timeout', 'workschedules', '$filter'];

    function overtimeCtrl($scope, $uibModal, $stateParams, localStorageService, $state, employee, ngDialog, attendance, toastr, $timeout, workschedules, $filter) {
        $scope.currentPayroll = {};
        $scope.employee = {};
        $scope.overtime = {};
        $scope.overtimes = [];
        $scope.dateArray = [];
        $scope.workDays = [];

        $scope.IsOverTimePosted = false;
        $scope.IsDisabled = true;
        $scope.IsOvertimeSaved = false;
        $scope.refNo = null;
        $scope.overtimeExpiry = null;
        $scope.TotalNoHrs = 0;

        Date.prototype.addDays = function(days) {
            var dat = new Date(this.valueOf())
            dat.setDate(dat.getDate() + days);
            return dat;
        };

        function getDates(startDate, stopDate) {
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push(currentDate)
                currentDate = currentDate.addDays(1);
            }
            return dateArray;
        }

        $scope.TotalNoHrs = 0;


        $scope.getEmployeeProfile = function(e_id) {
            employee.getEmployeeProfile(e_id, {}).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var employeeD = data.response.result;
                    if (!_.isEmpty(employeeD)) {
                        $scope.employee = employeeD;

                        if ($scope.employee.Photo) {
                            $scope.profileImage = 'data:image/png;base64,' + $scope.employee.Photo;
                        }

                        if (_.isEmpty($scope.refNo)) {
                            var refDate = new Date();
                            $scope.refNo = 'OT-' + $scope.employee.e_idno + '-' + refDate.getFullYear() + refDate.getMonth() + refDate.getDate();
                        }

                        $scope.IsDisabled = false;
                    }
                } else {
                    toastr.warning(data.response.msg +
                        "Please contact the HUMAN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                    $timeout(function() {
                        $state.go('main.dailytimerecord', {
                            emp_id: null
                        });
                    }, 300);
                    return;
                }
            });
        };

        $scope.getEmployeeOvertime = function(emp_id, payroll_id) {
            attendance.getEmployeeOvertime(emp_id, payroll_id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var result = data.response.result;
                    if (!_.isEmpty(result)) {
                        $scope.overtime = result;

                        $scope.refNo = $scope.overtime.RefNo;

                        if (_.isEmpty($scope.refNo)) {
                            var refDate = new Date();
                            $scope.refNo = 'OT-' + $scope.employee.e_idno + '-' + refDate.getFullYear() + refDate.getMonth() + refDate.getDate();
                        }

                        var OTExpiry = new Date($scope.overtime.OTExpiry);
                        $scope.overtimeExpiry = moment(OTExpiry).format('YYYY-MM-DD hh:mm:ss a');

                        $scope.overtime.overtimes = result.details;
                        $scope.overtimes = result.details;

                        if (result.details && result.details.length > 0) {
                            var results = _.reject(result.details, { 'ApprovedDate': null, 'IsApproved': 0 });
                            var regData = _.filter(results, { 'OTType': 1 });
                            var holData = _.filter(results, { 'OTType': 2 });
                            var splData = _.filter(results, { 'OTType': 3 });
                            var sunData = _.filter(results, { 'OTType': 4 });

                            $scope.TotalNoHrs = _.sumBy(result.details, function(o) { return o.NoHrs }) || 0;
                            $scope.TotalApprovedNoHrs = _.sumBy(results, function(o) { return o.NoHrs }) || 0;
                            $scope.TotalRegOT = _.sumBy(regData, function(o) { return o.NoHrs }) || 0;
                            $scope.TotalHolOT = _.sumBy(holData, function(o) { return o.NoHrs }) || 0;
                            $scope.TotalSplOT = _.sumBy(splData, function(o) { return o.NoHrs }) || 0;
                            $scope.TotalSunOT = _.sumBy(sunData, function(o) { return o.NoHrs }) || 0;
                        } else {
                            $scope.TotalApprovedNoHrs = 0;
                            $scope.TotalNoHrs = 0;
                            $scope.TotalRegOT = 0;
                            $scope.TotalHolOT = 0;
                            $scope.TotalSplOT = 0;
                            $scope.TotalSunOT = 0;
                        }

                        _.each($scope.overtimes, function(row) {
                            row.IsExpired = false;
                            if (row.TimeIn) {
                                row.TimeIn = new Date(row.TimeIn);
                            }
                            if (row.TimeOut) {
                                row.TimeOut = new Date(row.TimeOut);
                            }
                            row.OTExpiry = new Date(row.OTExpiry);
                            if (_.isEmpty(row.PreApprovedBy)) {
                                if (new Date() >= row.OTExpiry) {
                                    row.IsExpired = true;
                                }
                            }
                        });
                        $scope.overtimes = $filter('orderBy')($scope.overtimes, 'InclusiveDates');
                        console.log('$scope.overtimes: ', $scope.overtimes);
                        if (_.find($scope.overtimes, {
                                'IsApproved': 1
                            })) {
                            $scope.IsOverTimePosted = true;
                        }
                    }
                }
            })
        };

        async.waterfall([
            function(callback) {
                workschedules.getAllDays().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var workDays = data.response.result;
                        if (!_.isEmpty(workDays)) {
                            $scope.workDays = workDays;
                        }
                        callback();
                    }
                });
            },
            function(callback) {
                if (localStorageService.get('kiosk.user')) {
                    $scope.currentUser = localStorageService.get('kiosk.user').user;
                    $scope.getEmployeeProfile($scope.currentUser.E_UUID);

                    if (localStorageService.get('hris.payroll')) {
                        $scope.currentPayroll = localStorageService.get('hris.payroll');
                        var dateArray = getDates(new Date($scope.currentPayroll.FirstDate), new Date($scope.currentPayroll.SecondDate));
                        for (var i = 0; i < dateArray.length; i++) {
                            var date = new Date(dateArray[i]);
                            $scope.dateArray.push({
                                Days: moment(date).format('YYYY-MM-DD')
                            });
                        }

                        $scope.getEmployeeOvertime($scope.currentUser.Emp_UUID, $scope.currentPayroll.UUID);
                    }
                }
            }
        ]);


        $scope.refreshData = function() {
            $scope.currentPayroll = {};
            $scope.employee = {};
            $scope.overtime = {};

            $scope.overtimes = [];
            $scope.dateArray = [];
            $scope.workDays = [];
            $scope.TotalNoHrs = 0;

            async.waterfall([
                function(callback) {
                    workschedules.getAllDays().then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            var workDays = data.response.result;
                            if (!_.isEmpty(workDays)) {
                                $scope.workDays = workDays;
                            }
                            callback();
                        }
                    });
                },
                function(callback) {
                    if (localStorageService.get('kiosk.user')) {
                        $scope.currentUser = localStorageService.get('kiosk.user').user;
                        $scope.getEmployeeProfile($scope.currentUser.E_UUID);

                        if (localStorageService.get('hris.payroll')) {
                            $scope.currentPayroll = localStorageService.get('hris.payroll');
                            var dateArray = getDates(new Date($scope.currentPayroll.FirstDate), new Date($scope.currentPayroll.SecondDate));
                            for (var i = 0; i < dateArray.length; i++) {
                                var date = new Date(dateArray[i]);
                                $scope.dateArray.push({
                                    Days: moment(date).format('YYYY-MM-DD')
                                });
                            }

                            $scope.getEmployeeOvertime($scope.currentUser.Emp_UUID, $scope.currentPayroll.UUID);
                        }
                    }
                }
            ]);
        };

        $scope.payrollOptions = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/kiosk/templates/attendance/DTR/payroll.option.modal.html',
                controller: 'dtrPayrollOptionCtrl',
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.currentPayroll = selectedItem;
                    localStorageService.set('hris.payroll', selectedItem);

                    $scope.dateArray = [];
                    var dateArray = getDates(new Date(selectedItem.FirstDate), new Date(selectedItem.SecondDate));
                    for (var i = 0; i < dateArray.length; i++) {
                        var date = new Date(dateArray[i]);
                        $scope.dateArray.push({
                            Days: moment(date).format('YYYY-MM-DD')
                        });
                    }

                    if (localStorageService.get('kiosk.user')) {
                        $scope.currentUser = localStorageService.get('kiosk.user').user;
                        $scope.getEmployeeProfile($scope.currentUser.E_UUID);
                        $scope.getEmployeeOvertime($scope.currentUser.Emp_UUID, $scope.currentPayroll.UUID);
                    }
                }
            }, function() {});
        };

        $scope.insertOvertime = function() {
            if (_.isEmpty($scope.currentPayroll)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected ACTIVE PAYROLL OPTION to get the employee ATTENDANCE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/kiosk/templates/attendance/overtime/overtime2.modal.html',
                controller: 'overtimeModalCtrl',
                size: 'md',
                resolve: {
                    details: function() {
                        return null;
                    },
                    dateArray: function() {
                        return $scope.dateArray;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.TotalNoHrs = 0;

                    selectedItem.RefNo = $scope.refNo;
                    selectedItem.IsExpired = false;
                    $scope.overtimes.push(selectedItem);

                    _.each($scope.overtimes, function(row) {
                        $scope.TotalNoHrs += row.NoHrs;
                    });
                    $scope.overtimes = $filter('orderBy')($scope.overtimes, 'InclusiveDates');
                    console.log('$scope.overtimes: ', $scope.overtimes);
                }
            }, function() {});
        };

        $scope.updateOvertime = function(IndexID, detail, $index) {
            if (IndexID) {
                detail.IndexID = IndexID;
                detail.TransDate = $scope.overtime.TransDate;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/kiosk/templates/attendance/overtime/overtime2.modal.html',
                controller: 'overtimeModalCtrl',
                size: 'md',
                resolve: {
                    details: function() {
                        return detail;
                    },
                    dateArray: function() {
                        return $scope.dateArray;
                    }
                }
            });
            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    if (_.isEmpty(IndexID)) {
                        $scope.TotalNoHrs = 0;
                        $scope.overtimes.splice($index, 1);
                        selectedItem.RefNo = $scope.refNo;
                        $scope.overtimes.push(selectedItem);
                        _.each($scope.overtimes, function(row) {
                            $scope.TotalNoHrs += row.NoHrs;
                        });
                        $scope.overtimes = $filter('orderBy')($scope.overtimes, 'InclusiveDates');
                        console.log('$scope.overtimes: ', $scope.overtimes);
                    } else {
                        $scope.refreshData();
                    }
                }
            }, function() {});
        };

        $scope.logOvertime = function(IndexID, detail, $index) {
            if (detail.IsPreApproved !== 1) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the selected OVERTIME details is not PRE-APPROVED." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            if (IndexID) {
                detail.IndexID = IndexID;
                detail.TransDate = $scope.overtime.TransDate;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/kiosk/templates/attendance/overtime/overtime.modal.html',
                controller: 'overtimeLogModalCtrl',
                size: 'md',
                resolve: {
                    details: function() {
                        return detail;
                    },
                    employee: function() {
                        return $scope.employee;
                    }
                }
            });
            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.refreshData();
                }
            }, function() {});
        };

        $scope.saveOvertime = function() {
            $scope.modal = {
                title: 'Employee Overtime',
                message: 'Are you sure to save this overtime?'
            };

            if (_.isEmpty($scope.currentPayroll)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected ACTIVE PAYROLL OPTION to get the employee ATTENDANCE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            if (_.isEmpty($scope.employee)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected EMPLOYEE OVERTIME RECORD." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            if (_.isEmpty($scope.overtimes)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not set any OVERTIME DETAILS." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            $scope.overtime.PayrollID = $scope.currentPayroll.PayrollID;
            $scope.overtime.EmployeeID = $scope.employee.e_idno;
            $scope.overtime.RefNo = $scope.refNo;
            $scope.overtime.TotalNoHrs = $scope.TotalNoHrs;

            var overtimes = _.filter($scope.overtimes, {
                'IsExpired': false
            });

            _.each(overtimes, function(row) {
                if (row.TimeIn) {
                    row.TimeIn = moment(new Date(row.TimeIn)).format('YYYY-MM-DD HH:mm:ss')
                }
                if (row.TimeOut) {
                    row.TimeOut = moment(new Date(row.TimeOut)).format('YYYY-MM-DD HH:mm:ss')
                }
                if (row.OTExpiry) {
                    row.OTExpiry = moment(new Date(row.OTExpiry)).format('YYYY-MM-DD')
                }
            });
            console.log('overtimes: ',overtimes);
            $scope.overtime.overtimes = overtimes;

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                attendance.createEmployeeOvertime($scope.employee.Emp_UUID, $scope.currentPayroll.UUID, $scope.overtime).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS')
                        $timeout(function() {
                            $scope.refreshData();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            });
        };

        $scope.deleteOvertime = function() {
            $scope.modal = {
                title: 'Employee Overtime',
                message: 'Are you sure to delete this overtime?'
            };

            if (_.isEmpty($scope.overtime)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected EMPLOYEE OVERTIME RECORD." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                attendance.DeleteOvertime($scope.overtime.OT_UUID).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function() {
                            $state.reload();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            });
        };

        $scope.deleteOverTimeDetail = function(IndexID, $index) {
            $scope.modal = {
                title: 'Employee Overtime',
                message: 'Are you sure to delete this overtime?'
            };

            if (IndexID) {
                if (_.isEmpty($scope.overtime)) {
                    toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected EMPLOYEE OVERTIME RECORD." +
                        "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                    return;
                }

                ngDialog.openConfirm({
                    templateUrl: './public/dialogs/custom.dialog.html',
                    scope: $scope,
                    className: 'ngdialog-theme-default'
                }).then(function() {
                    attendance.Delete_OverTime_Details($scope.employee.Emp_UUID, $scope.currentPayroll.UUID, IndexID).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'SUCCESS');
                            $timeout(function() {
                                $state.reload();
                            }, 300);
                        } else {
                            toastr.error(data.response.msg, 'ERROR');
                            return;
                        }
                    });
                });
            } else {
                $scope.TotalNoHrs = 0;
                $scope.overtimes.splice($index, 1);
                _.each($scope.overtimes, function(row) {
                    $scope.TotalNoHrs += row.NoHrs;
                });
            }
        };
    }

})();