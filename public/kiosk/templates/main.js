'use strict';

angular.module('kiosk')
    .controller('mainCtrl', mainCtrl);

mainCtrl.$inject = ['$scope', '$state', 'auth', 'localStorageService', '$location','$uibModal']

function mainCtrl($scope, $state, auth, localStorageService, $location,$uibModal) {
    if (auth.getUser()) {
        $scope.currentUser = auth.getUser().user;
    } else {
        $location.path('/login');
    }

    $scope.logout = function() {
        auth.logout().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                localStorageService.remove('kiosk.user');
                localStorageService.remove('kiosk.authdata');
                localStorageService.remove('hris.payroll');
                $state.go('auth.login');
            }
        });
    };

    $scope.showUserProfile = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/kiosk/templates/profile/profile.html',
                controller: 'profileCtrl',
                size: 'lg'
            });

            modalInstance.result.then(function () {}, function () {});
        };


    $scope.showAbout = function() {

        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './public/hris/templates/about.modal.html',
            size: 'md',
            scope: $scope
        });

        $scope.cancel = function() {
            modalInstance.dismiss('cancel');
        };

        modalInstance.result.then(function() {}, function() {});
    };
}