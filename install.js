var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
    name: 'HRIS',
    description: 'MEAN Stack HRIS',
    script: require('path').join(__dirname, 'index.js')
    // script: 'C:\\wamp\\www\\hris\\index.js'
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install', function() {
    svc.start();
});


// Just in case this file is run twice.
svc.on('alreadyinstalled',function(){
  console.log('This service is already installed.');
});

// Listen for the "start" event and let us know when the
// process has actually started working.
svc.on('start ',function(){
console.lo
g(svc.name+' started!\nVisit http://127.0.0.1:3000 to see it in action.');
});


svc.install();
